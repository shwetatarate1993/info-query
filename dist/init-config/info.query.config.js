"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InfoQueryConfig {
    constructor() {
        /** No Op */
    }
    static configure(config, dialect) {
        this.configuredDialect = dialect;
        this.dbConfig = config;
    }
}
exports.default = InfoQueryConfig;
//# sourceMappingURL=info.query.config.js.map