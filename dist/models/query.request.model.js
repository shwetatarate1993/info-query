"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class QueryRequest {
    constructor(database, type, isNative, native, query, parameters, datasource) {
        this.query = '';
        this.parameters = [];
        this.database = database;
        this.type = type;
        this.query = query;
        this.isNative = isNative;
        this.native = native;
        this.parameters = parameters;
        this.datasource = datasource;
        Object.freeze(this);
    }
}
exports.default = QueryRequest;
//# sourceMappingURL=query.request.model.js.map