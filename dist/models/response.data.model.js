'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class ResponseData {
    constructor(rows, columns, cols) {
        this.rows = rows;
        this.columns = columns;
        this.cols = cols;
        Object.freeze(this);
    }
}
exports.default = ResponseData;
//# sourceMappingURL=response.data.model.js.map