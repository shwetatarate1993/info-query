'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable: variable-name*/
class QueryResponse {
    constructor(started_at, average_execution_time, status, context, row_count, running_time, data) {
        this.started_at = started_at;
        this.average_execution_time = average_execution_time;
        this.status = status;
        this.context = context;
        this.row_count = row_count;
        this.running_time = running_time;
        this.data = data;
        Object.freeze(this);
    }
}
exports.default = QueryResponse;
//# sourceMappingURL=query.response.model.js.map