'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
// TODO fix criteria grouping for criterias
class Criteria {
    constructor(field, condition, value) {
        this.field = field;
        this.condition = condition;
        this.value = value;
        Object.freeze(this);
    }
}
exports.default = Criteria;
//# sourceMappingURL=criteria.model.js.map