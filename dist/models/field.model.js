'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class Field {
    constructor(tableId, fieldId, alias) {
        this.tableId = tableId;
        this.fieldId = fieldId;
        this.alias = alias;
        Object.freeze(this);
    }
}
exports.default = Field;
//# sourceMappingURL=field.model.js.map