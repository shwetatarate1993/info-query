'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class Aggregation {
    constructor(field, aggregationFunction) {
        this.field = field;
        this.aggregationFunction = aggregationFunction;
        Object.freeze(this);
    }
}
exports.default = Aggregation;
//# sourceMappingURL=aggregation.model.js.map