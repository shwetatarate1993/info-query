"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NativeQuery {
    constructor(database, type, native, isNative, datasource) {
        this.database = database;
        this.type = type;
        this.native = native;
        this.isNative = isNative;
        this.datasource = datasource;
        Object.freeze(this);
    }
}
exports.default = NativeQuery;
//# sourceMappingURL=native.query.model.js.map