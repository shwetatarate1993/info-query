'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class OrderBy {
    constructor(field, value) {
        this.field = field;
        this.value = value;
        Object.freeze(this);
    }
}
exports.default = OrderBy;
//# sourceMappingURL=orderby.model.js.map