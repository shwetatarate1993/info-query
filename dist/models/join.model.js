'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class Join {
    constructor(sourceTableId, sourceFieldId, targetTableId, targetFieldId, joinType) {
        this.sourceTableId = sourceTableId;
        this.sourceFieldId = sourceFieldId;
        this.targetTableId = targetTableId;
        this.targetFieldId = targetFieldId;
        this.joinType = joinType;
        Object.freeze(this);
    }
}
exports.default = Join;
//# sourceMappingURL=join.model.js.map