'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class Query {
    constructor(type, databaseId, sourceTableId, limit, fields, criterias, aggregations, breakouts, orderBys, joins) {
        this.type = type;
        this.databaseId = databaseId;
        this.sourceTableId = sourceTableId;
        this.limit = limit;
        this.fields = fields;
        this.criterias = criterias;
        this.aggregations = aggregations;
        this.breakouts = breakouts;
        this.orderBys = orderBys;
        this.joins = joins;
        Object.freeze(this);
    }
}
exports.default = Query;
//# sourceMappingURL=query.model.js.map