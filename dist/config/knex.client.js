"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("knex"));
const constants_1 = require("../constants");
const createKnex = (dbConfig) => {
    const dialect = dbConfig.dialect;
    const dbEngine = constants_1.parseDBEngineEnum(dialect);
    switch (dbEngine) {
        case constants_1.DBEngine.SQLLITE:
            return knex_1.default({
                client: dialect,
                connection: dbConfig.connection,
                useNullAsDefault: true,
            });
        case constants_1.DBEngine.MYSQL:
            return knex_1.default({
                client: dialect,
                connection: {
                    database: dbConfig.name,
                    host: dbConfig.host,
                    password: dbConfig.password,
                    user: dbConfig.username,
                },
                pool: {
                    max: dbConfig.pool.max,
                    min: dbConfig.pool.min,
                },
                useNullAsDefault: true,
            });
        default:
            throw new Error('Invalid or Unsupported DB Engine ' + dbEngine);
    }
};
exports.default = createKnex;
//# sourceMappingURL=knex.client.js.map