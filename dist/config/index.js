"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const convict_1 = __importDefault(require("convict"));
const development_1 = __importDefault(require("./env/development"));
const production_1 = __importDefault(require("./env/production"));
const serverurl = 'http://ec2-3-87-133-52.compute-1.amazonaws.com:8891';
// Define a schema
const config = convict_1.default({
    env: {
        default: 'development',
        doc: 'The application environment.',
        env: 'NODE_ENV',
        format: ['production', 'development', 'test'],
    },
    secret: {
        default: 'defaultsecret',
        doc: 'Secret key',
        env: 'SECRET_KEY',
        format: '*',
    },
    ip: {
        default: '127.0.0.1',
        doc: 'The IP address to bind.',
        env: 'IP_ADDRESS',
        format: 'ipaddress',
    },
    port: {
        arg: 'port',
        default: 8891,
        doc: 'The port to bind.',
        env: 'PORT',
        format: 'port',
    },
    db: {
        '1001': {
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    doc: 'SQL Lite connection type',
                    format: '*',
                    default: ':memory:',
                },
            },
        },
        'mock': {
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    doc: 'SQL Lite connection type',
                    format: '*',
                    default: ':memory:',
                },
            },
        },
        '8b2b601c-0bec-4701-b6d7-c60293cb81c7': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'dummy',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
        '963c43fd-ae78-4989-b6d9-61ef9bb25948': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'proddb.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'prodrecruitmentapp',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
        '663c43fd-ae78-4989-b6d9-61ef9bb25948': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'proddb.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'prodfeaturemanagementmd',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
        '763c43fd-ae78-4989-b6d9-61ef9bb25948': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'proddb.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'prodinboxFeaturemanagementmd',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
        '863c43fd-ae78-4989-b6d9-61ef9bb25948': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'proddb.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'prodrecruitmentmd',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
        '963c43fd-ae78-4989-b6d9-61ef9bb259481': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'proddb.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'tpcds1',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
        '2a89f605-dcb8-4571-aab3-a4c03de6c79f': {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'villageportalappdev',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
        },
    },
    links: {
        cancel: serverurl + '/v1/authentication/cancel',
        change_password: serverurl + '/v1/authentication/credentials/change_password',
        extendedHelp: 'http://docs.domain.ext/solution',
        unlock_password: serverurl + '/v1/authentication/recovery/unlock',
        user_uri: serverurl + '/v1/users/',
    },
});
// Load environment dependent configuration
const envName = config.get('env');
switch (envName) {
    case 'production':
        config.load(production_1.default);
        break;
    case 'development':
        config.load(development_1.default);
        break;
    default:
    // do nothing
}
// Perform validation
config.validate({ allowed: 'strict' });
exports.default = config;
//# sourceMappingURL=index.js.map