"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../../config"));
const knex_config_1 = require("knex-config");
test('knex configuration for sqllite3', async () => {
    const databaseId = '1001';
    try {
        const knex = knex_config_1.knexClient(config_1.default.get('db'), databaseId, 'sqlite3');
        expect(knex).toBeTruthy();
    }
    catch (err) {
        throw err;
    }
});
//# sourceMappingURL=knex.test.js.map