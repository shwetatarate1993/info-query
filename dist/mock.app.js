"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mock_express_1 = __importDefault(require("mock-express"));
const controllers_1 = require("./controllers");
const mockApp = mock_express_1.default();
mockApp.get('/query', (req, res, next) => {
    controllers_1.queryController.executeQuery(req, res, next);
});
exports.default = mockApp;
//# sourceMappingURL=mock.app.js.map