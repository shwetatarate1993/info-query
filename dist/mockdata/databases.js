"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        description: '',
        features: [
            'basic-aggregations',
            'foreign-keys',
            'native-parameters',
            'nested-queries',
            'expressions',
            'binning',
        ],
        name: 'Sample SQLLITEDB',
        tables: [
            {
                description: 'This is a confirmed order for a product from a user.',
                schema: '',
                active: true,
                name: 'ORDERS',
                rows: 3,
                updated_at: '2018-09-29T06:50:02.000Z',
                id: 2,
                db_id: 1001,
                visibility_type: '',
                display_name: 'Orders',
                created_at: '2017-12-30T12:28:26.000Z',
            },
        ],
        updated_at: '2017-12-30T12:28:26.000Z',
        native_permissions: 'write',
        details: {
            client: 'sqlite3',
            connection: ':memory:',
        },
        id: 1001,
        engine: 'sqlite3',
        created_at: '2017-12-30T12:28:25.000Z',
    },
    {
        description: '',
        features: [
            'basic-aggregations',
            'foreign-keys',
            'native-parameters',
            'nested-queries',
            'expressions',
            'binning',
        ],
        name: 'Sample SQLLITEDB',
        tables: [
            {
                description: 'This is a confirmed order for a product from a user.',
                schema: '',
                active: true,
                name: 'ORDERS',
                rows: 3,
                updated_at: '2018-09-29T06:50:02.000Z',
                id: 2,
                db_id: '963c43fd-ae78-4989-b6d9-61ef9bb25948',
                visibility_type: '',
                display_name: 'Orders',
                created_at: '2017-12-30T12:28:26.000Z',
            },
        ],
        updated_at: '2017-12-30T12:28:26.000Z',
        native_permissions: 'write',
        details: {
            client: 'sqlite3',
            connection: ':memory:',
        },
        id: '963c43fd-ae78-4989-b6d9-61ef9bb25948',
        engine: 'sqlite3',
        created_at: '2017-12-30T12:28:25.000Z',
    },
    {
        description: '',
        features: [
            'basic-aggregations',
            'standard-deviation-aggregations',
            'expression-aggregations',
            'foreign-keys',
            'native-parameters',
            'nested-queries',
            'expressions',
            'set-timezone',
            'binning',
        ],
        name: 'Recruitment',
        tables: [
            {
                description: '',
                name: 'remarks_audit',
                rows: 0,
                updated_at: '2018-09-29T06:50:21.000Z',
                active: true,
                id: 823,
                db_id: 11,
                visibility_type: '',
                display_name: 'Remarks Audit',
                created_at: '2018-09-12T12:47:13.000Z',
                schema: '',
            },
            {
                description: '',
                schema: '',
                name: 'requirement_table',
                rows: 21,
                updated_at: '2018-09-29T06:50:21.000Z',
                active: true,
                id: 830,
                db_id: 11,
                visibility_type: '',
                display_name: 'Requirement Table',
                created_at: '2018-09-12T12:47:13.000Z',
            },
        ],
        updated_at: '2018-09-12T12:47:13.000Z',
        native_permissions: 'write',
        details: {
            'host': 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
            'port': 3306,
            'dbname': 'devrecruimentapp',
            'user': 'awsdevmaster',
            'password': '**InfoBiPass**',
            'tunnel-port': 22,
        },
        id: 11,
        engine: 'mysql',
        created_at: '2018-09-12T12:47:13.000Z',
    },
    {
        description: '',
        features: [
            'basic-aggregations',
            'standard-deviation-aggregations',
            'expression-aggregations',
            'foreign-keys',
            'native-parameters',
            'nested-queries',
            'expressions',
            'binning',
        ],
        name: 'Sample Dataset',
        tables: [
            {
                description: 'This is a confirmed order for a product from a user.',
                schema: 'PUBLIC',
                active: true,
                name: 'ORDERS',
                rows: 12805,
                updated_at: '2018-09-29T06:50:02.000Z',
                id: 2,
                db_id: 1,
                visibility_type: '',
                display_name: 'Orders',
                created_at: '2017-12-30T12:28:26.000Z',
            },
            {
                description: 'This is a user account. Note that employees and customer support staff will have accounts.',
                schema: 'PUBLIC',
                name: 'PEOPLE',
                rows: 2500,
                updated_at: '2018-09-29T06:50:02.000Z',
                active: true,
                id: 3,
                db_id: 1,
                visibility_type: '',
                display_name: 'People',
                created_at: '2017-12-30T12:28:26.000Z',
            },
            {
                description: 'This is our product catalog. It includes all products ever sold by the Sample Company.',
                schema: 'PUBLIC',
                name: 'PRODUCTS',
                rows: 200,
                updated_at: '2018-09-29T06:50:02.000Z',
                active: true,
                id: 1,
                db_id: 1,
                visibility_type: '',
                display_name: 'Products',
                created_at: '2017-12-30T12:28:26.000Z',
            },
            {
                description: 'These are reviews our customers have left on products. '
                    + 'Note that these are not tied to orders so it is possible people have reviewed '
                    + 'products they did not purchase from us.',
                schema: 'PUBLIC',
                name: 'REVIEWS',
                rows: 984,
                updated_at: '2018-09-29T06:50:03.000Z',
                active: true,
                id: 4,
                db_id: 1,
                visibility_type: '',
                display_name: 'Reviews',
                created_at: '2017-12-30T12:28:26.000Z',
            },
        ],
        updated_at: '2017-12-30T12:28:26.000Z',
        native_permissions: 'write',
        details: {
            db: 'zip:/apps/infoBi.jar!/sample-dataset.db;USER=GUEST;PASSWORD=guest',
        },
        id: 1,
        engine: 'sqlite3',
        created_at: '2017-12-30T12:28:25.000Z',
    },
    {
        name: 'Saved Questions',
        id: -1337,
        features: [
            'basic-aggregations',
        ],
        tables: [
            {
                id: 'card__10',
                db_id: -1337,
                display_name: 'All Task',
                schema: 'Recruitment',
                description: '',
            },
            {
                id: 'card__11',
                db_id: -1337,
                display_name: 'Cities',
                schema: 'Recruitment',
                description: '',
            },
            {
                id: 'card__13',
                db_id: -1337,
                display_name: 'Conversation Messages',
                schema: 'DUmmy',
                description: '',
            },
            {
                id: 'card__14',
                db_id: -1337,
                display_name: 'Conversation Messages, Count',
                schema: 'Everything else',
                description: '',
            },
            {
                id: 'card__15',
                db_id: -1337,
                display_name: 'Expenses',
                schema: 'Everything else',
                description: '',
            },
            {
                id: 'card__16',
                db_id: -1337,
                display_name: 'Expenses',
                schema: 'Everything else',
                description: '',
            },
            {
                id: 'card__12',
                db_id: -1337,
                display_name: 'Requirement',
                schema: 'Recruitment',
                description: '',
            },
        ],
        is_saved_questions: true,
    },
];
//# sourceMappingURL=databases.js.map