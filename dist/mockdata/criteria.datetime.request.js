"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    dateTimeBetweenCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'BETWEEN',
                [
                    'datetime-field',
                    ['field-id', '16'],
                    'minute',
                ],
                '2019-01-01T12:30:00',
                '2019-01-07T12:30:00',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeEqualCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                '=',
                [
                    'field-id', '16',
                ],
                '2019-01-08',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeMonthYearCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/month-year',
                [
                    'field-id', '16',
                ],
                '2019-01',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeOnCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'ON',
                [
                    'field-id', '16',
                ],
                '2019-01-01T12:31:00',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeQuarterYearCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/quarter-year',
                [
                    'field-id', '16',
                ],
                'Q1-2019',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeTodayCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'today',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeYesterdayCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'yesterday',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativePast7DayCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'past7days',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativePast30DayCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'past30days',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeLastWeekCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'lastweek',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeLastMonthCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'lastmonth',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeLastYearCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'lastyear',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeThisWeekCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'thisweek',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeThisMonthCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'thismonth',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    dateTimeRelativeThisYearCriterias: {
        database: '1001',
        query: {
            aggregation: [],
            breakout: [],
            filter: [
                'date/relative',
                [
                    'field-id', '16',
                ],
                'thisyear',
            ],
            order_by: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    previousZeroDay: {
        database: '1001',
        query: {
            filter: [
                'time-interval',
                [
                    'field-id', '8',
                ],
                0,
                'day',
                {},
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    previousDay: {
        database: '1001',
        query: {
            filter: [
                'time-interval',
                [
                    'field-id', '8',
                ],
                -2,
                'day',
                {},
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    previousDayIncludeCurrent: {
        database: '1001',
        query: {
            filter: [
                'time-interval',
                [
                    'field-id', '8',
                ],
                -2,
                'day',
                {
                    'include-current': true,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    nextDay: {
        database: '1001',
        query: {
            filter: [
                'time-interval',
                [
                    'field-id', '8',
                ],
                2,
                'day',
                {},
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    nextDayIncludeCurrent: {
        database: '1001',
        query: {
            filter: [
                'time-interval',
                [
                    'field-id', '8',
                ],
                2,
                'day',
                {
                    'include-current': true,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
};
//# sourceMappingURL=criteria.datetime.request.js.map