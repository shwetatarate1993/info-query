"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    simpleFieldFetchAndAlias: {
        database: '1001',
        query: {
            fields: [
                {
                    id: '4',
                    alias: 'Products Category',
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    fieldsWithAndWithoutAlias: {
        database: '1001',
        query: {
            fields: [
                {
                    id: '15',
                },
                {
                    id: '17',
                    alias: 'Quantity',
                },
                {
                    id: '9',
                    alias: 'Discount',
                },
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    fetchfieldsWithOneGroupBy: {
        database: '1001',
        query: {
            fields: [
                {
                    id: '15',
                },
                {
                    id: '17',
                    alias: 'Quantity',
                },
                {
                    id: '9',
                    alias: 'Discount',
                },
            ],
            breakout: [
                [
                    'field-id', '9',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    fetchfieldsWithTwoGroupBy: {
        database: '1001',
        query: {
            fields: [
                {
                    id: '15',
                },
                {
                    id: '17',
                    alias: 'Quantity',
                },
                {
                    id: '9',
                    alias: 'Discount',
                },
            ],
            breakout: [
                [
                    'field-id', '9',
                ],
                [
                    'field-id', '13',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    fetchfieldsWithGroupByAndAggregations: {
        database: '1001',
        query: {
            fields: [
                {
                    id: '15',
                },
            ],
            breakout: [
                [
                    'field-id', '13',
                ],
            ],
            aggregation: [
                [
                    'sum',
                    ['field-id', '17', 'Total Quantity'],
                ],
                [
                    'avg',
                    ['field-id', '9', 'Average Discount'],
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    hybridRequestWithAllClause: {
        database: '1001',
        query: {
            fields: [
                {
                    id: '15',
                },
            ],
            breakout: [
                [
                    'field-id', '13',
                ],
            ],
            aggregation: [
                [
                    'sum',
                    ['field-id', '17', 'Total Quantity'],
                ],
                [
                    'avg',
                    ['field-id', '9', 'Average Discount'],
                ],
            ],
            order_by: [
                [
                    ['field-id', '17'],
                    'descending',
                ],
            ],
            limit: 2,
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
};
//# sourceMappingURL=field.request.js.map