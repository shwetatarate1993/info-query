"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    equalCriteria: {
        database: '1001',
        query: {
            filter: [
                '=',
                [
                    'field-id', '17',
                ],
                5,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    notEqualCriteria: {
        database: '1001',
        query: {
            filter: [
                '!=',
                [
                    'field-id', '17',
                ],
                5,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    greaterThanCriteria: {
        database: '1001',
        query: {
            filter: [
                '>',
                [
                    'field-id', '17',
                ],
                3,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    lessThanCriteria: {
        database: '1001',
        query: {
            filter: [
                '<',
                [
                    'field-id', '17',
                ],
                3,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    betweenCriteria: {
        database: '1001',
        query: {
            filter: [
                'BETWEEN',
                [
                    'field-id', '17',
                ],
                2,
                5,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    greaterThanOrEqualCriteria: {
        database: '1001',
        query: {
            filter: [
                '>=',
                [
                    'field-id', '17',
                ],
                3,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    lessThanOrEqualCriteria: {
        database: '1001',
        query: {
            filter: [
                '<=',
                [
                    'field-id', '17',
                ],
                3,
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    isEmptyCriteria: {
        database: '1001',
        query: {
            filter: [
                'IS_NULL',
                [
                    'field-id', '17',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    notEmptyCriteria: {
        database: '1001',
        query: {
            filter: [
                'NOT_NULL',
                [
                    'field-id', '17',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
};
//# sourceMappingURL=criteria.numeric.request.js.map