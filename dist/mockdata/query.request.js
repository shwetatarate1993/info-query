"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    equalCondition: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'count',
                ],
                [
                    'min',
                    [
                        'field-id',
                        '14',
                    ],
                ],
            ],
            breakout: [
                [
                    'field-id',
                    '14',
                ],
            ],
            filter: [
                '=',
                [
                    'fk->', '10', '25',
                ],
                1,
            ],
            order_by: [
                [
                    'field-id',
                    '14',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    simpleNativeQuery: {
        database: '1001',
        native: {
            query: 'select * from todos',
            collection: '',
        },
        parameters: [],
        type: 'native',
        query: {},
        isNative: true,
    },
    customQueryWithAggregations: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'count',
                ],
                [
                    'sum',
                    [
                        'field-id',
                        '12',
                    ],
                ],
            ],
            breakout: [
                [
                    'field-id',
                    '13',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    nativeQueryWithAggregations: {
        database: '1001',
        native: {
            query: 'select PRODUCT_ID, count(*) as count, sum(TAX) as totalTax from ORDERS group by PRODUCT_ID',
        },
        type: 'native',
        isNative: true,
    },
    customQueryWithOneGroupBy: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'sum',
                    [
                        'field-id',
                        '17',
                    ],
                ],
            ],
            breakout: [
                [
                    'field-id',
                    '13',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    nativeQueryWithOneGroupBy: {
        database: '1001',
        native: {
            query: 'select sum(QUANTITY) as Quantity from ORDERS group by PRODUCT_ID',
        },
        type: 'native',
        isNative: true,
    },
    nativeMinAggregationAndGroupBy: {
        database: '1001',
        native: {
            query: 'select min(QUANTITY) as Quantity from ORDERS group by PRODUCT_ID',
        },
        type: 'native',
        isNative: true,
    },
    nativeMaxAggregation: {
        database: '1001',
        native: {
            query: 'select max(QUANTITY) as Quantity from ORDERS',
        },
        type: 'native',
        isNative: true,
    },
    simpleCustomMaxAggregation: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'max',
                    [
                        'field-id',
                        '17',
                    ],
                ],
            ],
            breakout: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    simpleCustomAvgAggregation: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'avg',
                    [
                        'field-id',
                        '17',
                    ],
                ],
            ],
            breakout: [],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    nativeAvgAggregation: {
        database: '1001',
        native: {
            query: 'select avg(QUANTITY) as Quantity from ORDERS',
        },
        type: 'native',
        isNative: true,
    },
    simpleCustomQueryLimit: {
        database: '1001',
        query: {
            source_table: '2',
            limit: 2,
        },
        type: 'query',
        isNative: false,
    },
    simpleNativeQueryLimit: {
        database: '1001',
        native: {
            query: 'select QUANTITY from ORDERS limit 2',
        },
        type: 'native',
        isNative: true,
    },
    customQueryWithTwoGroupBy: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'sum',
                    [
                        'field-id',
                        '17',
                    ],
                ],
            ],
            breakout: [
                [
                    'field-id',
                    '13',
                ],
                [
                    'field-id',
                    '9',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    nativeQueryWithTwoGroupBy: {
        database: '1001',
        native: {
            query: 'select sum(QUANTITY) as Quantity from ORDERS group by PRODUCT_ID, DISCOUNT',
        },
        type: 'native',
        isNative: true,
    },
    customQueryWithTwoAggregationAndGroupBy: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'sum',
                    [
                        'field-id',
                        '17',
                    ],
                ],
                [
                    'avg',
                    [
                        'field-id',
                        '9',
                    ],
                ],
            ],
            breakout: [
                [
                    'field-id',
                    '13',
                ],
                [
                    'field-id',
                    '9',
                ],
            ],
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    nativeQueryWithTwoAggregationAndGroupBy: {
        database: '1001',
        native: {
            query: 'select sum(QUANTITY) as Total_Quantity, avg(DISCOUNT) as Average_Discount '
                + 'from ORDERS group by PRODUCT_ID, DISCOUNT',
        },
        type: 'native',
        isNative: true,
    },
    customHybridQuery: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'sum',
                    [
                        'field-id',
                        '12',
                    ],
                ],
            ],
            breakout: [
                [
                    'field-id',
                    '13',
                ],
            ],
            order_by: [
                [
                    ['field-id', '12'],
                    'descending',
                ],
            ],
            filter: [],
            source_table: '2',
            limit: 3,
        },
        type: 'query',
        isNative: false,
    },
    hybridNativeQuery: {
        database: '1001',
        native: {
            query: 'select TITLE, sum(TAX) as totalTax '
                + 'from ORDERS o left join PRODUCTS p on (o.PRODUCT_ID = p.ID) '
                + 'group by PRODUCT_ID '
                + 'order by PRODUCT_ID desc limit 3',
        },
        type: 'native',
        isNative: true,
    },
    sqliteQuery: {
        database: '1001',
        query: {
            aggregation: [
                [
                    'sum',
                    [
                        'field-id', '17',
                    ],
                ],
            ],
            breakout: [
                [
                    'fk->', '13', '7',
                ],
            ],
            filter: [
                '=',
                [
                    'fk->', '13', '1',
                ],
                'Ziemann, Yundt and Murazik',
                'Roob, Smith and Hettinger',
            ],
            limit: 1,
            source_table: '2',
        },
        type: 'query',
        isNative: false,
    },
    betweenCriteria: {
        database: '1001',
        parameters: [],
        query: {
            source_table: '2',
            filter: [
                'BETWEEN',
                [
                    'field-id', '11',
                ],
                50,
                80,
            ],
        },
        type: 'query',
        isNative: false,
        native: null,
    },
};
//# sourceMappingURL=query.request.js.map