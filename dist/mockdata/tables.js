"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [{
        description: 'TODOs Task table',
        schema: '',
        db: {
            description: '',
            features: [
                'basic-aggregations',
                'foreign-keys',
                'native-parameters',
                'nested-queries',
                'expressions',
                'binning',
            ],
            name: 'Sample SQLLITEDB',
            updated_at: '2017-12-30T12:28:26.000Z',
            native_permissions: 'write',
            details: {
                client: 'sqlite3',
                connection: ':memory:',
            },
            id: 1001,
            engine: 'sqlite3',
            created_at: '2017-12-30T12:28:25.000Z',
        },
        name: 'todos',
        fields: [
            {
                description: 'Unique Task ID',
                table_id: 2001,
                special_type: '',
                name: 'id',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '9',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Unique ID',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Integer',
            },
            {
                description: 'Todo Tasks',
                table_id: 2001,
                special_type: '',
                name: 'tasks',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '10',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Tasks',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/String',
            },
            {
                description: 'Urgency Level',
                table_id: 2001,
                special_type: '',
                name: 'urgent',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '12',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Urgent',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Integer',
            },
        ],
        dimension_options: {
            0: {
                name: 'Minute',
                mbql: [
                    'datetime-field',
                    '',
                    'minute',
                ],
                type: 'type/DateTime',
            },
            1: {
                name: 'Hour',
                mbql: [
                    'datetime-field',
                    '',
                    'hour',
                ],
                type: 'type/DateTime',
            },
            2: {
                name: 'Day',
                mbql: [
                    'datetime-field',
                    '',
                    'day',
                ],
                type: 'type/DateTime',
            },
            3: {
                name: 'Week',
                mbql: [
                    'datetime-field',
                    '',
                    'week',
                ],
                type: 'type/DateTime',
            },
            4: {
                name: 'Month',
                mbql: [
                    'datetime-field',
                    '',
                    'month',
                ],
                type: 'type/DateTime',
            },
            5: {
                name: 'Quarter',
                mbql: [
                    'datetime-field',
                    '',
                    'quarter',
                ],
                type: 'type/DateTime',
            },
            6: {
                name: 'Year',
                mbql: [
                    'datetime-field',
                    '',
                    'year',
                ],
                type: 'type/DateTime',
            },
            7: {
                name: 'Minute of Hour',
                mbql: [
                    'datetime-field',
                    '',
                    'minute-of-hour',
                ],
                type: 'type/DateTime',
            },
            8: {
                name: 'Hour of Day',
                mbql: [
                    'datetime-field',
                    '',
                    'hour-of-day',
                ],
                type: 'type/DateTime',
            },
            9: {
                name: 'Day of Week',
                mbql: [
                    'datetime-field',
                    '',
                    'day-of-week',
                ],
                type: 'type/DateTime',
            },
            10: {
                name: 'Day of Month',
                mbql: [
                    'datetime-field',
                    '',
                    'day-of-month',
                ],
                type: 'type/DateTime',
            },
            11: {
                name: 'Day of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'day-of-year',
                ],
                type: 'type/DateTime',
            },
            12: {
                name: 'Week of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'week-of-year',
                ],
                type: 'type/DateTime',
            },
            13: {
                name: 'Month of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'month-of-year',
                ],
                type: 'type/DateTime',
            },
            14: {
                name: 'Quarter of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'quarter-of-year',
                ],
                type: 'type/DateTime',
            },
            15: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    '',
                    'default',
                ],
                type: 'type/Number',
            },
            16: {
                name: '10 bins',
                mbql: [
                    'binning-strategy',
                    '',
                    'num-bins',
                    10,
                ],
                type: 'type/Number',
            },
            17: {
                name: '50 bins',
                mbql: [
                    'binning-strategy',
                    '',
                    'num-bins',
                    50,
                ],
                type: 'type/Number',
            },
            18: {
                name: '100 bins',
                mbql: [
                    'binning-strategy',
                    '',
                    'num-bins',
                    100,
                ],
                type: 'type/Number',
            },
            19: {
                name: 'Don\'t bin',
                mbql: '',
                type: 'type/Number',
            },
            20: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    '',
                    'default',
                ],
                type: 'type/Coordinate',
            },
            21: {
                name: 'Bin every 1 degree',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    1,
                ],
                type: 'type/Coordinate',
            },
            22: {
                name: 'Bin every 10 degrees',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    10,
                ],
                type: 'type/Coordinate',
            },
            23: {
                name: 'Bin every 20 degrees',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    20,
                ],
                type: 'type/Coordinate',
            },
            24: {
                name: 'Bin every 50 degrees',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    50,
                ],
                type: 'type/Coordinate',
            },
            25: {
                name: 'Don\'t bin',
                mbql: '',
                type: 'type/Coordinate',
            },
        },
        rows: 12805,
        updated_at: '2018-10-25T05:50:02.000Z',
        active: true,
        id: '2001',
        db_id: 1,
        display_name: 'ToDos',
        created_at: '2017-12-30T12:28:26.000Z',
    },
    {
        description: 'This is a confirmed order for a product from a user.',
        schema: '',
        db: {
            description: '',
            features: [
                'basic-aggregations',
                'standard-deviation-aggregations',
                'expression-aggregations',
                'foreign-keys',
                'native-parameters',
                'nested-queries',
                'expressions',
                'binning',
            ],
            timezone: 'UTC',
            name: 'Sample Dataset',
            updated_at: '2017-12-30T12:28:26.000Z',
            id: 1001,
            engine: 'sqlite3',
            created_at: '2017-12-30T12:28:25.000Z',
        },
        name: 'ORDERS',
        fields: [
            {
                description: 'The date and time an order was submitted.',
                table_id: '2',
                special_type: '',
                name: 'CREATED_AT',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '0',
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '16',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '2',
                target: '',
                preview_display: true,
                display_name: 'Created At',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/DateTime',
            },
            {
                description: 'Discount amount.',
                table_id: 2,
                special_type: '',
                name: 'DISCOUNT',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '9',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Discount',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
            },
            {
                description: 'This is a unique ID for the product. It is also called the '
                    + '“Invoice number” or “Confirmation number” in customer facing emails and screens.',
                table_id: 2,
                special_type: 'type/PK',
                name: 'ID',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '15',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '',
                target: '',
                preview_display: true,
                display_name: 'ID',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/BigInteger',
            },
            {
                description: 'The product ID. This is an internal identifier for the product, '
                    + 'NOT the SKU.',
                table_id: 2,
                special_type: 'type/FK',
                name: 'PRODUCT_ID',
                fingerprint_version: 1,
                fk_target_field_id: 7,
                dimension_options: [],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '13',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '',
                target: {
                    description: 'The numerical product number. Only used internally. '
                        + 'All external communication should use the title or EAN.',
                    table_id: 1,
                    special_type: 'type/PK',
                    name: 'ID',
                    fingerprint_version: 1,
                    caveats: '',
                    fk_target_field_id: '',
                    updated_at: '2018-10-25T05:50:00.000Z',
                    active: true,
                    parent_id: '',
                    id: '7',
                    raw_column_id: '',
                    last_analyzed: '2017-12-30T12:28:30.000Z',
                    position: 0,
                    visibility_type: 'normal',
                    preview_display: true,
                    display_name: 'ID',
                    fingerprint: {
                        global: {
                            'distinct-count': 200,
                        },
                        type: {
                            'type/Number': {
                                min: 1,
                                max: 200,
                                avg: 100.5,
                            },
                        },
                    },
                    created_at: '2017-12-30T12:28:27.000Z',
                    base_type: 'type/BigInteger',
                    points_of_interest: '',
                },
                preview_display: true,
                display_name: 'Product ID',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Integer',
            },
            {
                description: 'Number of products bought.',
                table_id: 2,
                special_type: 'type/Category',
                name: 'QUANTITY',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '17',
                values: [
                    [
                        0,
                    ],
                    [
                        1,
                    ],
                    [
                        2,
                    ],
                    [
                        3,
                    ],
                    [
                        4,
                    ],
                    [
                        5,
                    ],
                    [
                        6,
                    ],
                    [
                        7,
                    ],
                    [
                        8,
                    ],
                    [
                        9,
                    ],
                    [
                        10,
                    ],
                    [
                        11,
                    ],
                    [
                        12,
                    ],
                    [
                        13,
                    ],
                    [
                        14,
                    ],
                    [
                        15,
                    ],
                    [
                        16,
                    ],
                    [
                        17,
                    ],
                    [
                        18,
                    ],
                    [
                        19,
                    ],
                    [
                        20,
                    ],
                    [
                        22,
                    ],
                    [
                        24,
                    ],
                    [
                        25,
                    ],
                    [
                        26,
                    ],
                    [
                        27,
                    ],
                    [
                        28,
                    ],
                    [
                        29,
                    ],
                    [
                        30,
                    ],
                    [
                        32,
                    ],
                    [
                        33,
                    ],
                    [
                        34,
                    ],
                    [
                        35,
                    ],
                    [
                        36,
                    ],
                    [
                        37,
                    ],
                    [
                        38,
                    ],
                    [
                        39,
                    ],
                    [
                        40,
                    ],
                    [
                        41,
                    ],
                    [
                        44,
                    ],
                    [
                        46,
                    ],
                    [
                        47,
                    ],
                    [
                        48,
                    ],
                    [
                        49,
                    ],
                    [
                        50,
                    ],
                    [
                        51,
                    ],
                    [
                        52,
                    ],
                    [
                        53,
                    ],
                    [
                        55,
                    ],
                    [
                        56,
                    ],
                    [
                        57,
                    ],
                    [
                        58,
                    ],
                    [
                        59,
                    ],
                    [
                        60,
                    ],
                    [
                        61,
                    ],
                    [
                        63,
                    ],
                    [
                        65,
                    ],
                    [
                        66,
                    ],
                    [
                        68,
                    ],
                    [
                        69,
                    ],
                    [
                        70,
                    ],
                    [
                        72,
                    ],
                    [
                        73,
                    ],
                    [
                        76,
                    ],
                    [
                        80,
                    ],
                    [
                        81,
                    ],
                    [
                        87,
                    ],
                ],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '',
                target: '',
                preview_display: true,
                display_name: 'Quantity',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Integer',
            },
            {
                description: 'The raw, pre-tax cost of the order. Note that this might be different '
                    + 'in the future from the product price due to promotions, credits, etc.',
                table_id: 2,
                special_type: '',
                name: 'SUBTOTAL',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '11',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Subtotal',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
            },
            {
                description: 'This is the amount of local and federal taxes that are collected on '
                    + 'the purchase. Note that other governmental fees on some products are not included '
                    + 'here, but instead are accounted for in the subtotal.',
                table_id: 2,
                special_type: '',
                name: 'TAX',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimension_options: [
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                ],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '12',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Tax',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
            },
            {
                description: 'The total billed amount.',
                table_id: 2,
                special_type: '',
                name: 'TOTAL',
                fingerprint_version: 1,
                fk_target_field_id: '',
                dimensions: [],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '14',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '15',
                target: '',
                preview_display: true,
                display_name: 'Total',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
            },
            {
                description: 'The id of the user who made this order. Note that in some cases '
                    + 'where an order was created on behalf of a customer who phoned the order in, '
                    + 'this might be the employee who handled the request.',
                table_id: 2,
                special_type: 'type/FK',
                name: 'USER_ID',
                fingerprint_version: 1,
                fk_target_field_id: 25,
                dimension_options: [],
                updated_at: '2018-10-25T05:50:00.000Z',
                active: true,
                id: '10',
                values: [],
                last_analyzed: '2017-12-30T12:28:32.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '',
                target: {
                    description: 'A unique identifier given to each user.',
                    table_id: 3,
                    special_type: 'type/PK',
                    name: 'ID',
                    fingerprint_version: 1,
                    caveats: '',
                    fk_target_field_id: '',
                    updated_at: '2018-10-25T05:50:01.000Z',
                    active: true,
                    parent_id: '',
                    id: '25',
                    raw_column_id: '',
                    last_analyzed: '2017-12-30T12:28:34.000Z',
                    position: 0,
                    visibility_type: 'normal',
                    preview_display: true,
                    display_name: 'ID',
                    fingerprint: {
                        global: {
                            'distinct-count': 2500,
                        },
                        type: {
                            'type/Number': {
                                min: 1,
                                max: 2500,
                                avg: 1250.5,
                            },
                        },
                    },
                    created_at: '2017-12-30T12:28:27.000Z',
                    base_type: 'type/BigInteger',
                    points_of_interest: '',
                },
                preview_display: true,
                display_name: 'User ID',
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Integer',
            },
        ],
        dimension_options: {
            0: {
                name: 'Minute',
                mbql: [
                    'datetime-field',
                    '',
                    'minute',
                ],
                type: 'type/DateTime',
            },
            1: {
                name: 'Hour',
                mbql: [
                    'datetime-field',
                    '',
                    'hour',
                ],
                type: 'type/DateTime',
            },
            2: {
                name: 'Day',
                mbql: [
                    'datetime-field',
                    '',
                    'day',
                ],
                type: 'type/DateTime',
            },
            3: {
                name: 'Week',
                mbql: [
                    'datetime-field',
                    '',
                    'week',
                ],
                type: 'type/DateTime',
            },
            4: {
                name: 'Month',
                mbql: [
                    'datetime-field',
                    '',
                    'month',
                ],
                type: 'type/DateTime',
            },
            5: {
                name: 'Quarter',
                mbql: [
                    'datetime-field',
                    '',
                    'quarter',
                ],
                type: 'type/DateTime',
            },
            6: {
                name: 'Year',
                mbql: [
                    'datetime-field',
                    '',
                    'year',
                ],
                type: 'type/DateTime',
            },
            7: {
                name: 'Minute of Hour',
                mbql: [
                    'datetime-field',
                    '',
                    'minute-of-hour',
                ],
                type: 'type/DateTime',
            },
            8: {
                name: 'Hour of Day',
                mbql: [
                    'datetime-field',
                    '',
                    'hour-of-day',
                ],
                type: 'type/DateTime',
            },
            9: {
                name: 'Day of Week',
                mbql: [
                    'datetime-field',
                    '',
                    'day-of-week',
                ],
                type: 'type/DateTime',
            },
            10: {
                name: 'Day of Month',
                mbql: [
                    'datetime-field',
                    '',
                    'day-of-month',
                ],
                type: 'type/DateTime',
            },
            11: {
                name: 'Day of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'day-of-year',
                ],
                type: 'type/DateTime',
            },
            12: {
                name: 'Week of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'week-of-year',
                ],
                type: 'type/DateTime',
            },
            13: {
                name: 'Month of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'month-of-year',
                ],
                type: 'type/DateTime',
            },
            14: {
                name: 'Quarter of Year',
                mbql: [
                    'datetime-field',
                    '',
                    'quarter-of-year',
                ],
                type: 'type/DateTime',
            },
            15: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    '',
                    'default',
                ],
                type: 'type/Number',
            },
            16: {
                name: '10 bins',
                mbql: [
                    'binning-strategy',
                    '',
                    'num-bins',
                    10,
                ],
                type: 'type/Number',
            },
            17: {
                name: '50 bins',
                mbql: [
                    'binning-strategy',
                    '',
                    'num-bins',
                    50,
                ],
                type: 'type/Number',
            },
            18: {
                name: '100 bins',
                mbql: [
                    'binning-strategy',
                    '',
                    'num-bins',
                    100,
                ],
                type: 'type/Number',
            },
            19: {
                name: 'Don\'t bin',
                mbql: '',
                type: 'type/Number',
            },
            20: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    '',
                    'default',
                ],
                type: 'type/Coordinate',
            },
            21: {
                name: 'Bin every 1 degree',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    1,
                ],
                type: 'type/Coordinate',
            },
            22: {
                name: 'Bin every 10 degrees',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    10,
                ],
                type: 'type/Coordinate',
            },
            23: {
                name: 'Bin every 20 degrees',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    20,
                ],
                type: 'type/Coordinate',
            },
            24: {
                name: 'Bin every 50 degrees',
                mbql: [
                    'binning-strategy',
                    '',
                    'bin-width',
                    50,
                ],
                type: 'type/Coordinate',
            },
            25: {
                name: 'Don\'t bin',
                mbql: '',
                type: 'type/Coordinate',
            },
        },
        rows: 12805,
        updated_at: '2018-10-25T05:50:02.000Z',
        active: true,
        id: '2',
        db_id: 1,
        display_name: 'Orders',
        created_at: '2017-12-30T12:28:26.000Z',
    },
    {
        description: 'This is a user account. Note that employees and customer support staff '
            + 'will have accounts.',
        entity_type: null,
        schema: '',
        raw_table_id: null,
        db: {
            description: null,
            features: [
                'basic-aggregations',
                'standard-deviation-aggregations',
                'expression-aggregations',
                'foreign-keys',
                'native-parameters',
                'nested-queries',
                'expressions',
                'binning',
            ],
            cache_field_values_schedule: '0 50 0 * * ? *',
            timezone: 'UTC',
            metadata_sync_schedule: '0 50 * * * ? *',
            name: 'Sample Dataset',
            caveats: null,
            is_full_sync: true,
            updated_at: '2017-12-30T12:28:26.000Z',
            is_sample: true,
            id: 1001,
            is_on_demand: false,
            engine: 'sqlite3',
            created_at: '2017-12-30T12:28:25.000Z',
            points_of_interest: null,
        },
        show_in_getting_started: false,
        name: 'PEOPLE',
        fields: [
            {
                description: 'The street address of the account’s billing address',
                table_id: 3,
                special_type: null,
                name: 'ADDRESS',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '22',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Address',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 17.9912,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'The date of birth of the user',
                table_id: 3,
                special_type: null,
                name: 'BIRTH_DATE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [
                    '0',
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                ],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '19',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '2',
                target: null,
                preview_display: true,
                display_name: 'Birth Date',
                fingerprint: {
                    global: {
                        'distinct-count': 2293,
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Date',
                points_of_interest: null,
            },
            {
                description: 'The city of the account’s billing address',
                table_id: 3,
                special_type: 'type/City',
                name: 'CITY',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '18',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'City',
                fingerprint: {
                    global: {
                        'distinct-count': 2469,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 12.1504,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'The date the user record was created. Also referred to as the user’s '
                    + '"join date"',
                table_id: 3,
                special_type: null,
                name: 'CREATED_AT',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [
                    '0',
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                ],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '30',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '2',
                target: null,
                preview_display: true,
                display_name: 'Created At',
                fingerprint: {
                    global: {
                        'distinct-count': 980,
                    },
                },
                created_at: '2017-12-30T12:28:28.000Z',
                base_type: 'type/DateTime',
                points_of_interest: null,
            },
            {
                description: 'The contact email for the account.',
                table_id: 3,
                special_type: 'type/Email',
                name: 'EMAIL',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '24',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Email',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 1,
                            'average-length': 24.1664,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'A unique identifier given to each user.',
                table_id: 3,
                special_type: 'type/PK',
                name: 'ID',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '25',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'ID',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Number': {
                            min: 1,
                            max: 2500,
                            avg: 1250.5,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/BigInteger',
                points_of_interest: null,
            },
            {
                description: 'This is the latitude of the user on sign-up. It might be updated in '
                    + 'the future to the last seen location.',
                table_id: 3,
                special_type: 'type/Latitude',
                name: 'LATITUDE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [
                    '20',
                    '21',
                    '22',
                    '23',
                    '24',
                    '25',
                ],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '21',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '20',
                target: null,
                preview_display: true,
                display_name: 'Latitude',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Number': {
                            min: -89.62556830684696,
                            max: 89.9420089232147,
                            avg: 0.2000029228840675,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
                points_of_interest: null,
            },
            {
                description: 'This is the longitude of the user on sign-up. It might be updated in '
                    + 'the future to the last seen location.',
                table_id: 3,
                special_type: 'type/Longitude',
                name: 'LONGITUDE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [
                    '20',
                    '21',
                    '22',
                    '23',
                    '24',
                    '25',
                ],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '29',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '20',
                target: null,
                preview_display: true,
                display_name: 'Longitude',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Number': {
                            min: -179.693556788217,
                            max: 179.9427208287961,
                            avg: 0.7039410787134972,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
                points_of_interest: null,
            },
            {
                description: 'The name of the user who owns an account',
                table_id: 3,
                special_type: 'type/Name',
                name: 'NAME',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '26',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Name',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 13.516,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'This is the salted password of the user. It should not be visible',
                table_id: 3,
                special_type: null,
                name: 'PASSWORD',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '27',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Password',
                fingerprint: {
                    global: {
                        'distinct-count': 2500,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 36,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'The channel through which we acquired this user. Valid values include: '
                    + 'Affiliate, Facebook, Google, Organic and Twitter',
                table_id: 3,
                special_type: 'type/Category',
                name: 'SOURCE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '20',
                values: [
                    [
                        'Affiliate',
                    ],
                    [
                        'Facebook',
                    ],
                    [
                        'Google',
                    ],
                    [
                        'Organic',
                    ],
                    [
                        'Twitter',
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Source',
                fingerprint: {
                    global: {
                        'distinct-count': 5,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 7.4096,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'The state or province of the account’s billing address',
                table_id: 3,
                special_type: 'type/State',
                name: 'STATE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '28',
                values: [
                    [
                        'AA',
                    ],
                    [
                        'AE',
                    ],
                    [
                        'AK',
                    ],
                    [
                        'AL',
                    ],
                    [
                        'AP',
                    ],
                    [
                        'AR',
                    ],
                    [
                        'AS',
                    ],
                    [
                        'AZ',
                    ],
                    [
                        'CA',
                    ],
                    [
                        'CO',
                    ],
                    [
                        'CT',
                    ],
                    [
                        'DC',
                    ],
                    [
                        'DE',
                    ],
                    [
                        'FL',
                    ],
                    [
                        'FM',
                    ],
                    [
                        'GA',
                    ],
                    [
                        'GU',
                    ],
                    [
                        'HI',
                    ],
                    [
                        'IA',
                    ],
                    [
                        'ID',
                    ],
                    [
                        'IL',
                    ],
                    [
                        'IN',
                    ],
                    [
                        'KS',
                    ],
                    [
                        'KY',
                    ],
                    [
                        'LA',
                    ],
                    [
                        'MA',
                    ],
                    [
                        'MD',
                    ],
                    [
                        'ME',
                    ],
                    [
                        'MH',
                    ],
                    [
                        'MI',
                    ],
                    [
                        'MN',
                    ],
                    [
                        'MO',
                    ],
                    [
                        'MP',
                    ],
                    [
                        'MS',
                    ],
                    [
                        'MT',
                    ],
                    [
                        'NC',
                    ],
                    [
                        'ND',
                    ],
                    [
                        'NE',
                    ],
                    [
                        'NH',
                    ],
                    [
                        'NJ',
                    ],
                    [
                        'NM',
                    ],
                    [
                        'NV',
                    ],
                    [
                        'NY',
                    ],
                    [
                        'OH',
                    ],
                    [
                        'OK',
                    ],
                    [
                        'OR',
                    ],
                    [
                        'PA',
                    ],
                    [
                        'PR',
                    ],
                    [
                        'PW',
                    ],
                    [
                        'RI',
                    ],
                    [
                        'SC',
                    ],
                    [
                        'SD',
                    ],
                    [
                        'TN',
                    ],
                    [
                        'TX',
                    ],
                    [
                        'UT',
                    ],
                    [
                        'VA',
                    ],
                    [
                        'VI',
                    ],
                    [
                        'VT',
                    ],
                    [
                        'WA',
                    ],
                    [
                        'WI',
                    ],
                    [
                        'WV',
                    ],
                    [
                        'WY',
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'State',
                fingerprint: {
                    global: {
                        'distinct-count': 62,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 2,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
            {
                description: 'The postal code of the account’s billing address',
                table_id: 3,
                special_type: 'type/ZipCode',
                name: 'ZIP',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:01.000Z',
                active: true,
                parent_id: null,
                id: '23',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:34.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Zip',
                fingerprint: {
                    global: {
                        'distinct-count': 2467,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 5,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                points_of_interest: null,
            },
        ],
        caveats: null,
        segments: [],
        dimension_options: {
            0: {
                name: 'Minute',
                mbql: [
                    'datetime-field',
                    null,
                    'minute',
                ],
                type: 'type/DateTime',
            },
            1: {
                name: 'Hour',
                mbql: [
                    'datetime-field',
                    null,
                    'hour',
                ],
                type: 'type/DateTime',
            },
            2: {
                name: 'Day',
                mbql: [
                    'datetime-field',
                    null,
                    'day',
                ],
                type: 'type/DateTime',
            },
            3: {
                name: 'Week',
                mbql: [
                    'datetime-field',
                    null,
                    'week',
                ],
                type: 'type/DateTime',
            },
            4: {
                name: 'Month',
                mbql: [
                    'datetime-field',
                    null,
                    'month',
                ],
                type: 'type/DateTime',
            },
            5: {
                name: 'Quarter',
                mbql: [
                    'datetime-field',
                    null,
                    'quarter',
                ],
                type: 'type/DateTime',
            },
            6: {
                name: 'Year',
                mbql: [
                    'datetime-field',
                    null,
                    'year',
                ],
                type: 'type/DateTime',
            },
            7: {
                name: 'Minute of Hour',
                mbql: [
                    'datetime-field',
                    null,
                    'minute-of-hour',
                ],
                type: 'type/DateTime',
            },
            8: {
                name: 'Hour of Day',
                mbql: [
                    'datetime-field',
                    null,
                    'hour-of-day',
                ],
                type: 'type/DateTime',
            },
            9: {
                name: 'Day of Week',
                mbql: [
                    'datetime-field',
                    null,
                    'day-of-week',
                ],
                type: 'type/DateTime',
            },
            10: {
                name: 'Day of Month',
                mbql: [
                    'datetime-field',
                    null,
                    'day-of-month',
                ],
                type: 'type/DateTime',
            },
            11: {
                name: 'Day of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'day-of-year',
                ],
                type: 'type/DateTime',
            },
            12: {
                name: 'Week of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'week-of-year',
                ],
                type: 'type/DateTime',
            },
            13: {
                name: 'Month of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'month-of-year',
                ],
                type: 'type/DateTime',
            },
            14: {
                name: 'Quarter of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'quarter-of-year',
                ],
                type: 'type/DateTime',
            },
            15: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    null,
                    'default',
                ],
                type: 'type/Number',
            },
            16: {
                name: '10 bins',
                mbql: [
                    'binning-strategy',
                    null,
                    'num-bins',
                    10,
                ],
                type: 'type/Number',
            },
            17: {
                name: '50 bins',
                mbql: [
                    'binning-strategy',
                    null,
                    'num-bins',
                    50,
                ],
                type: 'type/Number',
            },
            18: {
                name: '100 bins',
                mbql: [
                    'binning-strategy',
                    null,
                    'num-bins',
                    100,
                ],
                type: 'type/Number',
            },
            19: {
                name: 'Don\'t bin',
                mbql: null,
                type: 'type/Number',
            },
            20: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    null,
                    'default',
                ],
                type: 'type/Coordinate',
            },
            21: {
                name: 'Bin every 1 degree',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    1,
                ],
                type: 'type/Coordinate',
            },
            22: {
                name: 'Bin every 10 degrees',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    10,
                ],
                type: 'type/Coordinate',
            },
            23: {
                name: 'Bin every 20 degrees',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    20,
                ],
                type: 'type/Coordinate',
            },
            24: {
                name: 'Bin every 50 degrees',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    50,
                ],
                type: 'type/Coordinate',
            },
            25: {
                name: 'Don\'t bin',
                mbql: null,
                type: 'type/Coordinate',
            },
        },
        rows: 2500,
        updated_at: '2018-10-26T07:50:02.000Z',
        entity_name: null,
        active: true,
        id: '3',
        db_id: 1,
        visibility_type: null,
        display_name: 'People',
        metrics: [],
        created_at: '2017-12-30T12:28:26.000Z',
        points_of_interest: null,
    },
    {
        description: 'This is our product catalog. It includes all products ever sold by the '
            + 'Sample Company.',
        entity_type: null,
        schema: '',
        raw_table_id: null,
        db: {
            description: null,
            features: [
                'basic-aggregations',
                'standard-deviation-aggregations',
                'expression-aggregations',
                'foreign-keys',
                'native-parameters',
                'nested-queries',
                'expressions',
                'binning',
            ],
            cache_field_values_schedule: '0 50 0 * * ? *',
            timezone: 'UTC',
            metadata_sync_schedule: '0 50 * * * ? *',
            name: 'Sample Dataset',
            caveats: null,
            is_full_sync: true,
            updated_at: '2017-12-30T12:28:26.000Z',
            is_sample: true,
            id: 1001,
            is_on_demand: false,
            engine: 'sqlite3',
            created_at: '2017-12-30T12:28:25.000Z',
            points_of_interest: null,
        },
        show_in_getting_started: false,
        name: 'PRODUCTS',
        fields: [
            {
                description: 'The type of product, valid values include: Doohicky, Gadget, Gizmo and '
                    + 'Widget',
                table_id: 1,
                special_type: 'type/Category',
                name: 'CATEGORY',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '4',
                values: [
                    [
                        'Doohickey',
                    ],
                    [
                        'Gadget',
                    ],
                    [
                        'Gizmo',
                    ],
                    [
                        'Widget',
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Category',
                fingerprint: {
                    global: {
                        'distinct-count': 4,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 6.515,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                has_field_values: 'list',
                points_of_interest: null,
            },
            {
                description: 'The date the product was added to our catalog.',
                table_id: 1,
                special_type: null,
                name: 'CREATED_AT',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [
                    '0',
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                ],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '8',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: '2',
                target: null,
                preview_display: true,
                display_name: 'Created At',
                fingerprint: {
                    global: {
                        'distinct-count': 178,
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/DateTime',
                has_field_values: 'none',
                points_of_interest: null,
            },
            {
                description: 'The international article number. A 13 digit number uniquely identifying '
                    + 'the product.',
                table_id: 1,
                special_type: 'type/Category',
                name: 'EAN',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '6',
                values: [
                    [
                        '0039085257443',
                    ],
                    [
                        '0080913806166',
                    ],
                    [
                        '0193622489091',
                    ],
                    [
                        '0223463079578',
                    ],
                    [
                        '0266395937202',
                    ],
                    [
                        '0345353072053',
                    ],
                    [
                        '0365814083336',
                    ],
                    [
                        '0367211708072',
                    ],
                    [
                        '0368413976627',
                    ],
                    [
                        '0542301596924',
                    ],
                    [
                        '0685731340146',
                    ],
                    [
                        '0705767545749',
                    ],
                    [
                        '0761352999939',
                    ],
                    [
                        '0768866991480',
                    ],
                    [
                        '0771359736086',
                    ],
                    [
                        '0874083316866',
                    ],
                    [
                        '0889005880561',
                    ],
                    [
                        '0893866879954',
                    ],
                    [
                        '1015531302836',
                    ],
                    [
                        '1049066362089',
                    ],
                    [
                        '1224623671624',
                    ],
                    [
                        '1339793375323',
                    ],
                    [
                        '1388680209272',
                    ],
                    [
                        '1406042374052',
                    ],
                    [
                        '1446345768745',
                    ],
                    [
                        '1452839798082',
                    ],
                    [
                        '1491752229662',
                    ],
                    [
                        '1505574116588',
                    ],
                    [
                        '1508850445877',
                    ],
                    [
                        '1516020229073',
                    ],
                    [
                        '1531651914891',
                    ],
                    [
                        '1642168927639',
                    ],
                    [
                        '1645391395251',
                    ],
                    [
                        '1655144037741',
                    ],
                    [
                        '1708125205773',
                    ],
                    [
                        '1766476263453',
                    ],
                    [
                        '1822816243483',
                    ],
                    [
                        '1852124531316',
                    ],
                    [
                        '1883906886751',
                    ],
                    [
                        '1996911472984',
                    ],
                    [
                        '2014138835135',
                    ],
                    [
                        '2209870433613',
                    ],
                    [
                        '2303521686448',
                    ],
                    [
                        '2381000747866',
                    ],
                    [
                        '2421804675634',
                    ],
                    [
                        '2504641492807',
                    ],
                    [
                        '2524683839644',
                    ],
                    [
                        '2588209409483',
                    ],
                    [
                        '2708977753988',
                    ],
                    [
                        '2763391997653',
                    ],
                    [
                        '2767465173936',
                    ],
                    [
                        '2781272028067',
                    ],
                    [
                        '2853743176888',
                    ],
                    [
                        '2907442406781',
                    ],
                    [
                        '2912871854997',
                    ],
                    [
                        '2933938753855',
                    ],
                    [
                        '2972612435526',
                    ],
                    [
                        '2973334477184',
                    ],
                    [
                        '3307417948800',
                    ],
                    [
                        '3318870462178',
                    ],
                    [
                        '3378896351771',
                    ],
                    [
                        '3420284197796',
                    ],
                    [
                        '3547213972603',
                    ],
                    [
                        '3571460165730',
                    ],
                    [
                        '3618315234996',
                    ],
                    [
                        '3726665749920',
                    ],
                    [
                        '3791564699805',
                    ],
                    [
                        '3821120393523',
                    ],
                    [
                        '3833568155977',
                    ],
                    [
                        '3851134627031',
                    ],
                    [
                        '3993080536989',
                    ],
                    [
                        '4081328904887',
                    ],
                    [
                        '4121133415908',
                    ],
                    [
                        '4140372254086',
                    ],
                    [
                        '4217164925995',
                    ],
                    [
                        '4244169498940',
                    ],
                    [
                        '4275475741130',
                    ],
                    [
                        '4292136596694',
                    ],
                    [
                        '4357503908101',
                    ],
                    [
                        '4369580649532',
                    ],
                    [
                        '4402403456587',
                    ],
                    [
                        '4406397607536',
                    ],
                    [
                        '4409896545323',
                    ],
                    [
                        '4427367186769',
                    ],
                    [
                        '4477284243922',
                    ],
                    [
                        '4525981056212',
                    ],
                    [
                        '4635505629089',
                    ],
                    [
                        '4764828838592',
                    ],
                    [
                        '4793726998855',
                    ],
                    [
                        '4878153242549',
                    ],
                    [
                        '4936845518034',
                    ],
                    [
                        '4949351374308',
                    ],
                    [
                        '5001299033491',
                    ],
                    [
                        '5022101985173',
                    ],
                    [
                        '5025373493292',
                    ],
                    [
                        '5045842207991',
                    ],
                    [
                        '5106999164275',
                    ],
                    [
                        '5135432624304',
                    ],
                    [
                        '5205582918803',
                    ],
                    [
                        '5220731053266',
                    ],
                    [
                        '5235839511235',
                    ],
                    [
                        '5286281563457',
                    ],
                    [
                        '5294833001159',
                    ],
                    [
                        '5331769886372',
                    ],
                    [
                        '5392970146631',
                    ],
                    [
                        '5475451470026',
                    ],
                    [
                        '5494860851417',
                    ],
                    [
                        '5555933000153',
                    ],
                    [
                        '5580300869798',
                    ],
                    [
                        '5738100232421',
                    ],
                    [
                        '5771515662766',
                    ],
                    [
                        '5790831995778',
                    ],
                    [
                        '5836607682838',
                    ],
                    [
                        '5849738425786',
                    ],
                    [
                        '5953453545565',
                    ],
                    [
                        '5992571249849',
                    ],
                    [
                        '6004321858993',
                    ],
                    [
                        '6009595045858',
                    ],
                    [
                        '6069592700273',
                    ],
                    [
                        '6107216519808',
                    ],
                    [
                        '6158779904327',
                    ],
                    [
                        '6283077633775',
                    ],
                    [
                        '6307260357597',
                    ],
                    [
                        '6311805265001',
                    ],
                    [
                        '6358563805650',
                    ],
                    [
                        '6377993273341',
                    ],
                    [
                        '6403448512601',
                    ],
                    [
                        '6411836466023',
                    ],
                    [
                        '6662556936864',
                    ],
                    [
                        '6715866263028',
                    ],
                    [
                        '6756086028295',
                    ],
                    [
                        '6759465970192',
                    ],
                    [
                        '6773200168873',
                    ],
                    [
                        '6785768996409',
                    ],
                    [
                        '6806962988325',
                    ],
                    [
                        '6888636255735',
                    ],
                    [
                        '6892017974863',
                    ],
                    [
                        '6945241085673',
                    ],
                    [
                        '7060763627923',
                    ],
                    [
                        '7078803677708',
                    ],
                    [
                        '7149275420373',
                    ],
                    [
                        '7152242635960',
                    ],
                    [
                        '7226803568773',
                    ],
                    [
                        '7233267484815',
                    ],
                    [
                        '7328496622456',
                    ],
                    [
                        '7397630510058',
                    ],
                    [
                        '7399080258054',
                    ],
                    [
                        '7468836405254',
                    ],
                    [
                        '7509342302566',
                    ],
                    [
                        '7638160392616',
                    ],
                    [
                        '7638187937920',
                    ],
                    [
                        '7668295014299',
                    ],
                    [
                        '7744759715596',
                    ],
                    [
                        '7813135596934',
                    ],
                    [
                        '7915145518217',
                    ],
                    [
                        '7980400187934',
                    ],
                    [
                        '8034622167300',
                    ],
                    [
                        '8044360565948',
                    ],
                    [
                        '8145725713704',
                    ],
                    [
                        '8196059660318',
                    ],
                    [
                        '8202158269472',
                    ],
                    [
                        '8324001438214',
                    ],
                    [
                        '8337714047154',
                    ],
                    [
                        '8445467670915',
                    ],
                    [
                        '8467810981561',
                    ],
                    [
                        '8506092970761',
                    ],
                    [
                        '8522169002524',
                    ],
                    [
                        '8544803456050',
                    ],
                    [
                        '8659959585728',
                    ],
                    [
                        '8696829800550',
                    ],
                    [
                        '8766792505780',
                    ],
                    [
                        '8798832944532',
                    ],
                    [
                        '8866752005524',
                    ],
                    [
                        '8918828811727',
                    ],
                    [
                        '8942633253045',
                    ],
                    [
                        '8997654116091',
                    ],
                    [
                        '9038416137967',
                    ],
                    [
                        '9048944276175',
                    ],
                    [
                        '9152915256001',
                    ],
                    [
                        '9160659225962',
                    ],
                    [
                        '9177560996713',
                    ],
                    [
                        '9208431506097',
                    ],
                    [
                        '9226368553359',
                    ],
                    [
                        '9268782064691',
                    ],
                    [
                        '9280806230653',
                    ],
                    [
                        '9298563671329',
                    ],
                    [
                        '9351022797197',
                    ],
                    [
                        '9368714668208',
                    ],
                    [
                        '9373361190880',
                    ],
                    [
                        '9485432568426',
                    ],
                    [
                        '9576606975147',
                    ],
                    [
                        '9676838418051',
                    ],
                    [
                        '9766308228812',
                    ],
                    [
                        '9790510096471',
                    ],
                    [
                        '9798212806893',
                    ],
                    [
                        '9821258471821',
                    ],
                    [
                        '9866709398234',
                    ],
                    [
                        '9867512877572',
                    ],
                    [
                        '9873669903514',
                    ],
                    [
                        '9908718200969',
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Ean',
                fingerprint: {
                    global: {
                        'distinct-count': 200,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 13,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                has_field_values: 'search',
                points_of_interest: null,
            },
            {
                description: 'The numerical product number. Only used internally. All external '
                    + 'communication should use the title or EAN.',
                table_id: 1,
                special_type: 'type/PK',
                name: 'ID',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '7',
                values: [],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'ID',
                fingerprint: {
                    global: {
                        'distinct-count': 200,
                    },
                    type: {
                        'type/Number': {
                            min: 1,
                            max: 200,
                            avg: 100.5,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/BigInteger',
                has_field_values: 'none',
                points_of_interest: null,
            },
            {
                description: 'The list price of the product. Note that this is not always the price '
                    + 'the product sold for due to discounts, promotions, etc.',
                table_id: 1,
                special_type: 'type/Category',
                name: 'PRICE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '3',
                values: [
                    [
                        31.478481306013133,
                    ],
                    [
                        31.570810009636254,
                    ],
                    [
                        31.652451471595953,
                    ],
                    [
                        31.911059974627456,
                    ],
                    [
                        32.70869907177724,
                    ],
                    [
                        32.898589374893895,
                    ],
                    [
                        33.025676392361675,
                    ],
                    [
                        33.0691911827434,
                    ],
                    [
                        33.819398489965096,
                    ],
                    [
                        34.105364212062945,
                    ],
                    [
                        34.43393775314257,
                    ],
                    [
                        34.436949038695666,
                    ],
                    [
                        34.49232271512591,
                    ],
                    [
                        34.56736283853321,
                    ],
                    [
                        34.880668682414466,
                    ],
                    [
                        35.635592389312286,
                    ],
                    [
                        35.74549478152516,
                    ],
                    [
                        35.75521338106141,
                    ],
                    [
                        35.92769914542002,
                    ],
                    [
                        35.952248064666584,
                    ],
                    [
                        36.113935460814055,
                    ],
                    [
                        36.24941295417628,
                    ],
                    [
                        36.43197524755833,
                    ],
                    [
                        36.576426018011375,
                    ],
                    [
                        36.662566102988634,
                    ],
                    [
                        36.667628590443265,
                    ],
                    [
                        36.8315845033702,
                    ],
                    [
                        37.4916156743209,
                    ],
                    [
                        37.50054912285667,
                    ],
                    [
                        37.51104057352921,
                    ],
                    [
                        37.58694011400149,
                    ],
                    [
                        37.594646461064336,
                    ],
                    [
                        37.60486200773139,
                    ],
                    [
                        37.67719675012089,
                    ],
                    [
                        37.73218428524255,
                    ],
                    [
                        37.791426878082675,
                    ],
                    [
                        37.811220557372984,
                    ],
                    [
                        37.85601333686656,
                    ],
                    [
                        37.94845958813752,
                    ],
                    [
                        37.96298977577792,
                    ],
                    [
                        37.98894313586699,
                    ],
                    [
                        38.0205525319929,
                    ],
                    [
                        38.035528592907845,
                    ],
                    [
                        38.130719028057385,
                    ],
                    [
                        38.31451770105359,
                    ],
                    [
                        38.36261236465027,
                    ],
                    [
                        38.530771412532054,
                    ],
                    [
                        38.632153021191456,
                    ],
                    [
                        38.734016450731,
                    ],
                    [
                        38.787559038256376,
                    ],
                    [
                        38.84841754699762,
                    ],
                    [
                        38.88799647512116,
                    ],
                    [
                        38.944466441884,
                    ],
                    [
                        39.25322706185918,
                    ],
                    [
                        39.32619763276276,
                    ],
                    [
                        39.56265445490145,
                    ],
                    [
                        39.58553358265947,
                    ],
                    [
                        39.80596129296891,
                    ],
                    [
                        39.87054068251135,
                    ],
                    [
                        40.2051093470279,
                    ],
                    [
                        40.31331966092042,
                    ],
                    [
                        40.32041964503478,
                    ],
                    [
                        40.35810004525205,
                    ],
                    [
                        40.66896152589008,
                    ],
                    [
                        41.03090483120909,
                    ],
                    [
                        41.1481376882523,
                    ],
                    [
                        41.17140033287684,
                    ],
                    [
                        41.18667746593478,
                    ],
                    [
                        41.28073406920983,
                    ],
                    [
                        41.42694094244828,
                    ],
                    [
                        41.5210183254109,
                    ],
                    [
                        41.62637911399191,
                    ],
                    [
                        41.72533330055108,
                    ],
                    [
                        41.977197384353474,
                    ],
                    [
                        41.99768273555296,
                    ],
                    [
                        42.10305088938356,
                    ],
                    [
                        42.182090760487554,
                    ],
                    [
                        42.19613680465525,
                    ],
                    [
                        42.33811838875554,
                    ],
                    [
                        42.42927150549158,
                    ],
                    [
                        42.67451373301769,
                    ],
                    [
                        42.707309941184384,
                    ],
                    [
                        43.01529809205257,
                    ],
                    [
                        43.173563567776505,
                    ],
                    [
                        43.2103027382,
                    ],
                    [
                        43.886199108750255,
                    ],
                    [
                        44.27660098813121,
                    ],
                    [
                        45.27950659499382,
                    ],
                    [
                        45.636058560044816,
                    ],
                    [
                        46.39669998721752,
                    ],
                    [
                        70.57172752885909,
                    ],
                    [
                        70.85354088313747,
                    ],
                    [
                        71.49771803710149,
                    ],
                    [
                        71.54905986372474,
                    ],
                    [
                        72.96596482189507,
                    ],
                    [
                        73.19639194666517,
                    ],
                    [
                        73.65401307301552,
                    ],
                    [
                        73.66117403551691,
                    ],
                    [
                        73.78693193241033,
                    ],
                    [
                        73.86901346585773,
                    ],
                    [
                        73.88838559870689,
                    ],
                    [
                        73.89678717043223,
                    ],
                    [
                        74.0138399886752,
                    ],
                    [
                        74.58681052876558,
                    ],
                    [
                        74.65141332152318,
                    ],
                    [
                        74.68622776780649,
                    ],
                    [
                        74.70210866809188,
                    ],
                    [
                        74.74689226628516,
                    ],
                    [
                        74.7964997316208,
                    ],
                    [
                        74.82929026272033,
                    ],
                    [
                        74.88386414053099,
                    ],
                    [
                        75.04592049484627,
                    ],
                    [
                        75.17171353141414,
                    ],
                    [
                        75.39375636423622,
                    ],
                    [
                        75.4342325044977,
                    ],
                    [
                        75.5782958575026,
                    ],
                    [
                        75.88211088644746,
                    ],
                    [
                        75.93086065603255,
                    ],
                    [
                        76.03222871167812,
                    ],
                    [
                        76.11172770427238,
                    ],
                    [
                        76.27011260579093,
                    ],
                    [
                        76.36732260150961,
                    ],
                    [
                        76.37233456963133,
                    ],
                    [
                        76.39738017921495,
                    ],
                    [
                        76.50982131543958,
                    ],
                    [
                        76.75676766286642,
                    ],
                    [
                        76.87563519011165,
                    ],
                    [
                        76.92389684029916,
                    ],
                    [
                        76.92412015015292,
                    ],
                    [
                        76.95459498316461,
                    ],
                    [
                        77.05782495356112,
                    ],
                    [
                        77.10190755218902,
                    ],
                    [
                        77.15030490645287,
                    ],
                    [
                        77.18941333730268,
                    ],
                    [
                        77.28338940628402,
                    ],
                    [
                        77.48761028910431,
                    ],
                    [
                        77.55322688338404,
                    ],
                    [
                        77.56998383601317,
                    ],
                    [
                        77.6659600642541,
                    ],
                    [
                        77.75816139077654,
                    ],
                    [
                        77.87389118856868,
                    ],
                    [
                        77.87659600422019,
                    ],
                    [
                        77.99080121856078,
                    ],
                    [
                        78.0020983595424,
                    ],
                    [
                        78.00649355101187,
                    ],
                    [
                        78.00808522357906,
                    ],
                    [
                        78.0479937898473,
                    ],
                    [
                        78.04882374525098,
                    ],
                    [
                        78.06820055605543,
                    ],
                    [
                        78.07786359395314,
                    ],
                    [
                        78.08888786194562,
                    ],
                    [
                        78.1516484463319,
                    ],
                    [
                        78.27517848484804,
                    ],
                    [
                        78.35732113629447,
                    ],
                    [
                        78.38652808932241,
                    ],
                    [
                        78.54353607418858,
                    ],
                    [
                        78.59287204406947,
                    ],
                    [
                        78.59568620391565,
                    ],
                    [
                        78.70360294051893,
                    ],
                    [
                        78.81795404987895,
                    ],
                    [
                        79.01689858710748,
                    ],
                    [
                        79.0724098818133,
                    ],
                    [
                        79.14646416328823,
                    ],
                    [
                        79.2442446862764,
                    ],
                    [
                        79.25531873456106,
                    ],
                    [
                        79.3130107093201,
                    ],
                    [
                        79.34262029662838,
                    ],
                    [
                        79.40012710936679,
                    ],
                    [
                        79.50137062205391,
                    ],
                    [
                        79.53684798637575,
                    ],
                    [
                        79.55169756930505,
                    ],
                    [
                        79.59561366773278,
                    ],
                    [
                        79.61734293982114,
                    ],
                    [
                        79.71099776706703,
                    ],
                    [
                        79.82183989845656,
                    ],
                    [
                        80.04228684378334,
                    ],
                    [
                        80.06978661922757,
                    ],
                    [
                        80.08487588471849,
                    ],
                    [
                        80.23650200456913,
                    ],
                    [
                        80.392555805425,
                    ],
                    [
                        80.73348397034569,
                    ],
                    [
                        80.76530495656675,
                    ],
                    [
                        80.8113290846362,
                    ],
                    [
                        81.08314048886325,
                    ],
                    [
                        81.09828122135757,
                    ],
                    [
                        81.13091321632862,
                    ],
                    [
                        81.17813780639051,
                    ],
                    [
                        81.24207889711256,
                    ],
                    [
                        81.39731133780502,
                    ],
                    [
                        81.51127185581468,
                    ],
                    [
                        81.70338010030035,
                    ],
                    [
                        81.81966926973539,
                    ],
                    [
                        82.31933304702872,
                    ],
                    [
                        82.4778426224166,
                    ],
                    [
                        82.5660847281631,
                    ],
                    [
                        83.00775268524082,
                    ],
                    [
                        83.15002372588783,
                    ],
                    [
                        83.57855921489288,
                    ],
                    [
                        85.57515960004623,
                    ],
                    [
                        86.04566142585628,
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Price',
                fingerprint: {
                    global: {
                        'distinct-count': 200,
                    },
                    type: {
                        'type/Number': {
                            min: 31.478481306013133,
                            max: 86.04566142585628,
                            avg: 60.18227577428187,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
                has_field_values: 'list',
                points_of_interest: null,
            },
            {
                description: 'The average rating users have given the product. This ranges from 1 - 5',
                table_id: 1,
                special_type: 'type/Category',
                name: 'RATING',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '5',
                values: [
                    [
                        0,
                    ],
                    [
                        2,
                    ],
                    [
                        2.5,
                    ],
                    [
                        2.7,
                    ],
                    [
                        3,
                    ],
                    [
                        3.1,
                    ],
                    [
                        3.2,
                    ],
                    [
                        3.3,
                    ],
                    [
                        3.4,
                    ],
                    [
                        3.5,
                    ],
                    [
                        3.6,
                    ],
                    [
                        3.7,
                    ],
                    [
                        3.8,
                    ],
                    [
                        4,
                    ],
                    [
                        4.1,
                    ],
                    [
                        4.2,
                    ],
                    [
                        4.3,
                    ],
                    [
                        4.4,
                    ],
                    [
                        4.5,
                    ],
                    [
                        4.6,
                    ],
                    [
                        4.7,
                    ],
                    [
                        4.8,
                    ],
                    [
                        5,
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Rating',
                fingerprint: {
                    global: {
                        'distinct-count': 23,
                    },
                    type: {
                        'type/Number': {
                            min: 0,
                            max: 5,
                            avg: 3.928500000000003,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Float',
                has_field_values: 'list',
                points_of_interest: null,
            },
            {
                description: 'The name of the product as it should be displayed to customers.',
                table_id: 1,
                special_type: 'type/Category',
                name: 'TITLE',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '2',
                values: [
                    [
                        'Aerodynamic Copper Watch',
                    ],
                    [
                        'Aerodynamic Cotton Keyboard',
                    ],
                    [
                        'Aerodynamic Cotton Plate',
                    ],
                    [
                        'Aerodynamic Granite Coat',
                    ],
                    [
                        'Aerodynamic Linen Coat',
                    ],
                    [
                        'Aerodynamic Linen Pants',
                    ],
                    [
                        'Aerodynamic Paper Computer',
                    ],
                    [
                        'Aerodynamic Steel Clock',
                    ],
                    [
                        'Awesome Copper Keyboard',
                    ],
                    [
                        'Awesome Cotton Bottle',
                    ],
                    [
                        'Awesome Cotton Table',
                    ],
                    [
                        'Awesome Granite Lamp',
                    ],
                    [
                        'Awesome Iron Chair',
                    ],
                    [
                        'Awesome Iron Lamp',
                    ],
                    [
                        'Awesome Iron Pants',
                    ],
                    [
                        'Awesome Iron Table',
                    ],
                    [
                        'Awesome Linen Plate',
                    ],
                    [
                        'Awesome Marble Shirt',
                    ],
                    [
                        'Awesome Wooden Car',
                    ],
                    [
                        'Awesome Wooden Pants',
                    ],
                    [
                        'Durable Bronze Lamp',
                    ],
                    [
                        'Durable Concrete Hat',
                    ],
                    [
                        'Durable Concrete Shoes',
                    ],
                    [
                        'Durable Cotton Shoes',
                    ],
                    [
                        'Durable Iron Clock',
                    ],
                    [
                        'Durable Iron Coat',
                    ],
                    [
                        'Durable Leather Gloves',
                    ],
                    [
                        'Durable Leather Plate',
                    ],
                    [
                        'Durable Linen Coat',
                    ],
                    [
                        'Durable Rubber Clock',
                    ],
                    [
                        'Durable Steel Bag',
                    ],
                    [
                        'Durable Wooden Bottle',
                    ],
                    [
                        'Durable Wooden Hat',
                    ],
                    [
                        'Durable Wool Coat',
                    ],
                    [
                        'Enormous Bronze Shoes',
                    ],
                    [
                        'Enormous Bronze Toucan',
                    ],
                    [
                        'Enormous Copper Bag',
                    ],
                    [
                        'Enormous Copper Hat',
                    ],
                    [
                        'Enormous Copper Pants',
                    ],
                    [
                        'Enormous Granite Car',
                    ],
                    [
                        'Enormous Granite Computer',
                    ],
                    [
                        'Enormous Granite Shirt',
                    ],
                    [
                        'Enormous Iron Coat',
                    ],
                    [
                        'Enormous Linen Knife',
                    ],
                    [
                        'Enormous Marble Bag',
                    ],
                    [
                        'Enormous Marble Clock',
                    ],
                    [
                        'Enormous Paper Hat',
                    ],
                    [
                        'Enormous Paper Pants',
                    ],
                    [
                        'Enormous Paper Toucan',
                    ],
                    [
                        'Enormous Wooden Knife',
                    ],
                    [
                        'Enormous Wooden Table',
                    ],
                    [
                        'Enormous Wool Watch',
                    ],
                    [
                        'Ergonomic Concrete Chair',
                    ],
                    [
                        'Ergonomic Concrete Shoes',
                    ],
                    [
                        'Ergonomic Copper Pants',
                    ],
                    [
                        'Ergonomic Copper Plate',
                    ],
                    [
                        'Ergonomic Copper Shirt',
                    ],
                    [
                        'Ergonomic Cotton Bench',
                    ],
                    [
                        'Ergonomic Granite Lamp',
                    ],
                    [
                        'Ergonomic Iron Hat',
                    ],
                    [
                        'Ergonomic Marble Table',
                    ],
                    [
                        'Ergonomic Plastic Gloves',
                    ],
                    [
                        'Ergonomic Rubber Bottle',
                    ],
                    [
                        'Ergonomic Wooden Bag',
                    ],
                    [
                        'Ergonomic Wool Shirt',
                    ],
                    [
                        'Ergonomic Wool Table',
                    ],
                    [
                        'Fantastic Bronze Hat',
                    ],
                    [
                        'Fantastic Concrete Hat',
                    ],
                    [
                        'Fantastic Copper Bag',
                    ],
                    [
                        'Fantastic Copper Hat',
                    ],
                    [
                        'Fantastic Copper Watch',
                    ],
                    [
                        'Fantastic Granite Plate',
                    ],
                    [
                        'Fantastic Linen Clock',
                    ],
                    [
                        'Fantastic Linen Table',
                    ],
                    [
                        'Fantastic Marble Gloves',
                    ],
                    [
                        'Fantastic Silk Table',
                    ],
                    [
                        'Fantastic Silk Watch',
                    ],
                    [
                        'Fantastic Wool Bottle',
                    ],
                    [
                        'Fantastic Wool Clock',
                    ],
                    [
                        'Gorgeous Bronze Wallet',
                    ],
                    [
                        'Gorgeous Copper Bag',
                    ],
                    [
                        'Gorgeous Cotton Hat',
                    ],
                    [
                        'Gorgeous Cotton Shirt',
                    ],
                    [
                        'Gorgeous Granite Car',
                    ],
                    [
                        'Gorgeous Granite Chair',
                    ],
                    [
                        'Gorgeous Iron Clock',
                    ],
                    [
                        'Gorgeous Linen Chair',
                    ],
                    [
                        'Gorgeous Rubber Pants',
                    ],
                    [
                        'Gorgeous Wool Toucan',
                    ],
                    [
                        'Heavy-Duty Granite Chair',
                    ],
                    [
                        'Heavy-Duty Granite Clock',
                    ],
                    [
                        'Heavy-Duty Linen Plate',
                    ],
                    [
                        'Heavy-Duty Marble Plate',
                    ],
                    [
                        'Heavy-Duty Paper Pants',
                    ],
                    [
                        'Heavy-Duty Paper Watch',
                    ],
                    [
                        'Heavy-Duty Plastic Coat',
                    ],
                    [
                        'Heavy-Duty Steel Car',
                    ],
                    [
                        'Heavy-Duty Wool Coat',
                    ],
                    [
                        'Heavy-Duty Wool Hat',
                    ],
                    [
                        'Incredible Bronze Gloves',
                    ],
                    [
                        'Incredible Copper Lamp',
                    ],
                    [
                        'Incredible Copper Pants',
                    ],
                    [
                        'Incredible Cotton Chair',
                    ],
                    [
                        'Incredible Cotton Computer',
                    ],
                    [
                        'Incredible Granite Pants',
                    ],
                    [
                        'Incredible Linen Hat',
                    ],
                    [
                        'Incredible Rubber Keyboard',
                    ],
                    [
                        'Incredible Silk Computer',
                    ],
                    [
                        'Incredible Steel Coat',
                    ],
                    [
                        'Incredible Wool Wallet',
                    ],
                    [
                        'Intelligent Aluminum Bench',
                    ],
                    [
                        'Intelligent Aluminum Car',
                    ],
                    [
                        'Intelligent Bronze Chair',
                    ],
                    [
                        'Intelligent Leather Gloves',
                    ],
                    [
                        'Intelligent Plastic Chair',
                    ],
                    [
                        'Intelligent Plastic Keyboard',
                    ],
                    [
                        'Intelligent Rubber Bottle',
                    ],
                    [
                        'Intelligent Steel Clock',
                    ],
                    [
                        'Lightweight Aluminum Bench',
                    ],
                    [
                        'Lightweight Bronze Coat',
                    ],
                    [
                        'Lightweight Bronze Shirt',
                    ],
                    [
                        'Lightweight Bronze Table',
                    ],
                    [
                        'Lightweight Bronze Toucan',
                    ],
                    [
                        'Lightweight Concrete Knife',
                    ],
                    [
                        'Lightweight Granite Computer',
                    ],
                    [
                        'Lightweight Granite Shoes',
                    ],
                    [
                        'Lightweight Iron Pants',
                    ],
                    [
                        'Lightweight Leather Coat',
                    ],
                    [
                        'Lightweight Plastic Wallet',
                    ],
                    [
                        'Lightweight Rubber Computer',
                    ],
                    [
                        'Lightweight Rubber Shoes',
                    ],
                    [
                        'Lightweight Wooden Shoes',
                    ],
                    [
                        'Mediocre Bronze Computer',
                    ],
                    [
                        'Mediocre Bronze Gloves',
                    ],
                    [
                        'Mediocre Concrete Car',
                    ],
                    [
                        'Mediocre Concrete Knife',
                    ],
                    [
                        'Mediocre Copper Car',
                    ],
                    [
                        'Mediocre Cotton Knife',
                    ],
                    [
                        'Mediocre Linen Shoes',
                    ],
                    [
                        'Mediocre Marble Shoes',
                    ],
                    [
                        'Mediocre Silk Plate',
                    ],
                    [
                        'Mediocre Wooden Coat',
                    ],
                    [
                        'Mediocre Wooden Gloves',
                    ],
                    [
                        'Practical Aluminum Computer',
                    ],
                    [
                        'Practical Bronze Bottle',
                    ],
                    [
                        'Practical Concrete Table',
                    ],
                    [
                        'Practical Concrete Wallet',
                    ],
                    [
                        'Practical Concrete Watch',
                    ],
                    [
                        'Practical Cotton Shoes',
                    ],
                    [
                        'Practical Granite Bench',
                    ],
                    [
                        'Practical Granite Pants',
                    ],
                    [
                        'Practical Iron Car',
                    ],
                    [
                        'Practical Leather Bag',
                    ],
                    [
                        'Practical Paper Wallet',
                    ],
                    [
                        'Practical Plastic Car',
                    ],
                    [
                        'Practical Rubber Computer',
                    ],
                    [
                        'Practical Steel Shirt',
                    ],
                    [
                        'Practical Steel Table',
                    ],
                    [
                        'Practical Wooden Gloves',
                    ],
                    [
                        'Rustic Aluminum Computer',
                    ],
                    [
                        'Rustic Bronze Pants',
                    ],
                    [
                        'Rustic Iron Chair',
                    ],
                    [
                        'Rustic Leather Chair',
                    ],
                    [
                        'Rustic Leather Plate',
                    ],
                    [
                        'Rustic Linen Wallet',
                    ],
                    [
                        'Rustic Marble Shirt',
                    ],
                    [
                        'Rustic Rubber Bottle',
                    ],
                    [
                        'Rustic Steel Knife',
                    ],
                    [
                        'Sleek Aluminum Toucan',
                    ],
                    [
                        'Sleek Copper Bag',
                    ],
                    [
                        'Sleek Copper Bench',
                    ],
                    [
                        'Sleek Cotton Car',
                    ],
                    [
                        'Sleek Iron Bag',
                    ],
                    [
                        'Sleek Leather Gloves',
                    ],
                    [
                        'Sleek Steel Watch',
                    ],
                    [
                        'Small Bronze Bottle',
                    ],
                    [
                        'Small Bronze Hat',
                    ],
                    [
                        'Small Concrete Shirt',
                    ],
                    [
                        'Small Cotton Knife',
                    ],
                    [
                        'Small Iron Toucan',
                    ],
                    [
                        'Small Leather Clock',
                    ],
                    [
                        'Small Linen Keyboard',
                    ],
                    [
                        'Small Marble Chair',
                    ],
                    [
                        'Small Paper Chair',
                    ],
                    [
                        'Small Rubber Bench',
                    ],
                    [
                        'Small Rubber Hat',
                    ],
                    [
                        'Small Rubber Table',
                    ],
                    [
                        'Small Wooden Shirt',
                    ],
                    [
                        'Small Wool Chair',
                    ],
                    [
                        'Synergistic Aluminum Wallet',
                    ],
                    [
                        'Synergistic Bronze Coat',
                    ],
                    [
                        'Synergistic Cotton Bench',
                    ],
                    [
                        'Synergistic Paper Shirt',
                    ],
                    [
                        'Synergistic Rubber Knife',
                    ],
                    [
                        'Synergistic Steel Bag',
                    ],
                    [
                        'Synergistic Steel Clock',
                    ],
                    [
                        'Synergistic Wool Chair',
                    ],
                    [
                        'Synergistic Wool Gloves',
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Title',
                fingerprint: {
                    global: {
                        'distinct-count': 198,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 21.305,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                has_field_value: 'list',
                points_of_interest: null,
            },
            {
                description: 'The source of the product.',
                table_id: 1,
                special_type: 'type/Category',
                name: 'VENDOR',
                fingerprint_version: 1,
                caveats: null,
                fk_target_field_id: null,
                dimensions: [],
                dimension_options: [],
                updated_at: '2018-10-26T07:50:00.000Z',
                active: true,
                parent_id: null,
                id: '1',
                values: [
                    [
                        'Akeem Raynor Inc',
                    ],
                    [
                        'America Brakus Inc',
                    ],
                    [
                        'Angie Kautzer and Sons',
                    ],
                    [
                        'Armstrong-Abbott',
                    ],
                    [
                        'Arthur Stanton Jr. Group',
                    ],
                    [
                        'Ashton Abshire LLC',
                    ],
                    [
                        'Barton, Effertz and Weissnat',
                    ],
                    [
                        'Bayer, Bernier and Ebert',
                    ],
                    [
                        'Becker, Daniel and Swift',
                    ],
                    [
                        'Bednar, Boyle and Powlowski',
                    ],
                    [
                        'Bednar, Mayer and Bailey',
                    ],
                    [
                        'Bednar-Metz',
                    ],
                    [
                        'Beer-Treutel',
                    ],
                    [
                        'Benjamin West and Sons',
                    ],
                    [
                        'Bergstrom-Hudson',
                    ],
                    [
                        'Bessie Zemlak Group',
                    ],
                    [
                        'Blanda-Huel',
                    ],
                    [
                        'Caleb Considine LLC',
                    ],
                    [
                        'Chadd Brekke and Sons',
                    ],
                    [
                        'Chadd Champlin Inc',
                    ],
                    [
                        'Champlin-Wisoky',
                    ],
                    [
                        'Clair Carroll DDS LLC',
                    ],
                    [
                        'Cole, Hagenes and Stoltenberg',
                    ],
                    [
                        'Collins-Vandervort',
                    ],
                    [
                        'Connelly-Quigley',
                    ],
                    [
                        'Conroy, Muller and Swaniawski',
                    ],
                    [
                        'Conroy-Deckow',
                    ],
                    [
                        'Considine, Collier and Bergstrom',
                    ],
                    [
                        'Corine Roberts Group',
                    ],
                    [
                        'Cornelius Price Group',
                    ],
                    [
                        'Crist-Quitzon',
                    ],
                    [
                        'Cummings-Schroeder',
                    ],
                    [
                        'D\'Amore-Conn',
                    ],
                    [
                        'Darryl Krajcik Inc',
                    ],
                    [
                        'Declan Jenkins MD LLC',
                    ],
                    [
                        'Delaney Russel Inc',
                    ],
                    [
                        'Desiree White Group',
                    ],
                    [
                        'Devante Kilback Group',
                    ],
                    [
                        'Dibbert-Christiansen',
                    ],
                    [
                        'Dickinson, O\'Connell and Flatley',
                    ],
                    [
                        'Dietrich, Block and Block',
                    ],
                    [
                        'Dietrich-Crooks',
                    ],
                    [
                        'Dr. Joesph Schiller and Sons',
                    ],
                    [
                        'Dr. Kristoffer Kling and Sons',
                    ],
                    [
                        'Dr. Virgil Abbott LLC',
                    ],
                    [
                        'DuBuque-Farrell',
                    ],
                    [
                        'Emile Pacocha LLC',
                    ],
                    [
                        'Emmitt Barton LLC',
                    ],
                    [
                        'Erdman-Homenick',
                    ],
                    [
                        'Erna King Inc',
                    ],
                    [
                        'Ernestine Rau DDS Inc',
                    ],
                    [
                        'Eusebio Stehr Group',
                    ],
                    [
                        'Fadel, Kertzmann and Wehner',
                    ],
                    [
                        'Feest-Kreiger',
                    ],
                    [
                        'Felicita Legros Inc',
                    ],
                    [
                        'Flatley-McCullough',
                    ],
                    [
                        'Funk-Nolan',
                    ],
                    [
                        'Gaetano Macejkovic Group',
                    ],
                    [
                        'Genevieve Grimes LLC',
                    ],
                    [
                        'Gerhold-Hessel',
                    ],
                    [
                        'Gorczany, Haag and Hansen',
                    ],
                    [
                        'Graham, Feest and Russel',
                    ],
                    [
                        'Grant-Beer',
                    ],
                    [
                        'Grimes, Ziemann and Jenkins',
                    ],
                    [
                        'Gulgowski, Harris and Ortiz',
                    ],
                    [
                        'Gutkowski, Mante and Prosacco',
                    ],
                    [
                        'Gutmann, Hintz and Olson',
                    ],
                    [
                        'Gutmann, Nicolas and Donnelly',
                    ],
                    [
                        'Haag-Stroman',
                    ],
                    [
                        'Haley, Jast and Quitzon',
                    ],
                    [
                        'Hallie Mayert and Sons',
                    ],
                    [
                        'Hane, Murphy and Schaden',
                    ],
                    [
                        'Harber-Greenfelder',
                    ],
                    [
                        'Hartmann, Sawayn and Zboncak',
                    ],
                    [
                        'Hauck-Orn',
                    ],
                    [
                        'Hellen Langworth and Sons',
                    ],
                    [
                        'Herman Grant Group',
                    ],
                    [
                        'Hettinger-Ruecker',
                    ],
                    [
                        'Hills-Morar',
                    ],
                    [
                        'Hirthe-Donnelly',
                    ],
                    [
                        'Hoppe, Bogan and Strosin',
                    ],
                    [
                        'Hoppe-Abbott',
                    ],
                    [
                        'Irwin Jones and Sons',
                    ],
                    [
                        'Jacobi, VonRueden and Schoen',
                    ],
                    [
                        'Jenkins, Bins and Cormier',
                    ],
                    [
                        'Jenkins, Hahn and Daugherty',
                    ],
                    [
                        'Jerel Larkin Inc',
                    ],
                    [
                        'Jermey Smitham Group',
                    ],
                    [
                        'Jewell Kiehn LLC',
                    ],
                    [
                        'Jimmy Hirthe Group',
                    ],
                    [
                        'Joseph Reynolds and Sons',
                    ],
                    [
                        'Jovan Fay I and Sons',
                    ],
                    [
                        'Judd Turner Sr. Group',
                    ],
                    [
                        'Julian Gutkowski and Sons',
                    ],
                    [
                        'Junius Harris and Sons',
                    ],
                    [
                        'Juvenal Koch and Sons',
                    ],
                    [
                        'Kenneth Huels and Sons',
                    ],
                    [
                        'King-Stark',
                    ],
                    [
                        'Kody Windler Group',
                    ],
                    [
                        'Koepp-Conn',
                    ],
                    [
                        'Koss-Runte',
                    ],
                    [
                        'Kulas, Witting and Thiel',
                    ],
                    [
                        'Kylee Prosacco and Sons',
                    ],
                    [
                        'Lehner-Fadel',
                    ],
                    [
                        'Leonardo Ziemann and Sons',
                    ],
                    [
                        'Lind, Dicki and Hartmann',
                    ],
                    [
                        'Lindgren-Wuckert',
                    ],
                    [
                        'Lockman, Anderson and Ernser',
                    ],
                    [
                        'Lockman-Raynor',
                    ],
                    [
                        'Lonny Rice Group',
                    ],
                    [
                        'Lorena Mayer LLC',
                    ],
                    [
                        'Lorenzo Schneider LLC',
                    ],
                    [
                        'Lowe-Heaney',
                    ],
                    [
                        'Lueilwitz-Kuhn',
                    ],
                    [
                        'Lueilwitz-Runolfsson',
                    ],
                    [
                        'Lueilwitz-Witting',
                    ],
                    [
                        'Luettgen, Kilback and Satterfield',
                    ],
                    [
                        'Luettgen, Lindgren and Rau',
                    ],
                    [
                        'Lynch, Schultz and Bashirian',
                    ],
                    [
                        'Madisyn Kulas Inc',
                    ],
                    [
                        'Maggio-Gerlach',
                    ],
                    [
                        'Marcel Moore Inc',
                    ],
                    [
                        'Marguerite Klocko LLC',
                    ],
                    [
                        'Marlene Bergstrom Group',
                    ],
                    [
                        'Maxime Ryan LLC',
                    ],
                    [
                        'Mazie Anderson Jr. Group',
                    ],
                    [
                        'McCullough-VonRueden',
                    ],
                    [
                        'McGlynn-Gorczany',
                    ],
                    [
                        'McGlynn-Orn',
                    ],
                    [
                        'Miller, Marvin and Sauer',
                    ],
                    [
                        'Miss Sherwood Rice LLC',
                    ],
                    [
                        'Moen, Oberbrunner and Shields',
                    ],
                    [
                        'Mortimer Jakubowski LLC',
                    ],
                    [
                        'Mosciski-Glover',
                    ],
                    [
                        'Mrs. Ervin Nader LLC',
                    ],
                    [
                        'Mrs. Warren Metz Inc',
                    ],
                    [
                        'Mueller, Bartoletti and Lind',
                    ],
                    [
                        'Mueller, Littel and Hilll',
                    ],
                    [
                        'Murphy-Crooks',
                    ],
                    [
                        'Nitzsche-Wolff',
                    ],
                    [
                        'Nolan-Raynor',
                    ],
                    [
                        'O\'Connell, Schaden and Rogahn',
                    ],
                    [
                        'O\'Kon, Dooley and Mante',
                    ],
                    [
                        'Oberbrunner-Stiedemann',
                    ],
                    [
                        'Okuneva, Watsica and Weber',
                    ],
                    [
                        'Osinski-Fay',
                    ],
                    [
                        'Osinski-Streich',
                    ],
                    [
                        'Parisian-Borer',
                    ],
                    [
                        'Percival Kautzer Group',
                    ],
                    [
                        'Pfeffer-VonRueden',
                    ],
                    [
                        'Pouros, Morissette and Lowe',
                    ],
                    [
                        'Predovic-Bergnaum',
                    ],
                    [
                        'Reichel, Jast and O\'Hara',
                    ],
                    [
                        'Robel-Ledner',
                    ],
                    [
                        'Roberts, Dicki and Upton',
                    ],
                    [
                        'Roob, Smith and Hettinger',
                    ],
                    [
                        'Rosina Senger II LLC',
                    ],
                    [
                        'Russel, Jacobi and Keeling',
                    ],
                    [
                        'Rutherford, Carter and Muller',
                    ],
                    [
                        'Ryan Streich Sr. Group',
                    ],
                    [
                        'Samara Boyer and Sons',
                    ],
                    [
                        'Sanford Feil Inc',
                    ],
                    [
                        'Schaden-Muller',
                    ],
                    [
                        'Schamberger-Bins',
                    ],
                    [
                        'Schiller, Hyatt and Beahan',
                    ],
                    [
                        'Schmeler-Brown',
                    ],
                    [
                        'Schneider, Glover and Zieme',
                    ],
                    [
                        'Schoen-Franecki',
                    ],
                    [
                        'Schulist, Hackett and Bartell',
                    ],
                    [
                        'Schumm, Hegmann and White',
                    ],
                    [
                        'Schumm-Parisian',
                    ],
                    [
                        'Schuster, Kunde and Swaniawski',
                    ],
                    [
                        'Schuster-Crooks',
                    ],
                    [
                        'Shanahan-Kertzmann',
                    ],
                    [
                        'Sipes-Koch',
                    ],
                    [
                        'Smitham-Feil',
                    ],
                    [
                        'Sporer, Weber and Brown',
                    ],
                    [
                        'Stamm-Volkman',
                    ],
                    [
                        'Stanton-Emmerich',
                    ],
                    [
                        'Stroman, Vandervort and Schneider',
                    ],
                    [
                        'Sydnie Toy Inc',
                    ],
                    [
                        'Treutel, Kuhlman and Ernser',
                    ],
                    [
                        'Ursula Rohan and Sons',
                    ],
                    [
                        'Vada Predovic and Sons',
                    ],
                    [
                        'Veronica Hettinger and Sons',
                    ],
                    [
                        'Veum, Schultz and Kub',
                    ],
                    [
                        'Waelchi, Schuppe and McKenzie',
                    ],
                    [
                        'Walker-Stoltenberg',
                    ],
                    [
                        'Walter-White',
                    ],
                    [
                        'Watsica-Kertzmann',
                    ],
                    [
                        'Weissnat-Christiansen',
                    ],
                    [
                        'Welch-Bernhard',
                    ],
                    [
                        'White, Fisher and Walker',
                    ],
                    [
                        'White-Lesch',
                    ],
                    [
                        'Wilkinson, Walsh and Quigley',
                    ],
                    [
                        'Willms, Wolff and Heathcote',
                    ],
                    [
                        'Wolf-Wisozk',
                    ],
                    [
                        'Wyman-Torphy',
                    ],
                    [
                        'Zboncak-Tremblay',
                    ],
                    [
                        'Ziemann, Yundt and Murazik',
                    ],
                ],
                raw_column_id: null,
                last_analyzed: '2017-12-30T12:28:30.000Z',
                position: 0,
                visibility_type: 'normal',
                default_dimension_option: null,
                target: null,
                preview_display: true,
                display_name: 'Vendor',
                fingerprint: {
                    global: {
                        'distinct-count': 200,
                    },
                    type: {
                        'type/Text': {
                            'percent-json': 0,
                            'percent-url': 0,
                            'percent-email': 0,
                            'average-length': 19.915,
                        },
                    },
                },
                created_at: '2017-12-30T12:28:27.000Z',
                base_type: 'type/Text',
                has_field_values: 'list',
                points_of_interest: null,
            },
        ],
        caveats: null,
        segments: [],
        dimension_options: {
            0: {
                name: 'Minute',
                mbql: [
                    'datetime-field',
                    null,
                    'minute',
                ],
                type: 'type/DateTime',
            },
            1: {
                name: 'Hour',
                mbql: [
                    'datetime-field',
                    null,
                    'hour',
                ],
                type: 'type/DateTime',
            },
            2: {
                name: 'Day',
                mbql: [
                    'datetime-field',
                    null,
                    'day',
                ],
                type: 'type/DateTime',
            },
            3: {
                name: 'Week',
                mbql: [
                    'datetime-field',
                    null,
                    'week',
                ],
                type: 'type/DateTime',
            },
            4: {
                name: 'Month',
                mbql: [
                    'datetime-field',
                    null,
                    'month',
                ],
                type: 'type/DateTime',
            },
            5: {
                name: 'Quarter',
                mbql: [
                    'datetime-field',
                    null,
                    'quarter',
                ],
                type: 'type/DateTime',
            },
            6: {
                name: 'Year',
                mbql: [
                    'datetime-field',
                    null,
                    'year',
                ],
                type: 'type/DateTime',
            },
            7: {
                name: 'Minute of Hour',
                mbql: [
                    'datetime-field',
                    null,
                    'minute-of-hour',
                ],
                type: 'type/DateTime',
            },
            8: {
                name: 'Hour of Day',
                mbql: [
                    'datetime-field',
                    null,
                    'hour-of-day',
                ],
                type: 'type/DateTime',
            },
            9: {
                name: 'Day of Week',
                mbql: [
                    'datetime-field',
                    null,
                    'day-of-week',
                ],
                type: 'type/DateTime',
            },
            10: {
                name: 'Day of Month',
                mbql: [
                    'datetime-field',
                    null,
                    'day-of-month',
                ],
                type: 'type/DateTime',
            },
            11: {
                name: 'Day of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'day-of-year',
                ],
                type: 'type/DateTime',
            },
            12: {
                name: 'Week of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'week-of-year',
                ],
                type: 'type/DateTime',
            },
            13: {
                name: 'Month of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'month-of-year',
                ],
                type: 'type/DateTime',
            },
            14: {
                name: 'Quarter of Year',
                mbql: [
                    'datetime-field',
                    null,
                    'quarter-of-year',
                ],
                type: 'type/DateTime',
            },
            15: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    null,
                    'default',
                ],
                type: 'type/Number',
            },
            16: {
                name: '10 bins',
                mbql: [
                    'binning-strategy',
                    null,
                    'num-bins',
                    10,
                ],
                type: 'type/Number',
            },
            17: {
                name: '50 bins',
                mbql: [
                    'binning-strategy',
                    null,
                    'num-bins',
                    50,
                ],
                type: 'type/Number',
            },
            18: {
                name: '100 bins',
                mbql: [
                    'binning-strategy',
                    null,
                    'num-bins',
                    100,
                ],
                type: 'type/Number',
            },
            19: {
                name: 'Don\'t bin',
                mbql: null,
                type: 'type/Number',
            },
            20: {
                name: 'Auto bin',
                mbql: [
                    'binning-strategy',
                    null,
                    'default',
                ],
                type: 'type/Coordinate',
            },
            21: {
                name: 'Bin every 1 degree',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    1,
                ],
                type: 'type/Coordinate',
            },
            22: {
                name: 'Bin every 10 degrees',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    10,
                ],
                type: 'type/Coordinate',
            },
            23: {
                name: 'Bin every 20 degrees',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    20,
                ],
                type: 'type/Coordinate',
            },
            24: {
                name: 'Bin every 50 degrees',
                mbql: [
                    'binning-strategy',
                    null,
                    'bin-width',
                    50,
                ],
                type: 'type/Coordinate',
            },
            25: {
                name: 'Don\'t bin',
                mbql: null,
                type: 'type/Coordinate',
            },
        },
        rows: 200,
        updated_at: '2018-10-26T07:50:02.000Z',
        entity_name: null,
        active: true,
        id: '1',
        db_id: 1,
        visibility_type: null,
        display_name: 'Products',
        metrics: [],
        created_at: '2017-12-30T12:28:26.000Z',
        points_of_interest: null,
    },
];
//# sourceMappingURL=tables.js.map