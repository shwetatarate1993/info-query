"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    iSCriteria: {
        database: '1001',
        query: {
            filter: [
                '=',
                [
                    'field-id', '2',
                ],
                'Rustic Bronze Pants',
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    iSNotCriteria: {
        database: '1001',
        query: {
            filter: [
                '!=',
                [
                    'field-id', '2',
                ],
                'Rustic Bronze Pants',
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    containsCriteriaCaseInsensitive: {
        database: '1001',
        query: {
            filter: [
                'CONTAINS',
                [
                    'field-id', '2',
                ],
                'awesome',
                {
                    'case-sensitive': false,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    containsCriteriaCaseSensitive: {
        database: '1001',
        query: {
            filter: [
                'CONTAINS',
                [
                    'field-id', '2',
                ],
                'awesome',
                {
                    'case-sensitive': true,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    doesNotContainCriteriaCaseInsensitive: {
        database: '1001',
        query: {
            filter: [
                'DOES_NOT_CONTAIN',
                [
                    'field-id', '2',
                ],
                'bronze',
                {
                    'case-sensitive': false,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    doesNotContainCriteriaCaseSensitive: {
        database: '1001',
        query: {
            filter: [
                'DOES_NOT_CONTAIN',
                [
                    'field-id', '2',
                ],
                'bronze',
                {
                    'case-sensitive': true,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    iSEmptyCriteria: {
        database: '1001',
        query: {
            filter: [
                'IS_NULL',
                [
                    'field-id', '2',
                ],
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    iSNotEmptyCriteria: {
        database: '1001',
        query: {
            filter: [
                'NOT_NULL',
                [
                    'field-id', '2',
                ],
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    startsWithCriteriaCaseInsensitive: {
        database: '1001',
        query: {
            filter: [
                'STARTS_WITH',
                [
                    'field-id', '2',
                ],
                'awe',
                {
                    'case-sensitive': false,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    startsWithCriteriaCaseSensitive: {
        database: '1001',
        query: {
            filter: [
                'STARTS_WITH',
                [
                    'field-id', '2',
                ],
                'awe',
                {
                    'case-sensitive': true,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    endsWithCriteriaCaseInsensitive: {
        database: '1001',
        query: {
            filter: [
                'ENDS_WITH',
                [
                    'field-id', '2',
                ],
                'Ants',
                {
                    'case-sensitive': false,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
    endsWithCriteriaCaseSensitive: {
        database: '1001',
        query: {
            filter: [
                'ENDS_WITH',
                [
                    'field-id', '2',
                ],
                'Ants',
                {
                    'case-sensitive': true,
                },
            ],
            source_table: '1',
        },
        type: 'query',
        isNative: false,
    },
};
//# sourceMappingURL=criteria.string.request.js.map