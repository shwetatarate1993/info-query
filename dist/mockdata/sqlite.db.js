"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../config"));
const knex_config_1 = require("knex-config");
const knex = knex_config_1.knexClient(config_1.default.get('db'), '1001', 'sqlite3');
exports.readTable = async () => {
    const select = knex.withSchema('').select('*').from('todos').where('id', '>', 1);
    return select;
};
exports.seedTodoTable = async () => {
    // Inserts seed entries
    return knex('todos').insert([
        { id: 1, tasks: 'Go get coffee' },
        { id: 2, tasks: 'Go to the gym' },
        { id: 3, tasks: 'Go to EDA' },
    ]);
};
exports.truncateTodoTable = async () => {
    // Inserts seed entries
    return knex('todos').truncate();
};
exports.createTodoTable = async () => {
    return knex.schema.createTableIfNotExists('todos', (table) => {
        table.increments();
        table.string('tasks');
        table.integer('urgent');
    });
};
exports.todoTableSetup = async () => {
    await exports.createTodoTable();
    await exports.seedTodoTable();
};
exports.readOrderTable = async () => {
    const select = knex.withSchema('').select('*').from('ORDERS').where('ID', '>', 1);
    return select;
};
exports.seedOrderTable = async () => {
    // Inserts seed entries
    return knex('ORDERS').insert([
        {
            CREATED_AT: '2019-01-08', DISCOUNT: 4, ID: 1, PRODUCT_ID: 5,
            QUANTITY: 3, SUBTOTAL: 68.45, TAX: 3.76, TOTAL: 72.21, USER_ID: 1
        },
        {
            CREATED_AT: '2019-03-02', DISCOUNT: 3, ID: 2, PRODUCT_ID: 2,
            QUANTITY: 2, SUBTOTAL: 56.25, TAX: 0, TOTAL: 56.26, USER_ID: 2
        },
        {
            CREATED_AT: '2019-01-01T12:31:00', DISCOUNT: 5, ID: 3, PRODUCT_ID: 4,
            QUANTITY: 5, SUBTOTAL: 116.64, TAX: 4.67, TOTAL: 121.29, USER_ID: 3
        },
        {
            CREATED_AT: '2019-01-07T01:10:00', DISCOUNT: 4, ID: 4, PRODUCT_ID: 4,
            QUANTITY: 5, SUBTOTAL: 109.79, TAX: 4.39, TOTAL: 114.16, USER_ID: 3
        },
        {
            CREATED_AT: '2019-02-10', DISCOUNT: 5, ID: 5, PRODUCT_ID: 5,
            QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3
        },
    ]);
};
exports.truncateOrderTable = async () => {
    return knex('ORDERS').truncate();
};
exports.createOrderTable = async () => {
    return knex.schema.createTableIfNotExists('ORDERS', (table) => {
        table.integer('ID');
        table.dateTime('CREATED_AT');
        table.float('DISCOUNT');
        table.integer('PRODUCT_ID');
        table.integer('QUANTITY');
        table.float('SUBTOTAL');
        table.float('TAX');
        table.float('TOTAL');
        table.integer('USER_ID');
    });
};
exports.orderTableSetup = async () => {
    await exports.createOrderTable();
    await exports.seedOrderTable();
};
exports.readProductTable = async () => {
    const select = knex.withSchema('').select('*').from('PRODUCTS').where('ID', '>', 1);
    return select;
};
exports.seedProductTable = async () => {
    // Inserts seed entries
    return knex('PRODUCTS').insert([
        {
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CATEGORY: 'Gadget', EAN: '0705767545749',
            ID: 1, PRICE: 70.85, RATING: 3.6, TITLE: 'Awesome Wooden Pants', VENDOR: 'McGlynn-Orn'
        },
        {
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CATEGORY: 'Widget', EAN: '7668295014299',
            ID: 2, PRICE: 43.17, RATING: 3.6, TITLE: 'Lightweight Bronze Table', VENDOR: 'Connelly-Quigley'
        },
        {
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CATEGORY: 'Doohickey', EAN: '9368714668208',
            ID: 3, PRICE: 36.11, RATING: 3.6, TITLE: 'Incredible Granite Pants', VENDOR: 'Sporer, Weber and Brown'
        },
        {
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CATEGORY: 'Gizmo', EAN: '9208431506097',
            ID: 4, PRICE: 82.57, RATING: 3.5, TITLE: 'Rustic Bronze Pants', VENDOR: 'Roob, Smith and Hettinger'
        },
        {
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CATEGORY: 'Widget', EAN: '0193622489091',
            ID: 5, PRICE: 45.64, RATING: 3.7, TITLE: 'Awesome Linen Plate', VENDOR: 'Ziemann, Yundt and Murazik'
        },
    ]);
};
exports.truncateProductTable = async () => {
    return knex('PRODUCTS').truncate();
};
exports.createProductTable = async () => {
    return knex.schema.createTableIfNotExists('PRODUCTS', (table) => {
        table.integer('ID');
        table.string('CATEGORY');
        table.dateTime('CREATED_AT');
        table.string('EAN');
        table.float('PRICE');
        table.float('RATING');
        table.string('TITLE');
        table.string('VENDOR');
    });
};
exports.productTableSetup = async () => {
    await exports.createProductTable();
    await exports.seedProductTable();
};
exports.readPeopleTable = async () => {
    const select = knex.withSchema('').select('*').from('PEOPLE').where('ID', '>', 1);
    return select;
};
exports.seedPeopleTable = async () => {
    // Inserts seed entries
    return knex('PEOPLE').insert([
        {
            ADDRESS: '734 Prohaska Springs', BIRTH_DATE: 'Sunday, January 30, 1966 12:00 AM',
            CITY: 'Port Felipe', CREATED_AT: 'Monday, December 24, 2018 9:29 AM',
            EMAIL: 'adelia-eichmann@hotmail.com', ID: 1, NAME: 'Adelia Eichmann',
            LATITUDE: '30.42516699° S', LONGITUDE: '30.42516699° S',
            PASSWORD: '4a6d860f-9dc5-49a7-b5b8-197fcc2e4c3a', SOURCE: 'Google', STATE: 'NE',
            ZIP: '75048'
        },
        {
            ADDRESS: '3891 Rodger Prairie', BIRTH_DATE: 'Sunday, January 30, 1966 12:00 AM',
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CITY: 'Kutchville',
            EMAIL: 'beau.feil@hotmail.com', ID: 2, LATITUDE: '30.42516699° S',
            LONGITUDE: '30.42516699° S', NAME: 'Beau Feil',
            PASSWORD: '4a6d860f-9dc5-49a7-b5b8-197fcc2e4c3a', SOURCE: 'Google', STATE: 'DE',
            ZIP: '75048'
        },
        {
            ADDRESS: '0418 Tomasa Locks', BIRTH_DATE: 'Sunday, January 30, 1966 12:00 AM',
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CITY: 'South Franzport',
            EMAIL: 'kaylee-anderson@hotmail.com', ID: 3, LATITUDE: '30.42516699° S',
            LONGITUDE: '30.42516699° S', NAME: 'Kaylee Anderson',
            PASSWORD: '4a6d860f-9dc5-49a7-b5b8-197fcc2e4c3a', SOURCE: 'Google', STATE: 'SD', ZIP: '75048'
        },
        {
            ADDRESS: '236 Baumbach Knoll', BIRTH_DATE: 'Sunday, January 30, 1966 12:00 AM',
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CITY: 'New Kaylin',
            EMAIL: 'sawayn-jessica@yahoo.com', ID: 4, LATITUDE: '30.42516699° S',
            LONGITUDE: '30.42516699° S', NAME: 'Jessica Sawayn',
            PASSWORD: '4a6d860f-9dc5-49a7-b5b8-197fcc2e4c3a', SOURCE: 'Google', STATE: 'NC', ZIP: '75048'
        },
        {
            ADDRESS: '04616 Johnny Loaf', BIRTH_DATE: 'Sunday, January 30, 1966 12:00 AM',
            CREATED_AT: 'Monday, December 24, 2018 9:29 AM', CITY: 'Elvisburgh',
            EMAIL: 'justina.gerlach@hotmail.com', ID: 5, LATITUDE: '30.42516699° S',
            LONGITUDE: '30.42516699° S', NAME: 'Justina Gerlach',
            PASSWORD: '4a6d860f-9dc5-49a7-b5b8-197fcc2e4c3a', SOURCE: 'Google', STATE: 'UT', ZIP: '75048'
        },
    ]);
};
exports.trucatePeopleTable = async () => {
    return knex('PEOPLE').truncate();
};
exports.createPeopleTable = async () => {
    return knex.schema.createTableIfNotExists('PEOPLE', (table) => {
        table.integer('ID');
        table.string('ADDRESS');
        table.dateTime('CREATED_AT');
        table.dateTime('BIRTH_DATE');
        table.string('CITY');
        table.string('EMAIL');
        table.float('LATITUDE');
        table.float('LONGITUDE');
        table.string('NAME');
        table.string('PASSWORD');
        table.string('SOURCE');
        table.string('STATE');
        table.string('ZIP');
    });
};
exports.peopleTableSetup = async () => {
    await exports.createPeopleTable();
    await exports.seedPeopleTable();
};
//# sourceMappingURL=sqlite.db.js.map