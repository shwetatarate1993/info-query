"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const query_request_1 = __importDefault(require("../../mockdata/query.request"));
const services_1 = require("../../services");
const array_util_1 = __importDefault(require("../../utilities/array.util"));
const join_parser_1 = require("../join.parser");
test('parse joins', async () => {
    const mock = sinon_1.default.mock(services_1.queryMetadataService);
    mock.expects('getTableIdByFieldId').returns('3');
    const queryRequest = JSON.parse(JSON.stringify(query_request_1.default.equalCondition));
    const joins = await join_parser_1.parseJoin(queryRequest, '');
    expect(array_util_1.default.isNotEmpty(joins)).not.toBe(0);
    mock.restore();
});
//# sourceMappingURL=joins.parser.test.js.map