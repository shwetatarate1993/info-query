"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mockdata_1 = require("../../mockdata");
const array_util_1 = __importDefault(require("../../utilities/array.util"));
const criteria_parser_1 = require("../criteria.parser");
test('parse criteria for simple "On" filter', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeOnCriterias));
    const criteria = await criteria_parser_1.parseCriteria(queryRequest, '');
    expect(array_util_1.default.isNotEmpty(criteria)).not.toBe(0);
});
//# sourceMappingURL=criteria.parser.test.js.map