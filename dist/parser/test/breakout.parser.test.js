"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const query_request_1 = __importDefault(require("../../mockdata/query.request"));
const array_util_1 = __importDefault(require("../../utilities/array.util"));
const breakout_parser_1 = require("../breakout.parser");
test('parse breakout for simple group by', async () => {
    const queryRequest = JSON.parse(JSON.stringify(query_request_1.default.equalCondition));
    const breakouts = await breakout_parser_1.parseBreakout(queryRequest, '');
    expect(array_util_1.default.isNotEmpty(breakouts)).not.toBe(0);
});
//# sourceMappingURL=breakout.parser.test.js.map