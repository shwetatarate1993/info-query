"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const query_request_1 = __importDefault(require("../../mockdata/query.request"));
const array_util_1 = __importDefault(require("../../utilities/array.util"));
const orderby_parser_1 = require("../orderby.parser");
test('parse OrderBy', async () => {
    const queryRequest = JSON.parse(JSON.stringify(query_request_1.default.equalCondition));
    const orderBys = await orderby_parser_1.parseOrderBy(queryRequest, '');
    expect(array_util_1.default.isNotEmpty(orderBys)).not.toBe(0);
});
//# sourceMappingURL=orderby.parser.test.js.map