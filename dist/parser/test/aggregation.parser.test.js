"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const query_request_1 = __importDefault(require("../../mockdata/query.request"));
const array_util_1 = __importDefault(require("../../utilities/array.util"));
const aggregation_parser_1 = require("../aggregation.parser");
test('parse aggregation for simple aggregation', async () => {
    const queryRequest = JSON.parse(JSON.stringify(query_request_1.default.equalCondition));
    const aggregations = await aggregation_parser_1.parseAggregation(queryRequest, '');
    expect(array_util_1.default.isNotEmpty(aggregations)).not.toBe(0);
});
//# sourceMappingURL=aggregation.parser.test.js.map