"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
const aggregation_parser_1 = require("./aggregation.parser");
const breakout_parser_1 = require("./breakout.parser");
const criteria_parser_1 = require("./criteria.parser");
const field_parser_1 = require("./field.parser");
const join_parser_1 = require("./join.parser");
const orderby_parser_1 = require("./orderby.parser");
class QueryParser {
    async parseQuery(queryReq, jwtToken) {
        const queryType = queryReq.type;
        const databaseId = queryReq.database;
        const sourceTableId = queryReq.query.source_table;
        const limit = queryReq.query.limit;
        const joins = await join_parser_1.parseJoin(queryReq, jwtToken);
        const criterias = await criteria_parser_1.parseCriteria(queryReq, jwtToken);
        const aggregations = await aggregation_parser_1.parseAggregation(queryReq, jwtToken);
        const breakouts = await breakout_parser_1.parseBreakout(queryReq, jwtToken);
        const orderBys = await orderby_parser_1.parseOrderBy(queryReq, jwtToken);
        const fields = await field_parser_1.parseFields(queryReq, jwtToken);
        return new models_1.Query(queryType, databaseId, sourceTableId, limit, fields, criterias, aggregations, breakouts, orderBys, joins);
    }
    async parseNativeQuery(queryReq) {
        const databaseId = queryReq.database;
        const queryType = queryReq.type;
        const query = queryReq.native.query;
        const native = new models_1.Native(queryReq.native.query, queryReq.native.collection);
        return new models_1.NativeQuery(databaseId, queryType, native, true, queryReq.datasource);
    }
}
const queryParser = new QueryParser();
Object.freeze(queryParser);
exports.default = queryParser;
//# sourceMappingURL=query.req.parser.js.map