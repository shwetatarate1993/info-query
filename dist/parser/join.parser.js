"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const array_util_1 = __importDefault(require("../utilities/array.util"));
const parseCriteriaReqForJoins = async (queryReq, criteriaReq, jwtToken) => {
    if (array_util_1.default.isNotEmpty(criteriaReq)) {
        // Validation
        const criteriaField = criteriaReq[1];
        if (criteriaField[0] === constants_1.FOREIGNKEY) {
            const sourceFieldId = criteriaField[1];
            const sourceTableId = queryReq.query.source_table;
            const targetFieldId = criteriaField[2];
            const targetTableId = await services_1.queryMetadataService.getTableIdByFieldId(targetFieldId, jwtToken);
            return new models_1.Join(sourceTableId, sourceFieldId, targetTableId, targetFieldId, constants_1.LEFTJOIN);
        }
        else {
            return null;
        }
    }
    else {
        return null;
    }
};
const isMultipleCriterias = (filter) => {
    return array_util_1.default.isNotEmpty(filter)
        && (filter[0] === 'and' || filter[0] === 'or');
};
exports.parseJoin = async (queryReq, jwtToken) => {
    const joins = [];
    // const filter: any[] = arrayUtils.isEmpty(queryReq.query.filter) ? [] : queryReq.query.filter;
    const criteriaReqs = isMultipleCriterias(queryReq.query.filter) ? queryReq.query.filter.slice(1)
        : [queryReq.query.filter];
    for (const criteriaReq of criteriaReqs) {
        const parsedJoin = await parseCriteriaReqForJoins(queryReq, criteriaReq, jwtToken);
        if (parsedJoin != null) {
            joins.push(parsedJoin);
        }
    }
    return joins;
};
//# sourceMappingURL=join.parser.js.map