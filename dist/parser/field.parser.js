"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
const string_util_1 = __importDefault(require("../utilities/string.util"));
exports.parseFields = async (queryReq, jwtToken) => {
    const queryFields = [];
    if (queryReq.query.fields !== undefined) {
        for (const fieldReq of queryReq.query.fields) {
            const parsedField = await parseField(queryReq, fieldReq, jwtToken);
            if (parsedField != null) {
                queryFields.push(parsedField);
            }
        }
    }
    return queryFields;
};
const parseField = async (queryReq, fieldReq, jwtToken) => {
    if (string_util_1.default.isNotEmpty(fieldReq.id.toString())) {
        const alias = fieldReq.alias !== undefined ? fieldReq.alias : '';
        return new models_1.Field(queryReq.query.source_table, fieldReq.id, alias);
    }
    else {
        return null;
    }
};
//# sourceMappingURL=field.parser.js.map