"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const string_util_1 = require("../../utilities/string.util");
const constants_1 = require("../../constants");
const init_config_1 = require("../../init-config");
const models_1 = require("../../models");
const getColumns = async (result, queryMetadata, jwtToken) => {
    const colKeys = constants_1.NATIVE !== queryMetadata.queryType ? Object.keys(result[0]) :
        (result[0].length ? Object.keys(JSON.parse(JSON.stringify(result[0][0]))) : []);
    const colsMetadata = [];
    if (constants_1.NATIVE !== queryMetadata.queryType) {
        const entity = await queryMetadata.getEntity(queryMetadata.parsedQuery.sourceTableId);
        if (entity) {
            const physicalColumns = entity.physicalColumns;
            for (const colKey of colKeys) {
                const column = physicalColumns.find((physicalColumn) => physicalColumn.dbCode === colKey);
                colsMetadata.push(column);
            }
        }
    }
    else {
        const fields = result[1];
        for (const colKey of colKeys) {
            const field = fields.find((fd) => fd.name === colKey);
            field.display_name = string_util_1.toTitleCase(field.name);
            switch (field.type) {
                case 3:
                case 8: {
                    field.base_type = 'type/Integer';
                    break;
                }
                case 4: {
                    field.base_type = 'type/Float';
                    break;
                }
                case 7: {
                    field.base_type = 'type/DateTime';
                    break;
                }
                case 10: {
                    field.base_type = 'type/Date';
                    break;
                }
                case 246: {
                    field.base_type = 'type/Decimal';
                    break;
                }
                case 253: {
                    field.base_type = 'type/Text';
                    break;
                }
                default:
                    field.base_type = 'type/*';
            }
            colsMetadata.push(field);
        }
    }
    return { columns: colKeys, cols: colsMetadata };
};
const getData = (results, queryMetadata, columns) => {
    const dialect = init_config_1.InfoQueryConfig.configuredDialect;
    if (constants_1.NATIVE === queryMetadata.queryType) {
        if (!dialect || dialect === 'mysql') {
            results = results[0];
        }
    }
    const dataList = [];
    for (const result of results) {
        const record = [];
        for (const column of columns) {
            record.push(result[column]);
        }
        dataList.push(record);
    }
    return dataList;
};
exports.parseResponseData = async (results, queryMetadata, jwtToken) => {
    let columnDetails;
    const dialect = init_config_1.InfoQueryConfig.configuredDialect;
    if (!dialect || dialect === 'mysql') {
        columnDetails = await getColumns(results, queryMetadata, jwtToken);
    }
    else {
        columnDetails = { columns: results.length ? Object.keys(results[0]) : [], cols: [] };
    }
    const rows = getData(results, queryMetadata, columnDetails.columns);
    return new models_1.ResponseData(rows, columnDetails.columns, columnDetails.cols);
};
//# sourceMappingURL=res.data.parser.js.map