"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const string_util_1 = require("../utilities/string.util");
const constants_1 = require("../constants");
const models_1 = require("../models");
const array_util_1 = __importDefault(require("../utilities/array.util"));
const string_util_2 = __importDefault(require("../utilities/string.util"));
const getData = (results, queryMetadata, columns) => {
    if (constants_1.NATIVE === queryMetadata.queryType) {
        results = results[0];
    }
    const dataList = [];
    for (const result of results) {
        const record = [];
        for (const column of columns) {
            record.push(result[column]);
        }
        dataList.push(record);
    }
    return dataList;
};
const getColumns = async (result, queryMetadata, jwtToken) => {
    const colKeys = constants_1.NATIVE !== queryMetadata.queryType ? Object.keys(result[0]) :
        Object.keys(JSON.parse(JSON.stringify(result[0][0])));
    const colsMetadata = [];
    if (constants_1.NATIVE !== queryMetadata.queryType) {
        const table = await queryMetadata.getTableById(queryMetadata.parsedQuery.sourceTableId, jwtToken);
        const fields = table.fields;
        if (array_util_1.default.isEmpty(queryMetadata.parsedQuery.fields)) {
            for (const colKey of colKeys) {
                const field = fields.find((fd) => fd.name === colKey);
                colsMetadata.push(field);
            }
        }
        else {
            const fieldArray = [];
            fieldArray.push(...queryMetadata.parsedQuery.fields);
            for (const aggregation of queryMetadata.parsedQuery.aggregations) {
                fieldArray.push(aggregation.field);
            }
            for (const fieldObj of fieldArray) {
                // Copy 'value' of object to another instead of 'reference'
                const feild = Object.assign({}, fields.find((fd) => fd.id === fieldObj.fieldId));
                if (string_util_2.default.isNotEmpty(fieldObj.fieldId) && string_util_2.default.isNotEmpty(fieldObj.alias)) {
                    feild.display_name = fieldObj.alias;
                    colsMetadata.push(feild);
                }
            }
        }
    }
    else {
        const fields = result[1];
        for (const colKey of colKeys) {
            const field = fields.find((fd) => fd.name === colKey);
            field.display_name = string_util_1.toTitleCase(field.name);
            switch (field.type) {
                case 3:
                case 8: {
                    field.base_type = 'type/Integer';
                    break;
                }
                case 4: {
                    field.base_type = 'type/Float';
                    break;
                }
                case 7: {
                    field.base_type = 'type/DateTime';
                    break;
                }
                case 10: {
                    field.base_type = 'type/Date';
                    break;
                }
                case 246: {
                    field.base_type = 'type/Decimal';
                    break;
                }
                case 253: {
                    field.base_type = 'type/Text';
                    break;
                }
                default:
                    field.base_type = 'type/*';
            }
            colsMetadata.push(field);
        }
    }
    return { columns: colKeys, cols: colsMetadata };
};
exports.parseResponseData = async (results, queryMetadata, jwtToken) => {
    const columnDetails = await getColumns(results, queryMetadata, jwtToken);
    const rows = getData(results, queryMetadata, columnDetails.columns);
    return new models_1.ResponseData(rows, columnDetails.columns, columnDetails.cols);
};
//# sourceMappingURL=res.data.parser.js.map