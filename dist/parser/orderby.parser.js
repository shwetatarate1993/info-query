"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const array_util_1 = __importDefault(require("../utilities/array.util"));
const parseOrderByReq = async (queryReq, orderByReq, jwtToken) => {
    if (array_util_1.default.isNotEmpty(orderByReq)) {
        let fieldId = '';
        let tableId = '';
        const orderByField = orderByReq[0];
        if (orderByField[0] === constants_1.FIELDID) {
            fieldId = orderByField[1];
            tableId = queryReq.query.source_table;
        }
        if (orderByField[0] === constants_1.FOREIGNKEY) {
            fieldId = orderByField[2];
            tableId = await services_1.queryMetadataService.getTableIdByFieldId(fieldId, jwtToken);
        }
        const field = new models_1.Field(tableId, fieldId, '');
        const value = orderByReq[1];
        return new models_1.OrderBy(field, value);
    }
    else {
        return null;
    }
};
exports.parseOrderBy = async (queryReq, jwtToken) => {
    const queryOrderBys = [];
    const orderByReqs = array_util_1.default.isEmpty(queryReq.query.order_by) ? [] : queryReq.query.order_by;
    for (const orderByReq of orderByReqs) {
        const parsedOrderBy = await parseOrderByReq(queryReq, orderByReq, jwtToken);
        if (parsedOrderBy != null) {
            queryOrderBys.push(parsedOrderBy);
        }
    }
    return queryOrderBys;
};
//# sourceMappingURL=orderby.parser.js.map