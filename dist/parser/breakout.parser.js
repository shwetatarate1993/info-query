"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const array_util_1 = __importDefault(require("../utilities/array.util"));
const parseBreakoutReq = async (queryReq, breakoutReq, jwtToken) => {
    if (array_util_1.default.isNotEmpty(breakoutReq)) {
        let fieldId = '';
        let tableId = '';
        if (breakoutReq[0] === constants_1.FIELDID) {
            fieldId = breakoutReq[1];
            tableId = queryReq.query.source_table;
        }
        if (breakoutReq[0] === constants_1.FOREIGNKEY) {
            fieldId = breakoutReq[2];
            tableId = await services_1.queryMetadataService.getTableIdByFieldId(fieldId, jwtToken);
        }
        const field = new models_1.Field(tableId, fieldId, '');
        return new models_1.Breakout(field);
    }
    else {
        return null;
    }
};
exports.parseBreakout = async (queryReq, jwtToken) => {
    const queryBreakouts = [];
    const breakoutReqs = array_util_1.default.isEmpty(queryReq.query.breakout) ? [] : queryReq.query.breakout;
    for (const breakoutReq of breakoutReqs) {
        const parsedBreakout = await parseBreakoutReq(queryReq, breakoutReq, jwtToken);
        if (parsedBreakout != null) {
            queryBreakouts.push(parsedBreakout);
        }
    }
    return queryBreakouts;
};
//# sourceMappingURL=breakout.parser.js.map