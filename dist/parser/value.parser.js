"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
exports.parseValue = (criteriaReq) => {
    // Validation from Value Parser
    const conditionValue = criteriaReq[0];
    const condition = constants_1.parseConditionEnum(conditionValue);
    switch (condition) {
        case constants_1.Condition.GT:
        case constants_1.Condition.GE:
        case constants_1.Condition.LT:
        case constants_1.Condition.LE:
        case constants_1.Condition.DATE:
        case constants_1.Condition.MONTH_YEAR:
        case constants_1.Condition.QUARTER_YEAR:
        case constants_1.Condition.RELATIVE:
            return criteriaReq[2];
        case constants_1.Condition.EQ:
        case constants_1.Condition.NE:
        case constants_1.Condition.BETWEEN:
        case constants_1.Condition.CONTAINS:
        case constants_1.Condition.DOES_NOT_CONTAIN:
        case constants_1.Condition.STARTS_WITH:
        case constants_1.Condition.ENDS_WITH:
        case constants_1.Condition.TIME_INTERVAL:
            return criteriaReq.slice(2);
        default:
            return null;
    }
};
//# sourceMappingURL=value.parser.js.map