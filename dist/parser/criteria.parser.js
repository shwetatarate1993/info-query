"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const array_util_1 = __importDefault(require("../utilities/array.util"));
const value_parser_1 = require("./value.parser");
const parseCriteriaReq = async (queryReq, criteriaReq, jwtToken) => {
    if (array_util_1.default.isNotEmpty(criteriaReq)) {
        // Validation
        const conditionValue = criteriaReq[0];
        const condition = constants_1.parseConditionEnum(conditionValue);
        // Validate for criteria criteriaField
        const criteriaField = criteriaReq[1];
        let fieldId;
        let tableId;
        if (criteriaField[0] === constants_1.DATETIMEFIELD) {
            const fieldArr = criteriaField[1];
            fieldId = fieldArr[0] === constants_1.FIELDID ? fieldArr[1] : fieldArr[2];
            tableId = fieldArr[0] === constants_1.FIELDID ? queryReq.query.source_table :
                await services_1.queryMetadataService.getTableIdByFieldId(fieldId, jwtToken);
        }
        else if (criteriaField[0] !== constants_1.FOREIGNKEY) {
            fieldId = criteriaField[0] === constants_1.FIELDID ? criteriaField[1] : '';
            tableId = queryReq.query.source_table;
        }
        else if (criteriaField[0] === constants_1.FOREIGNKEY) {
            fieldId = criteriaField[2];
            tableId = await services_1.queryMetadataService.getTableIdByFieldId(fieldId, jwtToken);
        }
        else {
            return null;
        }
        const field = new models_1.Field(tableId, fieldId, '');
        const value = value_parser_1.parseValue(criteriaReq);
        return new models_1.Criteria(field, condition, value);
    }
    else {
        return null;
    }
};
const isMultipleCriterias = (filter) => {
    return array_util_1.default.isNotEmpty(filter)
        && (filter[0] === 'and' || filter[0] === 'or' || filter[0] === 'AND' || filter[0] === 'OR');
};
exports.parseCriteria = async (queryReq, jwtToken) => {
    const queryCriterias = [];
    // const filter: any[] = arrayUtils.isEmpty(queryReq.query.filter) ? [] : queryReq.query.filter;
    const criteriaReqs = isMultipleCriterias(queryReq.query.filter) ?
        queryReq.query.filter.slice(1) : [queryReq.query.filter];
    for (const criteriaReq of criteriaReqs) {
        const parsedCrietria = await parseCriteriaReq(queryReq, criteriaReq, jwtToken);
        if (parsedCrietria != null) {
            queryCriterias.push(parsedCrietria);
        }
    }
    return queryCriterias;
};
//# sourceMappingURL=criteria.parser.js.map