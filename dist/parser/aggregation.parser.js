"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const array_util_1 = __importDefault(require("../utilities/array.util"));
const parseAggregationReq = async (queryReq, aggregationReq, jwtToken) => {
    if (array_util_1.default.isNotEmpty(aggregationReq)) {
        // Validation
        const aggregationFunction = aggregationReq[0];
        const aggregation = constants_1.parseAggregationEnum(aggregationFunction);
        let tableId = '';
        let fieldId = '';
        let alias = '';
        // Condition for other than COUNT aggregation
        if (Array.isArray(aggregationReq[1])) {
            const aggregationField = aggregationReq[1];
            if (aggregationField[0] === constants_1.FIELDID) {
                tableId = queryReq.query.source_table;
                fieldId = aggregationField[1];
                alias = aggregationField[2] !== undefined ? aggregationField[2] : '';
            }
            if (aggregationField[0] === constants_1.FOREIGNKEY) {
                tableId = await services_1.queryMetadataService.getTableIdByFieldId(fieldId, jwtToken);
                fieldId = aggregationField[2];
                alias = aggregationField[3] !== undefined ? aggregationField[3] : '';
            }
        }
        else {
            alias = aggregationReq[1] !== undefined ? aggregationReq[1] : '';
        }
        const field = new models_1.Field(tableId, fieldId, alias);
        return new models_1.Aggregation(field, aggregation);
    }
    else {
        return null;
    }
};
exports.parseAggregation = async (queryReq, jwtToken) => {
    const queryAggregations = [];
    const aggregationReqs = array_util_1.default.isEmpty(queryReq.query.aggregation) ? [] : queryReq.query.aggregation;
    for (const aggregationReq of aggregationReqs) {
        const parsedAggregation = await parseAggregationReq(queryReq, aggregationReq, jwtToken);
        if (parsedAggregation != null) {
            queryAggregations.push(parsedAggregation);
        }
    }
    return queryAggregations;
};
//# sourceMappingURL=aggregation.parser.js.map