"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Status;
(function (Status) {
    Status["completed"] = "completed";
    Status["started"] = "started";
    Status["error"] = "error";
})(Status = exports.Status || (exports.Status = {}));
//# sourceMappingURL=status.enum.js.map