"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Condition;
(function (Condition) {
    Condition["MONTH_YEAR"] = "date/month-year";
    Condition["QUARTER_YEAR"] = "date/quarter-year";
    Condition["RELATIVE"] = "date/relative";
    Condition["TIME_INTERVAL"] = "time-interval";
    Condition["DATE"] = "ON";
    Condition["EQ"] = "=";
    Condition["LT"] = "<";
    Condition["LE"] = "<=";
    Condition["GT"] = ">";
    Condition["GE"] = ">=";
    Condition["NE"] = "!=";
    Condition["AND"] = "and";
    Condition["OR"] = "or";
    Condition["NOT"] = "!";
    Condition["IN"] = "in";
    Condition["NOTIN"] = "notin";
    Condition["IS_NULL"] = "IS_NULL";
    Condition["NOT_NULL"] = "NOT_NULL";
    Condition["BETWEEN"] = "BETWEEN";
    Condition["CONTAINS"] = "CONTAINS";
    Condition["DOES_NOT_CONTAIN"] = "DOES_NOT_CONTAIN";
    Condition["STARTS_WITH"] = "STARTS_WITH";
    Condition["ENDS_WITH"] = "ENDS_WITH";
})(Condition = exports.Condition || (exports.Condition = {}));
exports.parseConditionEnum = (val) => {
    switch (val) {
        case Condition.EQ:
            return Condition.EQ;
        case Condition.LT:
            return Condition.LT;
        case Condition.LE:
            return Condition.LE;
        case Condition.GT:
            return Condition.GT;
        case Condition.GE:
            return Condition.GE;
        case Condition.NE:
            return Condition.NE;
        case Condition.IS_NULL:
            return Condition.IS_NULL;
        case Condition.NOT_NULL:
            return Condition.NOT_NULL;
        case Condition.BETWEEN:
            return Condition.BETWEEN;
        case Condition.CONTAINS:
            return Condition.CONTAINS;
        case Condition.DOES_NOT_CONTAIN:
            return Condition.DOES_NOT_CONTAIN;
        case Condition.STARTS_WITH:
            return Condition.STARTS_WITH;
        case Condition.ENDS_WITH:
            return Condition.ENDS_WITH;
        case Condition.DATE:
            return Condition.DATE;
        case Condition.MONTH_YEAR:
            return Condition.MONTH_YEAR;
        case Condition.QUARTER_YEAR:
            return Condition.QUARTER_YEAR;
        case Condition.RELATIVE:
            return Condition.RELATIVE;
        case Condition.TIME_INTERVAL:
            return Condition.TIME_INTERVAL;
        default:
            throw new Error('Invalid or Unsupported filter condition ' + val);
    }
};
//# sourceMappingURL=condition.enum.js.map