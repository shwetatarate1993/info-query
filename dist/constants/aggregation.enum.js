"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AggregateFunction;
(function (AggregateFunction) {
    AggregateFunction["AVG"] = "avg";
    AggregateFunction["SUM"] = "sum";
    AggregateFunction["MIN"] = "min";
    AggregateFunction["MAX"] = "max";
    AggregateFunction["COUNT"] = "count";
    AggregateFunction["COUNTDISTINCT"] = "distinct";
})(AggregateFunction = exports.AggregateFunction || (exports.AggregateFunction = {}));
exports.parseAggregationEnum = (val) => {
    switch (val) {
        case AggregateFunction.AVG:
            return AggregateFunction.AVG;
        case AggregateFunction.SUM:
            return AggregateFunction.SUM;
        case AggregateFunction.MIN:
            return AggregateFunction.MIN;
        case AggregateFunction.MAX:
            return AggregateFunction.MAX;
        case AggregateFunction.COUNT:
            return AggregateFunction.COUNT;
        case AggregateFunction.COUNTDISTINCT:
            return AggregateFunction.COUNTDISTINCT;
        default:
            throw new Error('Invalid or Unsupported filter condition ' + val);
    }
};
//# sourceMappingURL=aggregation.enum.js.map