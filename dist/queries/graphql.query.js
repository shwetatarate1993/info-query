"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.query = graphql_tag_1.default ` query Database($databaseId: ID!, $tableId:ID!) {
    Database(id:$databaseId) {
        id
        name
        features
        description
        name
        timezone
        native_permissions
        engine
        details {
            host
            port
            dbname
            user
            password
            tunnel_port
        }
    },
    Table(id:$tableId) {
        id
        description
        name
        db_id
        rows
        updated_at
        created_at
        schema
        active
        visibility_type
        display_name
        db {
            id
            description
            features
            timezone
            name
            updated_at
            engine
            created_at
        }
        fields {
            id
            description
            name
            display_name
            fk_target_field_id
            base_type
            created_at
            updated_at
            table_id
            target {
                id
            }
            special_type
        }
    }
}`;
exports.queryField = graphql_tag_1.default ` query Field($fieldId:ID!) {
    Field(id:$fieldId) {
        id
        description
        name
        display_name
        fk_target_field_id
        base_type
        created_at
        updated_at
        table_id
        target {
            id
        }
        special_type
    }
}`;
exports.queryTable = graphql_tag_1.default ` query Table($tableId:ID!) {
    Table(id:$tableId) {
        id
        description
        name
        db_id
        rows
        updated_at
        created_at
        schema
        active
        visibility_type
        display_name
        db {
            id
            description
            features
            timezone
            name
            id
            updated_at
            engine
            created_at
        }
        fields {
            id
            description
            name
            display_name
            fk_target_field_id
            base_type
            created_at
            updated_at
            table_id
            target {
                id
            }
            special_type
        }
    }
}`;
exports.queryData = graphql_tag_1.default ` query Database($databaseId: ID!, $entityId: ID!) {
        Database(id:$databaseId) {
            configObjectId
            engine
        },
        PhysicalEntity(id:$entityId) {
          configObjectId
          compositeEntityId
          name
          configObjectType
          createdBy
          isDeleted
          itemDescription
          creationDate
          projectId
          updatedBy
          updationDate
          deletionDate
          expressionAvailable
          accessibilityRegex
          isMultivalueMapping
          order
          dbType
          dbTypeName
          schemaName
          releaseAreaSessName
          workAreaSessName
          insertQID
          updateQID
          sequenceQID
          singleSelectQID
          multiSelectQID
          deleteQID
          isAuditRequired
          physicalColumns {
            isKey
            dbCode
            length
            isDisplayColumn
            isPrimaryKey
            dataType
            dbType
            jsonName
            isLogicalColumnRequired
            isUnique
            isVirtualColumn
            isMultiValueField
            configObjectId
            name
            configObjectType
            createdBy
            isDeleted
            itemDescription
            creationDate
            projectId
            updatedBy
            updationDate
            deletionDate
          }
        }
}`;
//# sourceMappingURL=graphql.query.js.map