'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../../config"));
const knex_config_1 = require("knex-config");
const constants_1 = require("../../constants");
const init_config_1 = require("../../init-config");
const aggregate_1 = __importDefault(require("./aggregate"));
const criteria_1 = __importDefault(require("./criteria"));
const groupby_1 = __importDefault(require("./groupby"));
const join_1 = __importDefault(require("./join"));
const limit_1 = __importDefault(require("./limit"));
const orderby_1 = __importDefault(require("./orderby"));
const select_1 = __importDefault(require("./select"));
class KnexQueryExecutor {
    async executeQuery(queryMetadata, jwtToken) {
        let knex;
        const dialect = init_config_1.InfoQueryConfig.configuredDialect;
        if (constants_1.NATIVE === queryMetadata.queryType) {
            if (!dialect) {
                knex = knex_config_1.knexClient(config_1.default.get('db'), queryMetadata.parsedNativeQuery.database, 'mysql');
            }
            else {
                knex = knex_config_1.knexClient(init_config_1.InfoQueryConfig.dbConfig, queryMetadata.parsedNativeQuery.datasource, dialect);
            }
            return knex.raw(queryMetadata.parsedNativeQuery.native.query);
        }
        if (!dialect) {
            knex = knex_config_1.knexClient(config_1.default.get('db'), queryMetadata.parsedQuery.databaseId, 'mysql');
        }
        const baseQuery = knex.queryBuilder();
        const withTableSelection = select_1.default(queryMetadata);
        withTableSelection(baseQuery);
        const withJoins = join_1.default(queryMetadata);
        withJoins(baseQuery);
        const withCriterias = criteria_1.default(queryMetadata);
        withCriterias(baseQuery);
        const withAggregations = aggregate_1.default(queryMetadata, jwtToken);
        withAggregations(baseQuery);
        const withGroupBys = groupby_1.default(queryMetadata);
        withGroupBys(baseQuery);
        const withOrderBys = orderby_1.default(queryMetadata);
        withOrderBys(baseQuery);
        const withLimit = limit_1.default(queryMetadata);
        return withLimit(baseQuery);
    }
}
const knexQueryExecutor = new KnexQueryExecutor();
Object.freeze(knexQueryExecutor);
exports.default = knexQueryExecutor;
//# sourceMappingURL=index.js.map