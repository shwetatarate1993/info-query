"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const addSelect = (queryMetadata) => {
    return (baseQuery) => {
        const entity = queryMetadata.getEntity(queryMetadata.parsedQuery.sourceTableId);
        return baseQuery.withSchema(entity.schemaName).from(entity.dbTypeName);
    };
};
exports.default = addSelect;
//# sourceMappingURL=select.js.map