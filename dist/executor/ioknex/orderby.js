"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../constants");
const addOrderBys = (queryMetadata) => {
    return ((builder) => {
        for (const orderBy of queryMetadata.parsedQuery.orderBys) {
            const entity = queryMetadata.getEntity(orderBy.field.tableId);
            const column = entity.physicalColumns.find((col) => col.configObjectId === orderBy.field.fieldId);
            const fieldName = entity.dbTypeName + '.' + column.dbCode;
            const direction = orderBy.value === 'ascending' ? constants_1.ASC : constants_1.DESC;
            builder.orderBy(fieldName, direction);
        }
        return builder;
    });
};
exports.default = addOrderBys;
//# sourceMappingURL=orderby.js.map