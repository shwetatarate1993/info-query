"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const addGroupBys = (queryMetadata) => {
    return ((builder) => {
        for (const breakout of queryMetadata.parsedQuery.breakouts) {
            const field = breakout.field;
            const entity = queryMetadata.getEntity(field.tableId);
            const column = entity.physicalColumns.find((col) => field.fieldId === col.configObjectId);
            const fieldName = field.alias ? entity.dbTypeName + '.' + column.dbCode + ' as ' + field.alias
                : entity.dbTypeName + '.' + column.dbCode;
            builder.select(fieldName).groupBy(fieldName);
        }
        return builder;
    });
};
exports.default = addGroupBys;
//# sourceMappingURL=groupby.js.map