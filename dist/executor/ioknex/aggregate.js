"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../constants");
const addAggregations = (queryMetadata, jwtToken) => {
    return (async (builder) => {
        const indexMap = new Map([[constants_1.AggregateFunction.AVG, 1], [constants_1.AggregateFunction.SUM, 1],
            [constants_1.AggregateFunction.MIN, 1], [constants_1.AggregateFunction.MAX, 1], [constants_1.AggregateFunction.COUNT, 1]]);
        for (const aggregation of queryMetadata.parsedQuery.aggregations) {
            const fieldName = await getFieldWithAlias(builder, queryMetadata, aggregation, indexMap, jwtToken);
            addAggregation(aggregation, builder, fieldName);
        }
        return builder;
    });
};
const addAggregation = (aggregation, builder, fieldName) => {
    switch (aggregation.aggregationFunction) {
        case constants_1.AggregateFunction.AVG: {
            builder.avg(fieldName);
            break;
        }
        case constants_1.AggregateFunction.SUM: {
            builder.sum(fieldName);
            break;
        }
        case constants_1.AggregateFunction.MIN: {
            builder.min(fieldName);
            break;
        }
        case constants_1.AggregateFunction.MAX: {
            builder.max(fieldName);
            break;
        }
        case constants_1.AggregateFunction.COUNT: {
            builder.count(fieldName);
            break;
        }
        case constants_1.AggregateFunction.COUNTDISTINCT: {
            builder.countDistinct(fieldName);
            break;
        }
        default:
            throw new Error('Invalid or Unsupported Aggregation :' + JSON.stringify(aggregation));
    }
};
const getFieldWithAlias = async (builder, queryMetadata, aggregation, indexMap, jwtToken) => {
    const field = await queryMetadata.getColumn(aggregation.field.tableId, aggregation.field.fieldId);
    const table = await queryMetadata.getEntity(aggregation.field.tableId);
    let fieldName = aggregation.aggregationFunction === constants_1.AggregateFunction.COUNT
        ? '*' : table.name + '.' + field.name;
    const aggregateFunctionAlias = aggregation.aggregationFunction === constants_1.AggregateFunction.COUNTDISTINCT
        ? constants_1.AggregateFunction.COUNT : aggregation.aggregationFunction;
    fieldName = fieldName + ' as ' + aggregateFunctionAlias;
    fieldName = fieldName + (indexMap.get(aggregateFunctionAlias) === 1 ? '' : indexMap.get(aggregateFunctionAlias));
    indexMap.set(aggregateFunctionAlias, indexMap.get(aggregateFunctionAlias) + 1);
    return fieldName;
};
exports.default = addAggregations;
//# sourceMappingURL=aggregate.js.map