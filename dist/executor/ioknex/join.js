"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const addJoins = (queryMetadata) => {
    return ((builder) => {
        for (const join of queryMetadata.parsedQuery.joins) {
            addJoin(builder, queryMetadata, join);
        }
        return builder;
    });
};
const addJoin = (builder, queryMetadata, join) => {
    const srcEntity = queryMetadata.getEntity(join.sourceTableId);
    const srcColumn = queryMetadata.getColumn(join.sourceTableId, join.sourceFieldId);
    const trgEntity = queryMetadata.getEntity(join.targetTableId);
    const trgPhysicalEntity = Object.assign(new info_commons_1.PhysicalEntity(), trgEntity);
    const trgColumn = trgPhysicalEntity.getPrimaryKeyColumn();
    builder.leftJoin(trgPhysicalEntity.dbTypeName, srcEntity.dbTypeName + '.' + srcColumn.dbCode, trgPhysicalEntity.dbTypeName + '.' + trgColumn.dbCode);
};
exports.default = addJoins;
//# sourceMappingURL=join.js.map