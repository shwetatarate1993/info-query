"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const addLimit = (queryMetadata) => {
    return ((builder) => {
        if (queryMetadata.parsedQuery.limit !== undefined) {
            builder.limit(parseInt(queryMetadata.parsedQuery.limit, 10));
        }
        else {
            builder.limit(1000);
        }
        return builder;
    });
};
exports.default = addLimit;
//# sourceMappingURL=limit.js.map