"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const add_subtract_date_1 = __importDefault(require("add-subtract-date"));
const constants_1 = require("../../constants");
const constants_2 = require("../../constants");
const addCriterias = (queryMetadata) => {
    return ((builder) => {
        for (const criteria of queryMetadata.parsedQuery.criterias) {
            addCriteria(builder, queryMetadata, criteria);
        }
        return builder;
    });
};
const addCriteria = (builder, queryMetadata, criteria) => {
    const column = queryMetadata.getColumn(criteria.field.tableId, criteria.field.fieldId);
    const entity = queryMetadata.getEntity(criteria.field.tableId);
    const fieldName = entity.dbTypeName + '.' + column.dbCode;
    const database = queryMetadata.getDataBaseById(queryMetadata.parsedQuery.databaseId);
    const dialect = database.engine;
    switch (criteria.condition) {
        case constants_1.Condition.STARTS_WITH: {
            if (criteria.value[1][constants_1.CASE_SENSITIVE]) {
                builder.where(fieldName, constants_1.LIKE, criteria.value[0] + '%');
            }
            else {
                builder.whereRaw('LOWER(' + fieldName + ') LIKE ?', criteria.value[0].toLowerCase() + '%');
            }
            break;
        }
        case constants_1.Condition.ENDS_WITH: {
            if (criteria.value[1][constants_1.CASE_SENSITIVE]) {
                builder.where(fieldName, constants_1.LIKE, '%' + criteria.value[0]);
            }
            else {
                builder.whereRaw('LOWER(' + fieldName + ') LIKE ?', '%' + criteria.value[0].toLowerCase());
            }
            break;
        }
        case constants_1.Condition.CONTAINS: {
            if (criteria.value[1][constants_1.CASE_SENSITIVE]) {
                builder.where(fieldName, constants_1.LIKE, '%' + criteria.value[0] + '%');
            }
            else {
                builder.whereRaw('LOWER(' + fieldName + ') LIKE ?', '%' + criteria.value[0].toLowerCase() + '%');
            }
            break;
        }
        case constants_1.Condition.DOES_NOT_CONTAIN: {
            if (criteria.value[1][constants_1.CASE_SENSITIVE]) {
                builder.whereNot(fieldName, constants_1.LIKE, '%' + criteria.value[0] + '%');
            }
            else {
                builder.whereRaw('LOWER(' + fieldName + ') NOT LIKE ?', '%' + criteria.value[0].toLowerCase() + '%');
            }
            break;
        }
        case constants_1.Condition.BETWEEN: {
            builder.whereBetween(fieldName, criteria.value);
            break;
        }
        case constants_1.Condition.IS_NULL: {
            builder.whereNull(fieldName);
            break;
        }
        case constants_1.Condition.NOT_NULL: {
            builder.whereNotNull(fieldName);
            break;
        }
        case constants_1.Condition.EQ: {
            builder.whereIn(fieldName, criteria.value);
            break;
        }
        case constants_1.Condition.DATE: {
            // regex to compare DATE-TIME
            if (criteria.value.toString().match(/^(\d{4})\-(\d{2})\-(\d{2})$/) != null) {
                builder.where(fieldName, constants_1.LIKE, criteria.value + '%');
            }
            else {
                builder.where(fieldName, criteria.value);
            }
            break;
        }
        case constants_1.Condition.MONTH_YEAR: {
            switch (dialect) {
                case constants_2.DBEngine.SQLLITE: {
                    builder.whereRaw('strftime(\'%Y-%m\', ' + fieldName + ') = ?', criteria.value);
                    break;
                }
                case constants_2.DBEngine.MYSQL: {
                    builder.whereRaw('DATE_FORMAT(' + fieldName + ', \'%Y-%m\') = ?', criteria.value);
                    break;
                }
                default:
                    throw new Error('Invalid or Unsupported Database: ' + dialect);
            }
            break;
        }
        case constants_1.Condition.QUARTER_YEAR: {
            const quarter = criteria.value.toString().substring(0, 2);
            const year = criteria.value.toString().substring(3, 7);
            switch (quarter) {
                case 'Q1': {
                    builder.whereBetween(fieldName, [year + '-01-01', year + '-03-31']);
                    break;
                }
                case 'Q2': {
                    builder.whereBetween(fieldName, [year + '-04-01', year + '-06-31']);
                    break;
                }
                case 'Q3': {
                    builder.whereBetween(fieldName, [year + '-07-01', year + '-09-31']);
                    break;
                }
                case 'Q4': {
                    builder.whereBetween(fieldName, [year + '-10-01', year + '-12-31']);
                    break;
                }
            }
            break;
        }
        case constants_1.Condition.RELATIVE: {
            switch (criteria.value) {
                case 'today': {
                    const todaysDate = new Date().toJSON().slice(0, 10);
                    builder.where(fieldName, constants_1.LIKE, todaysDate + '%');
                    break;
                }
                case 'yesterday': {
                    const date = new Date();
                    date.setDate(date.getDate() - 1);
                    const yesterdaysDate = date.toJSON().slice(0, 10);
                    builder.where(fieldName, constants_1.LIKE, yesterdaysDate + '%');
                    break;
                }
                case 'past7days': {
                    const d = new Date();
                    d.setDate(d.getDate() - 7);
                    const pastSevenDaysDate = d.toJSON().slice(0, 10);
                    const date = new Date();
                    date.setDate(date.getDate() - 1);
                    const yesterdaysDate = date.toJSON().slice(0, 10);
                    builder.whereBetween(fieldName, [pastSevenDaysDate, yesterdaysDate]);
                    break;
                }
                case 'past30days': {
                    const d = new Date();
                    d.setDate(d.getDate() - 30);
                    const pastThirtyDaysDate = d.toJSON().slice(0, 10);
                    const date = new Date();
                    date.setDate(date.getDate() - 1);
                    const yesterdaysDate = date.toJSON().slice(0, 10);
                    builder.whereBetween(fieldName, [pastThirtyDaysDate, yesterdaysDate]);
                    break;
                }
                case 'lastweek': {
                    const d = new Date();
                    const to = d.setTime(d.getTime() - (d.getDay() ? d.getDay() + 1 : 7) * 24 * 60 * 60 * 1000);
                    const frm = d.setTime(d.getTime() - 6 * 24 * 60 * 60 * 1000);
                    builder.whereBetween(fieldName, [new Date(frm).toJSON().slice(0, 10),
                        new Date(to).toJSON().slice(0, 10)]);
                    break;
                }
                case 'lastmonth': {
                    const now = new Date();
                    const prevMonthLastDate = new Date(now.getFullYear(), now.getMonth(), 0);
                    const prevMonthFirstDate = new Date(now.getFullYear() - (now.getMonth() > 0 ? 0 : 1), (now.getMonth() - 1 + 12) % 12, 1);
                    builder.whereBetween(fieldName, [prevMonthFirstDate.toJSON().slice(0, 10),
                        prevMonthLastDate.toJSON().slice(0, 10)]);
                    break;
                }
                case 'lastyear': {
                    const lastYear = new Date().getFullYear() - 1;
                    builder.whereBetween(fieldName, [lastYear + '-01-01', lastYear + '-12-31']);
                    break;
                }
                case 'thisweek': {
                    const currentDate = new Date(); // get current date
                    const first = currentDate.getDate() - currentDate.getDay();
                    const last = first + 6;
                    const firstDay = new Date(currentDate.setDate(first));
                    const date = new Date();
                    date.setDate(last);
                    builder.whereBetween(fieldName, [firstDay.toJSON().slice(0, 10),
                        date.toJSON().slice(0, 10)]);
                    break;
                }
                case 'thismonth': {
                    const month = new Date().getMonth();
                    const year = new Date().getFullYear();
                    builder.whereBetween(fieldName, [year + '-' + month + '-01', year + '-' +
                            month + '-31']);
                    break;
                }
                case 'thisyear': {
                    const year = new Date().getFullYear();
                    builder.whereBetween(fieldName, [year + '-01-01', year + '-12-31']);
                    break;
                }
                default:
                    throw new Error('Invalid or Unsupported Relative Filter' + criteria.value);
            }
            break;
        }
        case constants_1.Condition.TIME_INTERVAL: {
            switch (criteria.value[1]) {
                case 'day': {
                    previousOrCurrentDayFilter(builder, fieldName, criteria);
                    break;
                }
                case 'week': {
                    previousOrCurrentWeekFilter(builder, fieldName, criteria);
                    break;
                }
                case 'month': {
                    previousOrCurrentMonthFilter(builder, fieldName, criteria);
                    break;
                }
                case 'year': {
                    previousOrCurrentYearFilter(builder, fieldName, criteria);
                    break;
                }
                case 'minute': {
                    previousOrCurrentMinuteFilter(builder, fieldName, criteria);
                    break;
                }
                case 'hour': {
                    previousOrCurrentHourFilter(builder, fieldName, criteria);
                    break;
                }
            }
            break;
        }
        case constants_1.Condition.NE: {
            builder.whereNotIn(fieldName, criteria.value);
            break;
        }
        case constants_1.Condition.GT:
        case constants_1.Condition.GE:
        case constants_1.Condition.LT:
        case constants_1.Condition.LE:
            builder.where(fieldName, criteria.condition, criteria.value);
            break;
        default:
            throw new Error('Invalid or Unsupported Criteria Condition :' + JSON.stringify(criteria));
    }
};
const previousOrCurrentDayFilter = (builder, fieldName, criteria) => {
    const date1 = new Date();
    const date2 = new Date();
    if (criteria.value[0] === 0) {
        builder.where(fieldName, constants_1.LIKE, date1.toJSON().slice(0, 10) + '%');
    }
    else if (criteria.value[0] < 0) {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            date1.setDate(date1.getDate() + (criteria.value[0] + 1));
            date2.setTime(constants_1.MAX_TIMESTAMP);
            builder.whereBetween(fieldName, [date1, date2]);
        }
        else {
            date1.setDate(date1.getDate() + criteria.value[0]);
            date2.setDate(date2.getDate() - 1);
            date2.setTime(constants_1.MAX_TIMESTAMP);
            builder.whereBetween(fieldName, [date1.toJSON().slice(0, 10), date2.toJSON().slice(0, 10)]);
        }
    }
    else {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            date1.setDate(date1.getDate() + (criteria.value[0] - 1));
            date1.setTime(constants_1.MAX_TIMESTAMP);
            builder.whereBetween(fieldName, [date2.toJSON().slice(0, 10), date1.toJSON().slice(0, 10)]);
        }
        else {
            date1.setDate(date1.getDate() + criteria.value[0]);
            date1.setTime(constants_1.MAX_TIMESTAMP);
            date2.setDate(date2.getDate() + 1);
            builder.whereBetween(fieldName, [date2.toJSON().slice(0, 10), date1.toJSON().slice(0, 10)]);
        }
    }
};
const previousOrCurrentWeekFilter = (builder, fieldName, criteria) => {
    const currentDate = new Date(); // get current date
    const first = currentDate.getDate() - currentDate.getDay();
    const last = first + 6;
    const firstDay = new Date(currentDate.setDate(first));
    const dt = new Date();
    dt.setDate(last);
    const date = new Date();
    if (criteria.value[0] === 0) {
        builder.whereBetween(fieldName, [firstDay.toJSON().slice(0, 10),
            dt.toJSON().slice(0, 10)]);
    }
    else if (criteria.value[0] < 0) {
        const subtractWeekDate = add_subtract_date_1.default.subtract(date, (criteria.value[0] - 1), 'week');
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            builder.whereBetween(fieldName, [subtractWeekDate.toJSON().slice(0, 10),
                dt.toJSON().slice(0, 10)]);
        }
        else {
            const fromWeekDate = add_subtract_date_1.default.subtract(date, criteria.value[0], 'week');
            builder.whereBetween(fieldName, [fromWeekDate.toJSON().slice(0, 10),
                subtractWeekDate.toJSON().slice(0, 10)]);
        }
    }
    else {
        const addedWeekDate = add_subtract_date_1.default.add(date, (criteria.value[0] - 1), 'week');
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            builder.whereBetween(fieldName, [firstDay.toJSON().slice(0, 10),
                addedWeekDate.toJSON().slice(0, 10)]);
        }
        else {
            const fromWeekDate = add_subtract_date_1.default.add(date, 1, 'week');
            const toWeekDate = add_subtract_date_1.default.add(date, criteria.value[0], 'week');
            builder.whereBetween(fieldName, [fromWeekDate.toJSON().slice(0, 10),
                toWeekDate.toJSON().slice(0, 10)]);
        }
    }
};
const previousOrCurrentMonthFilter = (builder, fieldName, criteria) => {
    const date = new Date();
    const date1 = new Date();
    if (criteria.value[0] === 0) {
        builder.whereBetween(fieldName, [date.getFullYear() + '-' + date.getMonth() + '-01',
            date.getFullYear() + '-' + date.getMonth() + '-31']);
    }
    else if (criteria.value[0] < 0) {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            date.setMonth(date.getMonth() + (criteria.value[0] + 1));
            builder.whereBetween(fieldName, [date.getFullYear() + '-' + date.getMonth() + '-01',
                date1.getFullYear() + '-' + date1.getMonth() + '-31']);
        }
        else {
            date.setMonth(date.getMonth() + criteria.value[0]);
            date1.setMonth(date1.getMonth() - 1);
            builder.whereBetween(fieldName, [date.getFullYear() + '-' + date.getMonth() + '-01',
                date1.getFullYear() + '-' + date1.getMonth() + '-31']);
        }
    }
    else {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            date1.setMonth(date1.getMonth() + (criteria.value[0] - 1));
            builder.whereBetween(fieldName, [date.getFullYear() + '-' + date.getMonth() + '-01',
                date1.getFullYear() + '-' + date1.getMonth() + '-31']);
        }
        else {
            date.setMonth(date.getMonth() + 1);
            date1.setMonth(date1.getMonth() + criteria.value[0]);
            builder.whereBetween(fieldName, [date.getFullYear() + '-' + date.getMonth() + '-01',
                date1.getFullYear() + '-' + date1.getMonth() + '-31']);
        }
    }
};
const previousOrCurrentYearFilter = (builder, fieldName, criteria) => {
    const date = new Date();
    if (criteria.value[0] === 0) {
        builder.whereBetween(fieldName, [date.getFullYear() + '-01-01', date.getFullYear() + '-12-31']);
    }
    else if (criteria.value[0] < 0) {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            date.setFullYear(date.getFullYear() + (criteria.value[0] + 1));
            builder.whereBetween(fieldName, [date.getFullYear() + '-01-01', new Date().getFullYear() + '-12-31']);
        }
        else {
            builder.whereBetween(fieldName, [(date.getFullYear() + criteria.value[0]) + '-01-01',
                (date.getFullYear() - 1) + '-12-31']);
        }
    }
    else {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            date.setFullYear(date.getFullYear() + (criteria.value[0] - 1));
            builder.whereBetween(fieldName, [new Date().getFullYear() + '-01-01',
                date.getFullYear() + '-12-31']);
        }
        else {
            builder.whereBetween(fieldName, [(date.getFullYear() + 1) + '-01-01',
                (date.getFullYear() + criteria.value[0]) + '-12-31']);
        }
    }
};
const previousOrCurrentMinuteFilter = (builder, fieldName, criteria) => {
    const date = new Date();
    if (criteria.value[0] === 0) {
        builder.where(fieldName, constants_1.Condition.EQ, date);
    }
    else if (criteria.value[0] < 0) {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            const subtractMinutesDate = add_subtract_date_1.default.subtract(date, (criteria.value[0] - 1), 'minutes');
            builder.whereBetween(fieldName, [subtractMinutesDate, date]);
        }
        else {
            const subtractMinutesFromDate = add_subtract_date_1.default.subtract(date, criteria.value[0], 'minutes');
            const subtractMinutesToDate = add_subtract_date_1.default.subtract(date, 1, 'minutes');
            builder.whereBetween(fieldName, [subtractMinutesFromDate, subtractMinutesToDate]);
        }
    }
    else {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            const addedMinutesDate = add_subtract_date_1.default.add(date, (criteria.value[0] - 1), 'minutes');
            builder.whereBetween(fieldName, [date, addedMinutesDate]);
        }
        else {
            const fromMinutesDate = add_subtract_date_1.default.add(date, 1, 'minutes');
            const toMinutesDate = add_subtract_date_1.default.add(date, criteria.value[0], 'minutes');
            builder.whereBetween(fieldName, [fromMinutesDate, toMinutesDate]);
        }
    }
};
const previousOrCurrentHourFilter = (builder, fieldName, criteria) => {
    const date = new Date();
    if (criteria.value[0] === 0) {
        builder.where(fieldName, constants_1.Condition.EQ, date);
    }
    else if (criteria.value[0] < 0) {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            const subtractedHoursDate = add_subtract_date_1.default.subtract(date, (criteria.value[0] - 1), 'hours');
            builder.whereBetween(fieldName, [subtractedHoursDate, date]);
        }
        else {
            const subtractedHoursFromDate = add_subtract_date_1.default.subtract(date, criteria.value[0], 'hours');
            const subtractedHoursToDate = add_subtract_date_1.default.subtract(date, 1, 'hours');
            builder.whereBetween(fieldName, [subtractedHoursFromDate, subtractedHoursToDate]);
        }
    }
    else {
        if (criteria.value[2][constants_1.INCLUDE_CURRENT] !== undefined) {
            const addedHoursDate = add_subtract_date_1.default.add(date, (criteria.value[0] - 1), 'hours');
            builder.whereBetween(fieldName, [date, addedHoursDate]);
        }
        else {
            const fromHoursDate = add_subtract_date_1.default.add(date, 1, 'hours');
            const toHoursDate = add_subtract_date_1.default.add(date, criteria.value[0], 'hours');
            builder.whereBetween(fieldName, [fromHoursDate, toHoursDate]);
        }
    }
};
exports.default = addCriterias;
//# sourceMappingURL=criteria.js.map