"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
const config_1 = __importDefault(require("../../config"));
const knex_config_1 = require("knex-config");
const metadata_1 = require("../../metadata");
const mockdata_1 = require("../../mockdata");
const mockdata_2 = require("../../mockdata");
const sqlite_db_1 = require("../../mockdata/sqlite.db");
const services_1 = require("../../services");
beforeAll(async () => {
    await sqlite_db_1.createTodoTable();
    await sqlite_db_1.createOrderTable();
    await sqlite_db_1.createProductTable();
    await sqlite_db_1.createPeopleTable();
    sinon_1.default.stub(services_1.queryMetadataService, 'getTableById').resolves(mockdata_1.tables);
});
beforeEach(async () => {
    await sqlite_db_1.seedTodoTable();
    await sqlite_db_1.seedOrderTable();
    await sqlite_db_1.seedProductTable();
    await sqlite_db_1.seedPeopleTable();
    this.mock = sinon_1.default.mock(services_1.queryMetadataService);
    this.mock.expects('queryMetadata').returns({ databases: mockdata_1.databases, tables: mockdata_1.tables, entities: mockdata_1.entities });
});
afterEach(async () => {
    await sqlite_db_1.truncateTodoTable();
    await sqlite_db_1.truncateOrderTable();
    await sqlite_db_1.truncateProductTable();
    await sqlite_db_1.trucatePeopleTable();
    this.mock.restore();
});
const knex = knex_config_1.knexClient(config_1.default.get('db'), '1001', 'sqlite3');
const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NTIzMDg1NzZ9.aN5_'
    + 'UtTKxIMAhGNnrigYAJxVs_-W-U4fb8zHqGvm26w';
test('sql query generated with one filter condition', async () => {
    sinon_1.default.stub(services_1.queryMetadataService, 'getTableIdByFieldId').resolves('1');
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.sqliteQuery));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    expect(queryMetadata.parsedQuery).not.toBeNull();
    expect(queryMetadata.parsedQuery.criterias[0].field.fieldId).toEqual('1');
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
        expect(tableResultFromExecutor[0].ID).toEqual(4);
        expect(tableResultFromExecutor[0].sum).toEqual(10);
    }
    catch (err) {
        throw err;
    }
});
test('Between Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.betweenCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    expect(queryMetadata.parsedQuery).not.toBeNull();
    expect(queryMetadata.parsedQuery.criterias[0].field.fieldId).toEqual('11');
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(2);
    }
    catch (err) {
        throw err;
    }
});
test('Simple Native Query with all Attributes of QueryRequest object', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.simpleNativeQuery));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].tasks).toEqual('Go get coffee');
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with MIN aggregation & one GROUP BY on PRODUCT_ID', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeMinAggregationAndGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].Quantity).toEqual(2);
        expect(tableResultFromExecutor[1].Quantity).toEqual(5);
        expect(tableResultFromExecutor[2].Quantity).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('Custom Query with MAX aggregation', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.simpleCustomMaxAggregation));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(1);
        expect(tableResultFromExecutor[0].max).toEqual(5);
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with MAX aggregation', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeMaxAggregation));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(1);
        expect(tableResultFromExecutor[0].Quantity).toEqual(5);
    }
    catch (err) {
        throw err;
    }
});
test('Custom Query with AVG aggregation', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.simpleCustomAvgAggregation));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(1);
        expect(tableResultFromExecutor[0].avg).toEqual(3.2);
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with AVG aggregation', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeAvgAggregation));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(1);
        expect(tableResultFromExecutor[0].Quantity).toEqual(3.2);
    }
    catch (err) {
        throw err;
    }
});
test('Simple Custom Query with Limit Only', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.simpleCustomQueryLimit));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(2);
    }
    catch (err) {
        throw err;
    }
});
test('Simple Native Query with Limit Only', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.simpleNativeQueryLimit));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(2);
    }
    catch (err) {
        throw err;
    }
});
test('Custom Query with COUNT & SUM aggregation and GROUP BY on PRODUCT_ID', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.customQueryWithAggregations));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].count).toEqual(1);
        expect(tableResultFromExecutor[1].count).toEqual(2);
        expect(tableResultFromExecutor[2].count).toEqual(2);
        expect(tableResultFromExecutor[0].sum).toEqual(0);
        expect(tableResultFromExecutor[2].sum).toEqual(8.41);
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with COUNT & SUM aggregation and GROUP BY on PRODUCT_ID', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeQueryWithAggregations));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].count).toEqual(1);
        expect(tableResultFromExecutor[1].count).toEqual(2);
        expect(tableResultFromExecutor[2].count).toEqual(2);
        expect(tableResultFromExecutor[0].totalTax).toEqual(0);
        expect(tableResultFromExecutor[2].totalTax).toEqual(8.41);
    }
    catch (err) {
        throw err;
    }
});
test('Custom Query with SUM aggregation & one GROUP BY on PRODUCT_ID', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.customQueryWithOneGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].sum).toEqual(2);
        expect(tableResultFromExecutor[1].sum).toEqual(10);
        expect(tableResultFromExecutor[2].sum).toEqual(4);
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with SUM aggregation & one GROUP BY on PRODUCT_ID', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeQueryWithOneGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].Quantity).toEqual(2);
        expect(tableResultFromExecutor[1].Quantity).toEqual(10);
        expect(tableResultFromExecutor[2].Quantity).toEqual(4);
    }
    catch (err) {
        throw err;
    }
});
test('Custom Query with SUM aggregation & two GROUP BY on PRODUCT_ID & DISCOUNT', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.customQueryWithTwoGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(5);
        expect(tableResultFromExecutor[0].sum).toEqual(2);
        expect(tableResultFromExecutor[1].sum).toEqual(5);
        expect(tableResultFromExecutor[2].sum).toEqual(5);
        expect(tableResultFromExecutor[3].sum).toEqual(3);
        expect(tableResultFromExecutor[4].sum).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with SUM aggregation & two GROUP BY on PRODUCT_ID & DISCOUNT', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeQueryWithTwoGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(5);
        expect(tableResultFromExecutor[0].Quantity).toEqual(2);
        expect(tableResultFromExecutor[1].Quantity).toEqual(5);
        expect(tableResultFromExecutor[2].Quantity).toEqual(5);
        expect(tableResultFromExecutor[3].Quantity).toEqual(3);
        expect(tableResultFromExecutor[4].Quantity).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('Custome Query with Sum & Avg aggregation and 2 Group By clause', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.customQueryWithTwoAggregationAndGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(5);
        expect(Object.keys(tableResultFromExecutor[0]).length).toEqual(4);
        expect('sum' in tableResultFromExecutor[0]).toBeTruthy();
        expect('avg' in tableResultFromExecutor[0]).toBeTruthy();
        expect('DISCOUNT' in tableResultFromExecutor[0]).toBeTruthy();
        expect(tableResultFromExecutor[0].sum).toEqual(2);
        expect(tableResultFromExecutor[0].avg).toEqual(3);
    }
    catch (err) {
        throw err;
    }
});
test('Native Query with Sum & Avg aggregation and 2 Group By clause', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.nativeQueryWithTwoAggregationAndGroupBy));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(5);
        expect(Object.keys(tableResultFromExecutor[0]).length).toEqual(2);
        expect('Total_Quantity' in tableResultFromExecutor[0]).toBeTruthy();
        expect('Average_Discount' in tableResultFromExecutor[0]).toBeTruthy();
        expect(tableResultFromExecutor[0].Total_Quantity).toEqual(2);
        expect(tableResultFromExecutor[0].Average_Discount).toEqual(3);
    }
    catch (err) {
        throw err;
    }
});
test('Hybrid Custom Query with (AGGREGATION, GROUP BY, ORDER BY, LIMIT)', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.customHybridQuery));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].sum).toEqual(9.059999999999999);
    }
    catch (err) {
        throw err;
    }
});
test('Hybrid Native Query with (FILTER, AGGREGATION, GROUP BY, ORDER BY, LIMIT) '
    + 'and missing Attributes of QueryRequest object', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_2.queryRequests.hybridNativeQuery));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.length).toEqual(3);
        expect(tableResultFromExecutor[0].TITLE).toEqual('Awesome Linen Plate');
        expect(tableResultFromExecutor[0].totalTax).toEqual(8.41);
    }
    catch (err) {
        throw err;
    }
});
//# sourceMappingURL=knex.query.executor.test.js.map