"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
const config_1 = __importDefault(require("../../config"));
const knex_config_1 = require("knex-config");
const metadata_1 = require("../../metadata");
const mockdata_1 = require("../../mockdata");
const mockdata_2 = require("../../mockdata");
const sqlite_db_1 = require("../../mockdata/sqlite.db");
const services_1 = require("../../services");
let mock;
beforeAll(async () => {
    await sqlite_db_1.createTodoTable();
    await sqlite_db_1.createOrderTable();
    await sqlite_db_1.createProductTable();
    await sqlite_db_1.createPeopleTable();
    sinon_1.default.stub(services_1.queryMetadataService, 'getTableById').resolves(mockdata_2.tables);
});
beforeEach(async () => {
    await sqlite_db_1.seedTodoTable();
    await sqlite_db_1.seedOrderTable();
    await sqlite_db_1.seedProductTable();
    await sqlite_db_1.seedPeopleTable();
    mock = sinon_1.default.mock(services_1.queryMetadataService);
    mock.expects('queryMetadata').returns({ databases: mockdata_2.databases, tables: mockdata_2.tables, entities: mockdata_2.entities });
});
afterEach(async () => {
    await sqlite_db_1.truncateTodoTable();
    await sqlite_db_1.truncateOrderTable();
    await sqlite_db_1.truncateProductTable();
    await sqlite_db_1.trucatePeopleTable();
    mock.restore();
});
const knex = knex_config_1.knexClient(config_1.default.get('db'), '1001', 'sqlite3');
const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NTIzMDg1NzZ9.aN5_'
    + 'UtTKxIMAhGNnrigYAJxVs_-W-U4fb8zHqGvm26w';
test('sql query generated with date equal filter criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeEqualCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime between filter criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeBetweenCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(2);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime On filter criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeOnCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime MONTH_YEAR filter criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeMonthYearCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(3);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime QUARTER_YEAR filter criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeQuarterYearCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(5);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative today filter criteria', async () => {
    // Inserting record on yesterday's date to test 'today' relative criteria
    const todaysDate = new Date().toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: todaysDate, DISCOUNT: '', ID: 5, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.dateTimeRelativeTodayCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative yesterday filter criteria', async () => {
    // Inserting record on yesterday's date to test 'Yesterday' relative criteria
    const date = new Date();
    date.setDate(date.getDate() - 1);
    const yesterdaysDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: yesterdaysDate, DISCOUNT: '', ID: 5, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativePast7DayCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative past 7 days filter criteria', async () => {
    // Inserting record for past dates to test 'past 7 days' relative criteria
    const date = new Date();
    date.setDate(date.getDate() - 1);
    const yesterdaysDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: yesterdaysDate, DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    date.setDate(date.getDate() - 2);
    const threeDaysBackDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: threeDaysBackDate, DISCOUNT: '', ID: 7, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    date.setDate(date.getDate() - 6);
    const nineDaysBackDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: nineDaysBackDate, DISCOUNT: '', ID: 8, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativePast7DayCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(2);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative past 30 days filter criteria', async () => {
    // Inserting record on past date to test 'past 30 days' relative criteria
    const date = new Date();
    date.setDate(date.getDate() - 1);
    const yesterdaysDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: yesterdaysDate, DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    date.setDate(date.getDate() - 2);
    const threeDaysBackDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: threeDaysBackDate, DISCOUNT: '', ID: 7, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    date.setDate(date.getDate() - 35);
    const nineDaysBackDate = date.toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: nineDaysBackDate, DISCOUNT: '', ID: 8, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativePast30DayCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(2);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative last week filter criteria', async () => {
    // Inserting record on last week dates
    const today = new Date();
    const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 6);
    await knex('ORDERS').insert({
        CREATED_AT: lastWeek.toJSON().slice(0, 10), DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativeLastWeekCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative last month filter criteria', async () => {
    // Inserting record on last month dates
    const now = new Date();
    const prevMonthLastDate = new Date(now.getFullYear(), now.getMonth(), 0);
    const prevMonthFirstDate = new Date(now.getFullYear() - (now.getMonth() > 0 ? 0 : 1), (now.getMonth() - 1 + 12) % 12, 1);
    await knex('ORDERS').insert({
        CREATED_AT: new Date(prevMonthFirstDate).toJSON().slice(0, 10),
        DISCOUNT: '', ID: 6, PRODUCT_ID: 5, QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65,
        TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativeLastMonthCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative last year filter criteria', async () => {
    // Inserting records on last year dates
    const lastYear = new Date().getFullYear() - 1;
    await knex('ORDERS').insert({
        CREATED_AT: lastYear + '-03-01', DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativeLastYearCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative this week filter criteria', async () => {
    // Clear Order table
    await knex('ORDERS').truncate();
    // Inserting records on current week dates
    const todaysDate = new Date().toJSON().slice(0, 10);
    await knex('ORDERS').insert({
        CREATED_AT: todaysDate, DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativeThisWeekCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative this month filter criteria', async () => {
    // Clear Order table
    await knex('ORDERS').truncate();
    // Inserting records on current month dates
    const month = new Date().getMonth();
    const year = new Date().getFullYear();
    await knex('ORDERS').insert({
        CREATED_AT: year + '-' + month + '-01', DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativeThisMonthCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('sql query generated with datetime relative this year filter criteria', async () => {
    // Clear Order table
    await knex('ORDERS').truncate();
    // Inserting records on current year dates
    const year = new Date().getFullYear();
    await knex('ORDERS').insert({
        CREATED_AT: year + '-03' + '-01', DISCOUNT: '', ID: 6, PRODUCT_ID: 5,
        QUANTITY: 1, SUBTOTAL: 116.35, TAX: 4.65, TOTAL: 121.02, USER_ID: 3,
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria
        .dateTimeRelativeThisYearCriterias));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    try {
        const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
        expect(tableResultFromExecutor.status).toEqual(tableResultFromExecutor.completed);
        expect(tableResultFromExecutor.length).toEqual(1);
    }
    catch (err) {
        throw err;
    }
});
test('Previous day filter with 0 as Input means previous 0 Day(i.e today)', async () => {
    // inserting record with current date
    let date = new Date();
    await knex('PRODUCTS').insert({
        CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 6,
        PRICE: 1, RATING: 1, TITLE: '', VENDOR: '',
    });
    // inserting record with yesterday's Date
    date.setDate(date.getDate() - 1);
    await knex('PRODUCTS').insert({
        CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 7,
        PRICE: 1, RATING: 1, TITLE: '', VENDOR: '',
    });
    // inserting record with 2 days back Date
    date = new Date();
    date.setDate(date.getDate() - 2);
    await knex('PRODUCTS').insert({
        CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 8,
        PRICE: 1, RATING: 1, TITLE: '', VENDOR: '',
    });
    // inserting record with 3 days back Date
    date = new Date();
    date.setDate(date.getDate() - 3);
    await knex('PRODUCTS').insert({
        CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 9,
        PRICE: 1, RATING: 1, TITLE: '', VENDOR: '',
    });
    // inserting tommorow's Date
    date = new Date();
    date.setDate(date.getDate() + 1);
    await knex('PRODUCTS').insert({
        CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 10,
        PRICE: 1, RATING: 1, TITLE: '', VENDOR: '',
    });
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.dateTimeCriteria.previousZeroDay));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    expect(queryMetadata.parsedQuery).not.toBeNull();
    expect(queryMetadata.parsedQuery.criterias[0].field.fieldId).toEqual('8');
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(1);
});
/*test('Previous day filter', async () => {
    // inserting record with current date
    let date: Date = new Date();
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 6,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

     // inserting record with yesterday's Date
    date.setDate(date.getDate() - 1);
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 7,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    // inserting record with 2 days back Date
    date = new Date();
    date.setDate(date.getDate() - 2);
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 8,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    // inserting record with 3 days back Date
    date = new Date();
    date.setDate(date.getDate() - 3);
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 9,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    const queryRequest: QueryRequest = JSON.parse(JSON.stringify(dateTimeCriteria.previousDay));
    const queryMetadata: QueryMetadata = new QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor: QueryExecutor = await createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);

    expect(tableResultFromExecutor.length).toEqual(2);

    // verifying 1 day back date
    date = new Date();
    date.setDate(date.getDate() - 1);
    expect(tableResultFromExecutor[0].CREATED_AT).toContain(date.toJSON().slice(0, 10));

    // verifying 2 days back date
    date = new Date();
    date.setDate(date.getDate() - 2);
    expect(tableResultFromExecutor[1].CREATED_AT).toContain(date.toJSON().slice(0, 10));

});

test('Previous day filter include current day', async () => {
    // inserting record with current date
    let date: Date = new Date();
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 6,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    // inserting record with yesterday's Date(Not needed for this test but still inserting)
    date.setDate(date.getDate() - 1);
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 7,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    // inserting record with 2 days back Date
    date = new Date();
    date.setDate(date.getDate() - 2);
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 8,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    // inserting record with 3 days back Date
    date = new Date();
    date.setDate(date.getDate() - 3);
    await knex('PRODUCTS').insert({CREATED_AT: date.toJSON(), CATEGORY: '', EAN: 5, ID: 9,
                                    PRICE: 1, RATING: 1, TITLE: '', VENDOR: ''});

    const queryRequest: QueryRequest = JSON.parse(JSON.stringify(dateTimeCriteria.previousDayIncludeCurrent));
    const queryMetadata: QueryMetadata = new QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor: QueryExecutor = await createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(2);

    //verifying todays date
    date = new Date();
    expect(tableResultFromExecutor[0].CREATED_AT).toContain(date.toJSON().slice(0,10));

    //verifying yesterdays date
    date.setDate(date.getDate() - 1);
    expect(tableResultFromExecutor[1].CREATED_AT).toContain(date.toJSON().slice(0,10));

});*/
//# sourceMappingURL=criteria.datetime.test.js.map