"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
const metadata_1 = require("../../metadata");
const mockdata_1 = require("../../mockdata");
const mockdata_2 = require("../../mockdata");
const sqlite_db_1 = require("../../mockdata/sqlite.db");
const services_1 = require("../../services");
let mock;
beforeAll(async () => {
    await sqlite_db_1.createTodoTable();
    await sqlite_db_1.createOrderTable();
    await sqlite_db_1.createProductTable();
    await sqlite_db_1.createPeopleTable();
    sinon_1.default.stub(services_1.queryMetadataService, 'getTableById').resolves(mockdata_2.tables);
});
beforeEach(async () => {
    await sqlite_db_1.seedTodoTable();
    await sqlite_db_1.seedOrderTable();
    await sqlite_db_1.seedProductTable();
    await sqlite_db_1.seedPeopleTable();
    mock = sinon_1.default.mock(services_1.queryMetadataService);
    mock.expects('queryMetadata').returns({ databases: mockdata_2.databases, tables: mockdata_2.tables, entities: mockdata_2.entities });
});
afterEach(async () => {
    await sqlite_db_1.truncateTodoTable();
    await sqlite_db_1.truncateOrderTable();
    await sqlite_db_1.truncateProductTable();
    await sqlite_db_1.trucatePeopleTable();
    mock.restore();
});
const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NTIzMDg1NzZ9.aN5_'
    + 'UtTKxIMAhGNnrigYAJxVs_-W-U4fb8zHqGvm26w';
test('Numeric Equal Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.equalCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    expect(queryMetadata.parsedQuery).not.toBeNull();
    expect(queryMetadata.parsedQuery.criterias[0].field.fieldId).toEqual('17');
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(2);
    expect(tableResultFromExecutor[0].ID).toEqual(3);
    expect(tableResultFromExecutor[1].ID).toEqual(4);
});
test('Numeric Not Equal Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.notEqualCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(3);
});
test('Numeric Greater Than Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.greaterThanCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(2);
});
test('Numeric Less Than Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.lessThanCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(2);
});
test('Numeric Between Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.betweenCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(4);
});
test('Numeric Greater Than Or Equal Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.greaterThanOrEqualCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(3);
});
test('Numeric Less Than Or Equal Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.lessThanOrEqualCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(3);
});
test('Numeric Is_Empty Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.isEmptyCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(0);
});
test('Numeric Not_Empty Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.numericCriteria.notEmptyCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(5);
});
//# sourceMappingURL=criteria.numeric.test.js.map