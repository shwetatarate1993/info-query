"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
const metadata_1 = require("../../metadata");
const mockdata_1 = require("../../mockdata");
const mockdata_2 = require("../../mockdata");
const sqlite_db_1 = require("../../mockdata/sqlite.db");
const services_1 = require("../../services");
let mock;
beforeAll(async () => {
    await sqlite_db_1.createTodoTable();
    await sqlite_db_1.createOrderTable();
    await sqlite_db_1.createProductTable();
    await sqlite_db_1.createPeopleTable();
    sinon_1.default.stub(services_1.queryMetadataService, 'getTableById').resolves(mockdata_2.tables);
});
beforeEach(async () => {
    await sqlite_db_1.seedTodoTable();
    await sqlite_db_1.seedOrderTable();
    await sqlite_db_1.seedProductTable();
    await sqlite_db_1.seedPeopleTable();
    mock = sinon_1.default.mock(services_1.queryMetadataService);
    mock.expects('queryMetadata').returns({ databases: mockdata_2.databases, tables: mockdata_2.tables, entities: mockdata_2.entities });
});
afterEach(async () => {
    await sqlite_db_1.truncateTodoTable();
    await sqlite_db_1.truncateOrderTable();
    await sqlite_db_1.truncateProductTable();
    await sqlite_db_1.trucatePeopleTable();
    mock.restore();
});
const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NTIzMDg1NzZ9.aN5_'
    + 'UtTKxIMAhGNnrigYAJxVs_-W-U4fb8zHqGvm26w';
test('String IS Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.iSCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    expect(queryMetadata.parsedQuery).not.toBeNull();
    expect(queryMetadata.parsedQuery.criterias[0].field.fieldId).toEqual('2');
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(1);
    expect(tableResultFromExecutor[0].TITLE).toEqual('Rustic Bronze Pants');
});
test('String Is_Not Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.iSNotCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(4);
});
test('String Contains Criteria with Case Insensitive', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.containsCriteriaCaseInsensitive));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(2);
});
/*test('String Contains Criteria with Case Sensitive', async () => {
    const queryRequest: QueryRequest = JSON.parse(JSON.stringify(stringCriteria.containsCriteriaCaseSensitive));
    const queryMetadata: QueryMetadata = new QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor: QueryExecutor = await createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);

    expect(tableResultFromExecutor.length).toEqual(0);
});*/
test('String Does Not Contain Criteria Case Insensitive', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.doesNotContainCriteriaCaseInsensitive));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(3);
});
/*test('String Does Not Contain Criteria Case Sensitive', async () => {
    const queryRequest: QueryRequest = JSON.parse(JSON.stringify(stringCriteria.doesNotContainCriteriaCaseSensitive));
    const queryMetadata: QueryMetadata = new QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor: QueryExecutor = await createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);

    expect(tableResultFromExecutor.length).toEqual(0);
});*/
test('String Is Empty Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.iSEmptyCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(0);
});
test('String Is Not Empty Criteria', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.iSNotEmptyCriteria));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(5);
});
test('String Starts With Criteria Case Insensitive', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.startsWithCriteriaCaseInsensitive));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(2);
});
/*test('String Starts With Criteria Case Sensitive', async () => {
    const queryRequest: QueryRequest = JSON.parse(JSON.stringify(stringCriteria.startsWithCriteriaCaseSensitive));
    const queryMetadata: QueryMetadata = new QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor: QueryExecutor = await createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);

    expect(tableResultFromExecutor.length).toEqual(0);
});*/
test('String Ends With Criteria Case Insensitive', async () => {
    const queryRequest = JSON.parse(JSON.stringify(mockdata_1.stringCriteria.endsWithCriteriaCaseInsensitive));
    const queryMetadata = new metadata_1.QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor = await __1.createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);
    expect(tableResultFromExecutor.length).toEqual(3);
});
/*test('String Ends With Criteria Case Sensitive', async () => {
    const queryRequest: QueryRequest = JSON.parse(JSON.stringify(stringCriteria.endsWithCriteriaCaseSensitive));
    const queryMetadata: QueryMetadata = new QueryMetadata(queryRequest);
    await queryMetadata.init(jwt);
    const queryExecutor: QueryExecutor = await createQueryExecutor(queryMetadata);
    const tableResultFromExecutor = await queryExecutor.executeQuery(queryMetadata, jwt);

    expect(tableResultFromExecutor.length).toEqual(0);
});*/
//# sourceMappingURL=criteria.string.test.js.map