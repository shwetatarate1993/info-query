"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const ioknex_1 = __importDefault(require("./ioknex"));
exports.createQueryExecutor = async (queryMetadata) => {
    const database = await queryMetadata.getDataBaseById(constants_1.NATIVE === queryMetadata.queryType ?
        queryMetadata.parsedNativeQuery.database : queryMetadata.parsedQuery.databaseId);
    const dbEngine = constants_1.parseDBEngineEnum(database.engine);
    switch (dbEngine) {
        case constants_1.DBEngine.SQLLITE:
            return ioknex_1.default;
        case constants_1.DBEngine.MYSQL:
            return ioknex_1.default;
        default:
            throw new Error(' No Query Executor found for DBEngine ' + dbEngine);
    }
};
//# sourceMappingURL=index.js.map