"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const parser_1 = require("../parser");
const services_1 = require("../services");
class QueryMetadata {
    constructor(queryReq) {
        this.queryReq = queryReq;
        this.queryType = queryReq.type;
    }
    get parsedQuery() {
        return this._parsedQuery;
    }
    get parsedNativeQuery() {
        return this._parsedNativeQuery;
    }
    async init(jwtToken) {
        if (constants_1.NATIVE === this.queryType) {
            this._parsedNativeQuery = await parser_1.queryParser.parseNativeQuery(this.queryReq);
            this.queryMetadata = await services_1.outboundQueryMetadataService.queryMetadata(this._parsedNativeQuery, jwtToken);
        }
        else {
            this._parsedQuery = await parser_1.queryParser.parseQuery(this.queryReq, jwtToken);
            this.queryMetadata = await services_1.outboundQueryMetadataService.queryMetadata(this._parsedQuery, jwtToken);
            this.entities = this.queryMetadata.entities;
        }
    }
    getDataBaseById(databaseId) {
        return this.queryMetadata.databases.find((db) => db.configObjectId.toString() === databaseId);
    }
    getEntity(id) {
        return this.entities.find((entity) => id === entity.configObjectId);
    }
    getColumn(entityId, columnId) {
        const entity = this.getEntity(entityId);
        if (entity) {
            return entity.physicalColumns.find((column) => columnId === column.configObjectId);
        }
    }
}
exports.default = QueryMetadata;
//# sourceMappingURL=outbound.query.meta.js.map