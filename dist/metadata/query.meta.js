"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const parser_1 = require("../parser");
const services_1 = require("../services");
class QueryMetadata {
    constructor(queryReq) {
        this.queryReq = queryReq;
        this.queryType = queryReq.type;
    }
    get parsedQuery() {
        return this._parsedQuery;
    }
    get parsedNativeQuery() {
        return this._parsedNativeQuery;
    }
    async init(jwtToken) {
        if (constants_1.NATIVE === this.queryType) {
            this._parsedNativeQuery = await parser_1.queryParser.parseNativeQuery(this.queryReq);
            this.queryMetadata = await services_1.queryMetadataService.queryMetadata(this._parsedNativeQuery, jwtToken);
        }
        else {
            this._parsedQuery = await parser_1.queryParser.parseQuery(this.queryReq, jwtToken);
            this.queryMetadata = await services_1.queryMetadataService.queryMetadata(this._parsedQuery, jwtToken);
            this.entities = this.queryMetadata.entities;
        }
    }
    getDataBaseById(databaseId) {
        return this.queryMetadata.databases.find((db) => db.id.toString() === databaseId);
    }
    getEntity(id) {
        return this.entities.find((entity) => id === entity.configObjectId);
    }
    getColumn(entityId, columnId) {
        const entity = this.getEntity(entityId);
        if (entity) {
            return entity.physicalColumns.find((column) => columnId === column.configObjectId);
        }
    }
    async getTableById(tableId, jwtToken) {
        const tables = await services_1.queryMetadataService.getTableById(tableId, jwtToken);
        return tables.find((tb) => tb !== null && tb.id === tableId);
    }
    async getFieldById(tableId, fieldId, jwtToken) {
        const table = await this.getTableById(tableId, jwtToken);
        if (table != null) {
            const fields = table.fields;
            return fields.find((fd) => fd.id === fieldId);
        }
        else {
            return null;
        }
    }
    async getPrimaryKeyFieldByTable(table) {
        const fields = table.fields;
        return fields.find((fd) => fd.special_type === constants_1.PRIMARYKEYTYPE);
    }
}
exports.default = QueryMetadata;
//# sourceMappingURL=query.meta.js.map