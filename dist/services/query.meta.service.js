"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_query_1 = require("../queries/graphql.query");
/* tslint:disable-next-line:no-var-requires */
const fetch = require('node-fetch');
const info_commons_1 = require("info-commons");
const constants_1 = require("../constants");
const client = info_commons_1.createApolloClient(info_commons_1.INFO_META_URI);
class QueryMetadataService {
    async queryMetadata(queryData, jwtToken) {
        const variables = constants_1.NATIVE !== queryData.type ?
            { databaseId: queryData.databaseId, tableId: queryData.sourceTableId } :
            { databaseId: queryData.database, tableId: '-999' };
        const { data } = await client.query({
            query: graphql_query_1.query,
            context: {
                headers: {
                    authorization: jwtToken,
                },
            },
            variables,
        });
        return {
            databases: new Array(data.Database),
            tables: new Array(data.Table),
        };
    }
    async getTableIdByFieldId(fieldId, jwtToken) {
        const variables = { fieldId };
        const { data } = await client.query({
            query: graphql_query_1.queryField,
            context: {
                headers: {
                    authorization: jwtToken,
                },
            },
            variables,
        });
        return data.Field.table_id;
    }
    async getTableById(tableId, jwtToken) {
        const variables = { tableId };
        const { data } = await client.query({
            query: graphql_query_1.queryTable,
            context: {
                headers: {
                    authorization: jwtToken,
                },
            },
            variables,
        });
        return new Array(data.Table);
    }
}
const queryMetadataService = new QueryMetadataService();
/*If freezing below than not able to mock method of this file in tests*/
// Object.freeze(queryMetadataService);
exports.default = queryMetadataService;
//# sourceMappingURL=query.meta.service.js.map