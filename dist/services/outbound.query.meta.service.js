"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_query_1 = require("../queries/graphql.query");
/* tslint:disable-next-line:no-var-requires */
const fetch = require('node-fetch');
const info_commons_1 = require("info-commons");
const constants_1 = require("../constants");
const metadata_utils_1 = require("../utilities/metadata.utils");
class QueryMetadataService {
    async queryMetadata(queryObject, jwtToken) {
        const variables = constants_1.NATIVE !== queryObject.type ?
            { databaseId: queryObject.databaseId, entityId: queryObject.sourceTableId } :
            { databaseId: queryObject.database, entityId: '-999' };
        const data = await metadata_utils_1.getMetaData(info_commons_1.APPENG_META_URI, variables, graphql_query_1.queryData, jwtToken);
        return {
            databases: new Array(data.Database),
            entities: new Array(data.PhysicalEntity),
        };
    }
}
const queryMetadataService = new QueryMetadataService();
exports.default = queryMetadataService;
//# sourceMappingURL=outbound.query.meta.service.js.map