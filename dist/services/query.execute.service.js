"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const executor_1 = require("../executor");
const models_1 = require("../models");
const ioknex_1 = require("../parser/ioknex");
const array_util_1 = __importDefault(require("../utilities/array.util"));
class QueryExecutionService {
    async executeQuery(queryMetadata, jwtToken) {
        try {
            const start = new Date();
            const queryExecutor = await executor_1.createQueryExecutor(queryMetadata);
            const results = await queryExecutor.executeQuery(queryMetadata, jwtToken);
            const end = new Date();
            const startedAt = start.toISOString();
            const averageExecutionTime = end.getTime() - start.getTime();
            const status = constants_1.Status.completed;
            const context = constants_1.ADHOC;
            const rowCount = results.length;
            const runningTime = end.getTime() - start.getTime();
            const data = array_util_1.default.isNotEmpty(results) && Object.entries(results[0]).length > 0 ?
                await ioknex_1.parseResponseData(results, queryMetadata, jwtToken) :
                new models_1.ResponseData([], [], []);
            return new models_1.QueryResponse(startedAt, averageExecutionTime, status, context, rowCount, runningTime, data);
        }
        catch (err) {
            throw err;
        }
    }
}
const queryExecutionService = new QueryExecutionService();
// Object.freeze(queryExecutionService);
exports.default = queryExecutionService;
//# sourceMappingURL=query.execute.service.js.map