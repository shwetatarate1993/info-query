"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const metadata_1 = require("../metadata");
const services_1 = require("../services");
class QueryController {
    async executeQuery(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        const queryMetadata = new metadata_1.QueryMetadata(JSON.parse(JSON.stringify(req.body)));
        await queryMetadata.init(req.get(info_commons_1.TOKEN_KEY));
        const queryResponse = await services_1.queryExecutionService.executeQuery(queryMetadata, req.get(info_commons_1.TOKEN_KEY));
        return res.json(queryResponse);
    }
}
exports.default = new QueryController();
//# sourceMappingURL=query.controller.js.map