"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const arrayUtils = {
    isEmpty: (arr) => {
        const result = !Array.isArray(arr) || !arr.length;
        return result;
    },
    isNotEmpty: (arr) => Array.isArray(arr) && arr.length,
};
exports.default = arrayUtils;
//# sourceMappingURL=array.util.js.map