"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const stringUtils = {
    isEmpty: (str) => str === undefined || str.trim() === '',
    isNotEmpty: (str) => str !== undefined && str !== 'undefined' && str.trim() !== '',
};
exports.default = stringUtils;
// Convert 'how are you' to 'How Are You'
exports.toTitleCase = (str) => {
    str = str.replace('_', ' ');
    return str.replace(/\w\S*/g, (txt) => {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};
//# sourceMappingURL=string.util.js.map